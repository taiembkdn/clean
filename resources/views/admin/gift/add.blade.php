@extends('templates.admin.master')
@section('content')
    
  <main id="main" class="main">

<div class="pagetitle">
  <h1>Thêm mã giảm giá</h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="/admin">Dashboard</a></li>
      <li class="breadcrumb-item"><a href="{{ route('admin.gift.index') }}">Mã giảm giá</a></li>
      <li class="breadcrumb-item active">Thêm mã giảm giá</li>
    </ol>
  </nav>
</div><!-- End Page Title -->

<section class="section">
  <div class="row">
    <div class="col-lg-12">

      <div class="card">
        <div class="card-body">
          <h5 class="card-title">Thêm mã giảm giá mới</h5>

          <!-- General Form Elements -->
          <form action="{{ route('admin.gift.store') }}" method="post">
              {{ csrf_field() }}
            <div class="row mb-3">
              <label for="code" class="col-sm-2 col-form-label">Mã giảm giá</label>
              <div class="col-sm-10">
                <input type="text" name="code" id="code" class="form-control" required value="{{ old('code') }}" />
              </div>
            </div>

            <div class="row mb-3">
              <label class="col-sm-2 col-form-label">Loại giảm giá</label>
              <div class="col-sm-10">
                <select class="form-select" name="type" aria-label="Default select example">
                  <option {{ old('type') == 0?'selected':'' }} value="0">Theo phần trăm</option>
                  <option {{ old('type') == 1?'selected':'' }} value="1">Theo số tiền</option>
                </select>
              </div>
            </div>

            <div class="row mb-3">
              <label for="value" class="col-sm-2 col-form-label">Giá trị(Phần trăm hoặc số tiền):</label>
              <div class="col-sm-10">
                <input type="number" name="value" id="value" class="form-control" required value="{{ old('value') }}" />
              </div>
            </div>

            <div class="row mb-3">
              <label class="col-sm-2 col-form-label">Submit</label>
              <div class="col-sm-10">
                <button type="submit" class="btn btn-primary">Thêm</button>
              </div>
            </div>

          </form><!-- End General Form Elements -->

        </div>
      </div>

    </div>

  </div>
</section>

</main><!-- End #main -->
@endsection