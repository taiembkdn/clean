@extends('templates.admin.master')
@section('content')
    

<main id="main" class="main">

<div class="pagetitle">
  <h1>Mã giảm giá</h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="/admin">Dashboard</a></li>
      <li class="breadcrumb-item active">Mã giảm giá</li>
    </ol>
  </nav>
</div><!-- End Page Title -->

<section class="section">
  <div class="row">
    <div class="col-lg-12">

      <div class="card">
        <div class="card-body">
            @include('errors.msg')
          <div class="py-2">
            <a href="{{ route('admin.gift.create') }}" class="btn btn-success rounded-pill"><i class="bi bi-star me-1"></i> Thêm
              mới</a>
          </div>

          <!-- Table with stripped rows -->
          <table class="table datatable">
            <thead>
              <tr>
                <th scope="col">STT</th>
                <th scope="col">Mã giảm giá</th>
                <th scope="col">Loại</th>
                <th scope="col">Giá trị</th>
                <th scope="col">Tình trạng</th>
                <th scope="col">Chức năng</th>
              </tr>
            </thead>
            <tbody>
            @foreach($gifts as $key => $gift)
              <tr>
                <th scope="row">{{ $key + 1 }}</th>
                <td>{{ $gift->code }}</td>
                @if($gift->type == 0)
                <td>Giảm theo %</td>
                @else
                <td>Giảm theo giá</td>
                @endif
                @if($gift->type == 0)
                <td>{{ $gift->value }}%</td>
                @else
                <td>{{ number_format($gift->value, 0, ',', '.') }}đ</td>
                @endif
                @if($gift->active == 1)
                <td id="result-{{ $gift->id }}"><a onclick="return getActive({{$gift->id}})" class="text-success" href="javascript:void(0)">Đang hoạt động</a></td>
                @else
                <td id="result-{{ $gift->id }}"><a onclick="return getActive({{$gift->id}})" class="text-danger" href="javascript:void(0)">Ngưng hoạt động</a></td>
                @endif
                <td>
                  <a href="{{ route('admin.gift.edit', $gift->id) }}" class="btn btn-warning rounded-pill btn-sm text-white"><i
                      class="bi bi-pen"></i>
                    Sửa</a>
                  <a href="{{ route('admin.gift.delete', $gift->id) }}" class="btn btn-danger rounded-pill btn-sm"><i class="bi bi-trash"></i>
                    Xóa</a>
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
          <!-- End Table with stripped rows -->

        </div>
      </div>

    </div>
  </div>
</section>

</main><!-- End #main -->

<script>
      function getActive(id){
        $.ajax({
          url: "{{ route('admin.gift.active') }}",
          type: 'GET',
          cache: false,
          data: {
                id: id,
            },
          success: function(data){
            console.log('success')
            $('#result-'+id).html(data);
          }, 
          error: function() {
           alert("Có lỗi");
         }
       }); 
        return false;
      }

</script>
@endsection