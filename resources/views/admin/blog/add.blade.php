@extends('templates.admin.master')
@section('content')
    
  <main id="main" class="main">

<div class="pagetitle">
  <h1>Thêm bài viết</h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="/admin">Dashboard</a></li>
      <li class="breadcrumb-item"><a href="{{ route('admin.blog.index') }}">Blog</a></li>
      <li class="breadcrumb-item active">Thêm bài viết</li>
    </ol>
  </nav>
</div><!-- End Page Title -->

<section class="section">
  <div class="row">
    <div class="col-lg-12">

      <div class="card">
        <div class="card-body">
          <h5 class="card-title">Thêm bài viết mới</h5>

          <!-- General Form Elements -->
          <form action="{{ route('admin.blog.store') }}" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
            <div class="row mb-3">
              <label for="title" class="col-sm-2 col-form-label">Tên bài viết</label>
              <div class="col-sm-10">
                <input type="text" name="title" class="form-control" required value="{{ old('title') }}" id="title" />
              </div>
            </div>
            <div class="row mb-3">
              <label for="picture" class="col-sm-2 col-form-label">Hình ảnh</label>
              <div class="col-sm-10">
                <input class="form-control" type="file" id="picture" name="picture">
              </div>
            </div>
            
            <div class="row mb-3">
              <label class="col-sm-2 col-form-label">Danh mục</label>
              <div class="col-sm-10">
                <select class="form-select" name="category_id" aria-label="Default select example">
                  @foreach($categories as $cate)
                  @if($cate->id == old('category_id'))
                  <option selected value="{{ $cate->id }}">{{ $cate->name }}</option>
                  @else
                  <option value="{{ $cate->id }}">{{ $cate->name }}</option>
                  @endif
                  @endforeach
                </select>
              </div>
            </div>
            
            <div class="row mb-3">
                <label for="description" class="col-sm-2 col-form-label">Mô tả</label>
                <div class="col-sm-10">
                  <textarea class="form-control" name="description" style="height: 100px" id="description">{{ old('description') }}</textarea>
                </div>
              </div>
                
              <div class="row mb-3">
                  <label for="detail" class="col-sm-2 col-form-label">Chi tiết</label>
                  <div class="col-sm-10">
                    <textarea class="form-control" name="detail" style="height: 100px" id="detail">{{ old('detail') }}</textarea>
                  </div>
                </div>

            <div class="row mb-3">
              <label class="col-sm-2 col-form-label">Submit</label>
              <div class="col-sm-10">
                <button type="submit" class="btn btn-primary">Thêm</button>
              </div>
            </div>

          </form><!-- End General Form Elements -->

        </div>
      </div>

    </div>

  </div>
</section>

</main><!-- End #main -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/ckeditor/4.18.0/ckeditor.js"></script>

<script>
  var options = {
    filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
    filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
    filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
    filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
  };
</script>
<script>
CKEDITOR.replace('detail', options);
</script>

@endsection