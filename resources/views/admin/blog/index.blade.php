@extends('templates.admin.master')
@section('content')
    

<main id="main" class="main">

<div class="pagetitle">
  <h1>Blog</h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="/admin">Dashboard</a></li>
      <li class="breadcrumb-item active">Blog</li>
    </ol>
  </nav>
</div><!-- End Page Title -->

<section class="section">
  <div class="row">
    <div class="col-lg-12">

      <div class="card">
        <div class="card-body">
            @include('errors.msg')
          <div class="py-2">
            <a href="{{ route('admin.blog.create') }}" class="btn btn-success rounded-pill"><i class="bi bi-star me-1"></i> Thêm
              mới</a>
          </div>

          <!-- Table with stripped rows -->
          <table class="table datatable">
            <thead>
              <tr>
                <th scope="col">STT</th>
                <th scope="col">Tên bài viết</th>
                <th scope="col">Danh mục</th>
                <th scope="col">Hình ảnh</th>
                <th scope="col">Trạng thái</th>
                <th scope="col">Chức năng</th>
              </tr>
            </thead>
            <tbody>
            @foreach($blogs as $key => $blog)
              <tr>
                <th scope="row">{{ $key + 1 }}</th>
                <td>{{ $blog->title }}</td>
                <td>{{ $blog->category->name }}</td>
                <td><img src="/upload/{{ $blog->picture }}" class="object-fit" width="200px" height="150px" /></td>
                @if($blog->active == 1)
                <td id="result-{{ $blog->id }}"><a onclick="return getActive({{$blog->id}})" class="text-success" href="javascript:void(0)">Có hiển thị</a></td>
                @else
                <td id="result-{{ $blog->id }}"><a onclick="return getActive({{$blog->id}})" class="text-danger" href="javascript:void(0)">Không hiển thị</a></td>
                @endif
                <td>
                  <a href="{{ route('admin.blog.edit', $blog->id) }}" class="btn btn-warning rounded-pill btn-sm text-white"><i
                      class="bi bi-pen"></i>
                    Sửa</a>
                  <a href="{{ route('admin.blog.delete', $blog->id) }}" class="btn btn-danger rounded-pill btn-sm"><i class="bi bi-trash"></i>
                    Xóa</a>
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
          <!-- End Table with stripped rows -->

        </div>
      </div>

    </div>
  </div>
</section>

</main><!-- End #main -->

<script>
      function getActive(id){
        $.ajax({
          url: "{{ route('admin.blog.active') }}",
          type: 'GET',
          cache: false,
          data: {
                id: id,
            },
          success: function(data){
            console.log('success')
            $('#result-'+id).html(data);
          }, 
          error: function() {
           alert("Có lỗi");
         }
       }); 
        return false;
      }

</script>
@endsection