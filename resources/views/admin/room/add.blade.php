@extends('templates.admin.master')
@section('content')
    
  <main id="main" class="main">

<div class="pagetitle">
  <h1>Thêm phòng</h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="/admin">Dashboard</a></li>
      <li class="breadcrumb-item"><a href="{{ route('admin.room.index') }}">Quản lý phòng</a></li>
      <li class="breadcrumb-item active">Thêm phòng</li>
    </ol>
  </nav>
</div><!-- End Page Title -->

<section class="section">
  <div class="row">
    <div class="col-lg-12">

      <div class="card">
        <div class="card-body">
          <h5 class="card-title">Thêm phòng mới</h5>

          <!-- General Form Elements -->
          <form action="{{ route('admin.room.store') }}" method="post">
              {{ csrf_field() }}
            <div class="row mb-3">
              <label for="inputText" class="col-sm-2 col-form-label">Tên phòng</label>
              <div class="col-sm-10">
                <input type="text" name="name" class="form-control" required value="{{ old('name') }}" />
              </div>
            </div>
            
            <div class="row mb-3">
              <label class="col-sm-2 col-form-label">Danh mục cha</label>
              <div class="col-sm-10">
                <select class="form-select" name="room_id" aria-label="Default select example">
                  <option value="0">Không</option>
                  @foreach($rooms as $room)
                  @if($room->id == old('room_id'))
                  <option selected value="{{ $room->id }}">{{ $room->name }}</option>
                  @else
                  <option value="{{ $room->id }}">{{ $room->name }}</option>
                  @endif
                  @endforeach
                </select>
              </div>
            </div>

            <div class="row mb-3">
              <label class="col-sm-2 col-form-label">Submit</label>
              <div class="col-sm-10">
                <button type="submit" class="btn btn-primary">Thêm</button>
              </div>
            </div>

          </form><!-- End General Form Elements -->

        </div>
      </div>

    </div>

  </div>
</section>

</main><!-- End #main -->
@endsection