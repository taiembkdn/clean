@extends('templates.admin.master')
@section('content')
    

<main id="main" class="main">

<div class="pagetitle">
  <h1>Người dùng</h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="/admin">Dashboard</a></li>
      <li class="breadcrumb-item active">Người dùng</li>
    </ol>
  </nav>
</div><!-- End Page Title -->

<section class="section">
  <div class="row">
    <div class="col-lg-12">

      <div class="card">
        <div class="card-body">
            @include('errors.msg')

          <!-- Table with stripped rows -->
          <table class="table datatable">
            <thead>
              <tr>
                <th scope="col">STT</th>
                <th scope="col">Tên người dùng</th>
                <th scope="col">Email</th>
                <th scope="col">Số điện thoại</th>
                <th scope="col">Trạng thái</th>
                <th scope="col">Chức năng</th>
              </tr>
            </thead>
            <tbody>
            @foreach($users as $key => $user)
              <tr>
                <th scope="row">{{ $key + 1 }}</th>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->phone_number }}</td>
                @if($user->active == 1)
                <td id="result-{{ $user->id }}"><a onclick="return getActive({{$user->id}})" class="text-success" href="javascript:void(0)">Đang hoạt động</a></td>
                @else
                <td id="result-{{ $user->id }}"><a onclick="return getActive({{$user->id}})" class="text-danger" href="javascript:void(0)">Bị khóa</a></td>
                @endif
                <td>
                  <a href="{{ route('admin.user.show', $user->id) }}" class="btn btn-success rounded-pill btn-sm text-white"><i
                      class="bi bi-eye"></i>
                    Xem</a>
                  <a href="{{ route('admin.user.delete', $user->id) }}" class="btn btn-danger rounded-pill btn-sm"><i class="bi bi-trash"></i>
                    Xóa</a>
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
          <!-- End Table with stripped rows -->

        </div>
      </div>

    </div>
  </div>
</section>

</main><!-- End #main -->

<script>
      function getActive(id){
        $.ajax({
          url: "{{ route('admin.user.active') }}",
          type: 'GET',
          cache: false,
          data: {
                id: id,
            },
          success: function(data){
            console.log('success')
            $('#result-'+id).html(data);
          }, 
          error: function() {
           alert("Có lỗi");
         }
       }); 
        return false;
      }

</script>
@endsection