@extends('templates.admin.master')
@section('content')
    
<main id="main" class="main">

<div class="pagetitle">
  <h1>Xem thành viên</h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="/admin">Dashboard</a></li>
      <li class="breadcrumb-item"><a href="{{ route('admin.user.index') }}">Thành viên</a></li>
      <li class="breadcrumb-item active">Xem thành viên</li>
    </ol>
  </nav>
</div><!-- End Page Title -->

<section class="section profile">
  <div class="row">
    <div class="col-xl-4">

      <div class="card">
        <div class="card-body profile-card pt-4 d-flex flex-column align-items-center">

          <img src="/upload/{{ $user->avatar }}" alt="Profile" class="object-fit rounded-circle" height="120px" width="120px" >
          <h2>{{ $user->name }}</h2>
          
        </div>
      </div>

    </div>

    <div class="col-xl-8">

      <div class="card">
        <div class="card-body pt-3">
          <!-- Bordered Tabs -->
          <ul class="nav nav-tabs nav-tabs-bordered">

            <li class="nav-item">
              <button class="nav-link active" data-bs-toggle="tab"
                data-bs-target="#profile-overview">Overview</button>
            </li>

            <li class="nav-item">
              <button class="nav-link" data-bs-toggle="tab" data-bs-target="#profile-edit">Lịch sử đặt lịch</button>
            </li>

          </ul>
          <div class="tab-content pt-2">

            <div class="tab-pane fade show active profile-overview" id="profile-overview">

              <h5 class="card-title">Thông tin chi tiết</h5>

              <div class="row">
                <div class="col-lg-3 col-md-4 label ">Full Name</div>
                <div class="col-lg-9 col-md-8">{{ $user->name }}</div>
              </div>

              <div class="row">
                <div class="col-lg-3 col-md-4 label">Email</div>
                <div class="col-lg-9 col-md-8">{{ $user->email }}</div>
              </div>

              <div class="row">
                <div class="col-lg-3 col-md-4 label">Số điện thoại</div>
                <div class="col-lg-9 col-md-8">{{ $user->phone_number }}</div>
              </div>

              <div class="row">
                <div class="col-lg-3 col-md-4 label">Địa chỉ</div>
                <div class="col-lg-9 col-md-8">{{ $user->address }}</div>
              </div>

              <div class="row">
                <div class="col-lg-3 col-md-4 label">Quận/Huyện</div>
                <div class="col-lg-9 col-md-8">{{ $user->district }}</div>
              </div>

              <div class="row">
                <div class="col-lg-3 col-md-4 label">Thành phố</div>
                <div class="col-lg-9 col-md-8">{{ $user->city }}</div>
              </div>

            </div>
            <div class="tab-pane fade profile-edit pt-3" id="profile-edit">

              <table class="table datatable">
                <thead>
                  <tr>
                    <th scope="col">STT</th>
                    <th scope="col">Tên dịch vụ</th>
                    <th scope="col">Hình ảnh</th>
                    <th scope="col">Trạng thái</th>
                    <th scope="col">Chức năng</th>
                  </tr>
                </thead>
                <tbody>
                @foreach($user->booking as $key => $booking)
                  <tr>
                    <th scope="row">{{ $key + 1 }}</th>
                    <td>{{ $booking->service->title }}</td>
                    <td><img src="/upload/{{ $booking->service->picture }}" height="80px" width="200px" class="object-fit" /></td>
                    @if($booking->active == 1)
                    <td id="result-{{ $booking->id }}"><a onclick="return getActive({{$booking->id}})" class="text-success" href="javascript:void(0)">Đã hoàn thành</a></td>
                    @else
                    <td id="result-{{ $booking->id }}"><a onclick="return getActive({{$booking->id}})" class="text-danger" href="javascript:void(0)">Chưa hoàn thành</a></td>
                    @endif
                    <td>
                      <a href="{{ route('admin.booking.show', $booking->id) }}" class="btn btn-success rounded-pill btn-sm"><i class="bi bi-eye"></i>
                        Xem</a>
                      <a href="{{ route('admin.booking.delete', $booking->id) }}" class="btn btn-danger rounded-pill btn-sm"><i class="bi bi-trash"></i>
                        Xóa</a>
                    </td>
                  </tr>
                @endforeach
                </tbody>
              </table>

            </div>

          </div><!-- End Bordered Tabs -->

        </div>
      </div>

    </div>
  </div>
</section>

</main><!-- End #main -->
@endsection