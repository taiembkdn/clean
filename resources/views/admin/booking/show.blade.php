@extends('templates.admin.master')
@section('content')
<style type="text/css">
    .label {
        font-weight: 600;
        color: rgba(1, 41, 112, 0.6);
    }
</style>
<main id="main" class="main">

    <div class="pagetitle">
        <h1>Xem thành viên</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/admin">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{ route('admin.booking.index') }}">Quản lý đặt lịch</a></li>
                <li class="breadcrumb-item active">Xem chi tiết dịch vụ</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section profile">
        <div class="row">

            <div class="col-xl-12">

                <div class="card">
                    <div class="card-body pt-3">
                        <!-- Bordered Tabs -->
                        <ul class="nav nav-tabs nav-tabs-bordered">

                            <li class="nav-item">
                                <button class="nav-link active" data-bs-toggle="tab"
                                    data-bs-target="#profile-overview">Overview</button>
                            </li>

                        </ul>
                        <div class="tab-content pt-2">

                            <div class="tab-pane fade show active profile-overview">

                                <h5 class="card-title">Thông tin chi tiết</h5>

                                <div class="row">
                                    <div class="col-lg-3 col-md-4 label ">Full Name</div>
                                    <div class="col-lg-9 col-md-8">{{ $booking->user->name }}</div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-3 col-md-4 label">Email</div>
                                    <div class="col-lg-9 col-md-8">{{ $booking->user->email }}</div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-3 col-md-4 label">Số điện thoại</div>
                                    <div class="col-lg-9 col-md-8">{{ $booking->user->phone_number }}</div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-3 col-md-4 label">Địa chỉ</div>
                                    <div class="col-lg-9 col-md-8">{{ $booking->user->address }}</div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-3 col-md-4 label">Quận/huyện</div>
                                    <div class="col-lg-9 col-md-8">{{ $booking->user->district }}</div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-3 col-md-4 label">Thành phố</div>
                                    <div class="col-lg-9 col-md-8">{{ $booking->user->city }}</div>
                                </div>


                                <div class="row ">
                                    <div class="col-lg-3 col-md-4 label ">Tên dịch vụ</div>
                                    <div class="col-lg-9 col-md-8">{{ $booking->service->title }}</div>
                                </div>

                                <div class="row ">
                                    <div class="col-lg-3 col-md-4 label">Giờ</div>
                                    <div class="col-lg-9 col-md-8">{{ $booking->hour }}</div>
                                </div>

                                <div class="row ">
                                    <div class="col-lg-3 col-md-4 label">Ngày</div>
                                    <div class="col-lg-9 col-md-8">{{ $booking->date_view }}</div>
                                </div>

                                <div class="row ">
                                    <div class="col-lg-3 col-md-4 label">Sử dụng dịch vụ trong</div>
                                    <div class="col-lg-9 col-md-8">{{ $booking->calendar }}</div>
                                </div>

                                <div class="row ">
                                    <div class="col-lg-3 col-md-4 label">Biết đến dịch vụ từ</div>
                                    <div class="col-lg-9 col-md-8">{{ $booking->know }}</div>
                                </div>

                                <div class="row ">
                                    <div class="col-lg-3 col-md-4 label">Chi tiết phòng</div>
                                    <div class="col-lg-9 col-md-8">
                                        <ul>
                                            @foreach($booking->room as $room)
                                            <li>{{ $room->name }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>

                                <div class="row ">
                                    <div class="col-lg-3 col-md-4 label">Dịch vụ đặc biệt</div>
                                    <div class="col-lg-9 col-md-8">
                                        <ul>
                                            @foreach($booking->special as $special)
                                            <li>{{ $special->name }}</li>
                                            @endforeach
                                         
                                        </ul>
                                    </div>
                                </div>
                                
                                <div class="row ">
                                    <div class="col-lg-3 col-md-4 label">Yêu cầu khác</div>
                                    <div class="col-lg-9 col-md-8">
                                        <ul>
                                           
                                            @if($booking->other_request) <li>{{ $booking->other_request }}</li>
                                            @endif
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        

                        </div><!-- End Bordered Tabs -->

                    </div>
                </div>

            </div>
        </div>
    </section>

</main><!-- End #main -->
@endsection