@extends('templates.admin.master')
@section('content')
    

<main id="main" class="main">

<div class="pagetitle">
  <h1>Danh mục blog</h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="/admin">Dashboard</a></li>
      <li class="breadcrumb-item active">Danh mục blog</li>
    </ol>
  </nav>
</div><!-- End Page Title -->

<section class="section">
  <div class="row">
    <div class="col-lg-12">

      <div class="card">
        <div class="card-body">
            @include('errors.msg')

          <!-- Table with stripped rows -->
          <table class="table datatable">
            <thead>
              <tr>
                <th scope="col">STT</th>
                <th scope="col">Dịch vụ</th>
                <th scope="col">Tên</th>
                <th scope="col">SDT</th>
                <th scope="col">Chức năng</th>
              </tr>
            </thead>
            <tbody>
            @foreach($bookings as $key => $booking)
              <tr>
                <th scope="row">{{ $key + 1 }}</th>
                <td>{{ $booking->service->title }}</td>
                <td>{{ $booking->user->name }}</td>
                <td>{{ $booking->user->phone_number }}</td>
                <td>
                  <a href="{{ route('admin.booking.show', $booking->id) }}" class="btn btn-warning rounded-pill btn-sm text-white"><i
                      class="bi bi-pen"></i>
                    Xem</a>
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
          <!-- End Table with stripped rows -->

        </div>
      </div>

    </div>
  </div>
</section>

</main><!-- End #main -->
@endsection