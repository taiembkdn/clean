@extends('templates.admin.master')
@section('content')
    
  <main id="main" class="main">

<div class="pagetitle">
  <h1>Thêm yêu cầu</h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="/admin">Dashboard</a></li>
      <li class="breadcrumb-item"><a href="{{ route('admin.room.index') }}">Yêu cầu đặc biệt</a></li>
      <li class="breadcrumb-item active">Thêm yêu cầu</li>
    </ol>
  </nav>
</div><!-- End Page Title -->

<section class="section">
  <div class="row">
    <div class="col-lg-12">

      <div class="card">
        <div class="card-body">
          <h5 class="card-title">Thêm yêu cầu mới</h5>

          <!-- General Form Elements -->
          <form action="{{ route('admin.special.store') }}" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
            <div class="row mb-3">
              <label for="inputText" class="col-sm-2 col-form-label">Tên yêu cầu</label>
              <div class="col-sm-10">
                <input type="text" name="name" class="form-control" required value="{{ old('name') }}" />
              </div>
            </div>

            <div class="row mb-3">
              <label for="icon" class="col-sm-2 col-form-label">Hình ảnh</label>
              <div class="col-sm-10">
                <input class="form-control" type="file" id="icon" name="icon">
              </div>
            </div>

            <div class="row mb-3">
              <label class="col-sm-2 col-form-label">Submit</label>
              <div class="col-sm-10">
                <button type="submit" class="btn btn-primary">Thêm</button>
              </div>
            </div>

          </form><!-- End General Form Elements -->

        </div>
      </div>

    </div>

  </div>
</section>

</main><!-- End #main -->
@endsection