@extends('templates.admin.master')
@section('content')
    

<main id="main" class="main">

<div class="pagetitle">
  <h1>Quản lý admin</h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="/admin">Dashboard</a></li>
      <li class="breadcrumb-item active">Quản lý admin</li>
    </ol>
  </nav>
</div><!-- End Page Title -->

<section class="section">
  <div class="row">
    <div class="col-lg-12">

      <div class="card">
        <div class="card-body">
            @include('errors.msg')
          <div class="py-2">
            <a href="{{ route('admin.admin.create') }}" class="btn btn-success rounded-pill"><i class="bi bi-star me-1"></i> Thêm
              mới</a>
          </div>

          <!-- Table with stripped rows -->
          <table class="table datatable">
            <thead>
              <tr>
                <th scope="col">STT</th>
                <th scope="col">Tên người dùng</th>
                <th scope="col">Email</th>
                <th scope="col">Chức năng</th>
              </tr>
            </thead>
            <tbody>
            @foreach($users as $key => $user)
              <tr>
                <th scope="row">{{ $key + 1 }}</th>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td>
                  <a href="{{ route('admin.admin.edit', $user->id) }}" class="btn btn-warning rounded-pill btn-sm text-white"><i
                      class="bi bi-pen"></i>
                    Sửa</a>
                  <a href="{{ route('admin.admin.delete', $user->id) }}" class="btn btn-danger rounded-pill btn-sm"><i class="bi bi-trash"></i>
                    Xóa</a>
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
          <!-- End Table with stripped rows -->

        </div>
      </div>

    </div>
  </div>
</section>

</main><!-- End #main -->
@endsection