@extends('templates.admin.master')
@section('content')
<main id="main" class="main">

    <div class="pagetitle">
        <h1>Dashboard</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                <li class="breadcrumb-item active">Dashboard</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section dashboard">
        <div class="row">

            <!-- Left side columns -->
            <div class="col-lg-12">
                <div class="row">

                    <div class="col-xxl-12 col-md-12 text-center py-3">
                        <h1>Welcome to <br> FreshHouse Admin </h1>
                    </div><!-- End Sales Card -->
                    <!-- Sales Card -->
                    <div class="col-xxl-3 col-md-6">
                        <div class="card info-card sales-card">



                            <div class="card-body">
                                <h5 class="card-title">Lượt đặt lịch</h5>

                                <div class="d-flex align-items-center">
                                    <div
                                        class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                                        <i class="bi bi-calendar4-event"></i>
                                    </div>
                                    <div class="ps-3">
                                        <h6>145</h6>
                                        <span class="text-muted small pt-2 ps-1">Lượt book</span>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div><!-- End Sales Card -->

                    <!-- Revenue Card -->
                    <div class="col-xxl-3 col-md-6">
                        <div class="card info-card revenue-card">



                            <div class="card-body">
                                <h5 class="card-title">Mã giảm giá </h5>

                                <div class="d-flex align-items-center">
                                    <div
                                        class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                                        <i class="bi bi-currency-dollar"></i>
                                    </div>
                                    <div class="ps-3">
                                        <h6>34 </h6>
                                        <span class="text-muted small pt-2 ps-1">Mã</span>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div><!-- End Revenue Card -->

                    <!-- Customers Card -->
                    <div class="col-xxl-3 col-xl-12">

                        <div class="card info-card customers-card">



                            <div class="card-body">
                                <h5 class="card-title">Người dùng</h5>

                                <div class="d-flex align-items-center">
                                    <div
                                        class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                                        <i class="bi bi-people"></i>
                                    </div>
                                    <div class="ps-3">
                                        <h6>34</h6>
                                        <span class="text-muted small pt-2 ps-1">Người dùng</span>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div><!-- End Customers Card -->


                    <!-- Revenue Card -->
                    <div class="col-xxl-3 col-md-6">
                        <div class="card info-card revenue-card">



                            <div class="card-body">
                                <h5 class="card-title">Dịch vụ </h5>

                                <div class="d-flex align-items-center">
                                    <div
                                        class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                                        <i class="bi bi-list-check"></i>
                                    </div>
                                    <div class="ps-3">
                                        <h6>4 </h6>
                                        <span class="text-muted small pt-2 ps-1">Dịch vụ</span>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div><!-- End Revenue Card -->

                    <div class="col-xxl-12 col-md-6">
                        <div class="card info-card sales-card">
                            <div class="card-body">
                                <div class="row pt-3">
                                    <div class="col-xxl-4">
                                        <div class="py-2">
                                            <a href="/" class="btn btn-success rounded-pill"><i
                                                    class="bi bi-list-check"></i> Thêm
                                                mới dịch vụ</a>
                                        </div>
                                        <div class="py-2">
                                            <a href="/" class="btn btn-success rounded-pill"><i class="bi bi-grid"></i>
                                                Thêm
                                                mới yêu cầu</a>
                                        </div>
                                    </div>
                                    <div class="col-xxl-4">
                                        <div class="py-2">
                                            <a href="/" class="btn btn-success rounded-pill"><i class="bi bi-gift"></i>
                                                Thêm
                                                mã giảm giá</a>
                                        </div>
                                        <div class="py-2">
                                            <a href="/" class="btn btn-success rounded-pill"><i class="bi bi-star"></i>
                                                Thêm danh các yêu
                                                cầu đặc biệt</a>
                                        </div>

                                    </div>
                                    <div class="col-xxl-4">
                                        <div class="py-2">
                                            <a href="/" class="btn btn-success rounded-pill"><i
                                                    class="bi bi-clipboard-check"></i> Thêm danh mục
                                                blog</a>
                                        </div>
                                        <div class="py-2">
                                            <a href="/" class="btn btn-success rounded-pill"><i class="bi bi-pen"></i>
                                                Thêm blog</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div><!-- End Left side columns -->



        </div>
    </section>

</main><!-- End #main -->
@stop