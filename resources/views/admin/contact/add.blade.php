@extends('templates.admin.master')
@section('content')
    
  <main id="main" class="main">

<div class="pagetitle">
  <h1>Thêm admin</h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="/admin">Dashboard</a></li>
      <li class="breadcrumb-item"><a href="{{ route('admin.category.index') }}">Quản lý admin</a></li>
      <li class="breadcrumb-item active">Thêm admin</li>
    </ol>
  </nav>
</div><!-- End Page Title -->

<section class="section">
  <div class="row">
    <div class="col-lg-12">

      <div class="card">
        <div class="card-body">
          <h5 class="card-title">Thêm admin mới</h5>

          <!-- General Form Elements -->
          <form action="{{ route('admin.admin.store') }}" method="post">
              {{ csrf_field() }}
            <div class="row mb-3">
              <label for="inputText" class="col-sm-2 col-form-label">Tên người dùng</label>
              <div class="col-sm-10">
                <input type="text" name="name" class="form-control" required value="{{ old('name') }}" />
                @if($errors->has('name'))<p class="text-danger">{{ $errors->first('name') }}</p> 
                @endif
              </div>
            </div>

            <div class="row mb-3">
              <label for="inputText" class="col-sm-2 col-form-label">Email</label>
              <div class="col-sm-10">
                <input type="email" name="email" class="form-control" required value="{{ old('email') }}" />
              </div>
            </div>
            @if($errors->has('email'))<p class="text-danger">{{ $errors->first('email') }}</p> 
            @endif

            <div class="row mb-3">
              <label for="inputText" class="col-sm-2 col-form-label">Password</label>
              <div class="col-sm-10">
                <input type="password" name="password" class="form-control" required />
              </div>
            </div>
            @if($errors->has('password'))<p class="text-danger">{{ $errors->first('password') }}</p> 
            @endif

            <div class="row mb-3">
              <label class="col-sm-2 col-form-label">Submit</label>
              <div class="col-sm-10">
                <button type="submit" class="btn btn-primary">Thêm</button>
              </div>
            </div>

          </form><!-- End General Form Elements -->

        </div>
      </div>

    </div>

  </div>
</section>

</main><!-- End #main -->
@endsection