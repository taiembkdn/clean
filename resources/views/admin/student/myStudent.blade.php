@extends('admin.master')

@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="css/style.css">
@endsection

@section('content')
<main class="content">

  @include('admin.header')

  <div class="row">
    <div class="col-sm-12">
      <div class="table-responsive py-4">
        <table class="table" id="myTable">
          <thead class="thead-light">
            <tr>
              <th>id</th>
              <th>Tên</th>
              <th>Tỉnh thành</th>
              <th>Chuyên ngành</th>
              <th>Người tư vấn</th>
              <th>Trạng thái</th>
              <th>action</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($listStudent as $student)
            <tr>
              <td>{{ $loop->index+1 }}</td>
              <td>{{ $student->name }}</td>
              <td>{{ $student->province->name }}</td>
              <td>{{ $student->major->name }}</td>
              <td>
                <select class="form-select" id="role" name="role" disabled>
                  <option value="{{ auth()->user()->id }}">{{ auth()->user()->name }}</option>
                </select>
              </td>
              <td>
                <select class="form-select" id="status" name="status"
                  onchange="selectStatus({{ $student->id }}, this.value)">
                  <option value="">-- Chọn --</option>
                  @foreach($listStatus as $status)
                  <option value="{{ $status->id }}" @if ($status->id == $student->status_id) selected @endif>
                    {{ $status->name }}
                  </option>
                  @endforeach
                </select>
              </td>
              <td>
                <a class="btn btn-secondary py-1 px-2 font-small"
                  href="{{ route('admin.student.show', $student->id) }}">
                  <span class="font-weight-bold">Sửa</span>
                </a>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>

  @include('admin.footer')
</main>
@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" referrerpolicy="no-referrer"></script>
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>

<script>
  $(document).ready(function () {
    $('#myTable').DataTable();
  });

  $('#myTable').dataTable( {
    "pageLength": 50 // --> now set 25 row
  });
</script>

<script>
  function selectTeacher(s_id, id) {
    $.ajax({
      url: "{{ route('admin.student.assign') }}",
      type: "GET",
      cache: false,
      data: {
        s_id: s_id,
        id: id,
      },
      success: function(data) {
        console.log(id);
      },
      error: function() {
        console.log("Có lỗi");
      }
    });
  }

  function selectStatus(s_id, id) {
    $.ajax({
      url: "{{ route('admin.student.status') }}",
      type: "GET",
      cache: false,
      data: {
        s_id: s_id,
        id: id,
      },
      success: function(data) {
        console.log(id);
      },
      error: function() {
        console.log("Có lỗi");
      }
    });
  }
</script>
@endsection