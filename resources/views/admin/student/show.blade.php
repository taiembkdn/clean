@extends('admin.master')

@section('css')
<link rel="stylesheet" href="css/style.css">
@endsection

@section('content')
<!-- Modal -->
<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="modal-form" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body p-0">
        <div class="card border-light p-4">
          <div class="card-header border-0 text-center pb-0">
            <h2 class="h4">Kiểm tra thông tin sinh viên</h2>
            <span class="font-italic text-info">Nếu giấy báo sai sót vui lòng liên hệ quản trị viên để sửa lại giấy
              báo</span>
          </div>
          <div class="card-body">
            <form action="{{ route('admin.student.print', $student->id) }}" method="GET" id="addInfo">
              <div class="form-group mb-3">
                <label for="name">Họ và tên</label>
                <input type="text" class="form-control" id="name1" name="name1" value="{{ $student->name }}" disabled>
              </div>
              <div class="form-group mb-3">
                <label for="birthday">Ngày sinh</label>
                <input type="text" class="form-control" id="birthday1" name="birthday1"
                  value="{{ date('d-m-Y', strtotime($student->birthday)) }}" disabled>
              </div>
              <div class="form-group mb-3">
                <label for="gender">Giới tính</label>
                <input type="text" class="form-control" id="gender1" name="gender1"
                  value="{{ $student->gender == 'nam' ? 'Nam' : 'Nữ' }}" disabled>
              </div>
              <div class="form-group mb-3">
                <label for="province">Hộ khẩu thường trú</label>
                <input type="text" class="form-control" id="province1" name="province1"
                  value="{{ $student->province->name }}" disabled>
              </div>
              <div class="form-group mb-3">
                <label for="major1">Ngành học</label>
                <input type="text" class="form-control" id="major1" name="major1"
                  value="{{ $student->major->name }}" disabled>
              </div>
              <button type="submit" class="btn btn-block btn-secondary">In giấy báo</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end modal -->

<main class="content">
  @include('admin.header')

  <form method="POST" action="{{ route('admin.student.update', $student->id) }}" id="editUser">
    @csrf
    <div class="row my-4">
      <!-- leftbar -->
      <div class="col-12 col-xl-8">
        <div class="card card-body bg-white border-light shadow-sm mb-4">
          <h2 class="h5 mb-4">Thông tin thí sinh - ID số: 
            <span class="font-weight-bold text-danger">{{ $student->id }}</span>
          </h2>
          <!-- Họ và tên & email -->
          <div class="row">
            <div class="col-md-6 mb-3">
              <label for="name">{{ __('Họ và tên') }}</label>
              <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="name"
                value="{{ $student->name }}" autocomplete="name" placeholder="Trần Văn A">
              @error('name')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
              @enderror
            </div>
            <div class="col-md-6 mb-3">
              <div class="form-group">
                <label for="email">{{ __('Địa chỉ E-mail') }}</label>
                <input type="text" class="form-control @error('email') is-invalid @enderror" id="email" name="email"
                  placeholder="example@company.com" value="{{ $student->email }}" autocomplete="email">
                @error('email')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
                @enderror
              </div>
            </div>
          </div>

          <!-- Ngày sinh & giới tính -->
          <div class="row">
            <div class="col-md-6 mb-3">
              <label for="birthday">{{ __('Ngày sinh') }}</label>
              <div class="input-group">
                <span class="input-group-text"><span class="far fa-calendar-alt"></span></span>
                <input data-datepicker="" class="form-control" id="birthday" name="birthday" type="text"
                  placeholder="dd/mm/yyyy" value="{{ date('d-m-Y', strtotime($student->birthday)) }}">
              </div>
            </div>
            <div class="col-md-6 mb-3">
              <label for="gender">{{ __('Giới tính') }}</label>
              <select class="form-select mb-0" id="gender" aria-label="Gender select example" name="gender">
                <option value="nam" {{ $student->gender == 'nam' ? 'selected' : '' }}>Nam</option>
                <option value="nu" {{ $student->gender == 'nu' ? 'selected' : '' }}>Nữ</option>
              </select>
              @error('gender')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
              @enderror
            </div>
          </div>

          <!-- Đối tượng & mã giới thiệu -->
          <div class="row">
            <div class="col-md-6 mb-3">
              <label for="level">{{ __('Đối tượng') }}</label>
              <select class="form-select mb-0" id="level" aria-label="level select example" name="level">
                <option value="tot-nghiep-thcs" {{ $student->level == 'tot-nghiep-thcs' ? 'selected' : '' }}>
                  Tốt nghiệp THCS
                </option>
                <option value="dang-hoc-12" {{ $student->level == 'dang-hoc-12' ? 'selected' : '' }}>
                  Đang học 12
                </option>
                <option value="tot-nghiep-12" {{ $student->level == 'tot-nghiep-12' ? 'selected' : '' }}>
                  Tốt nghiệp 12
                </option>
                <option value="hoc-xong-12" {{ $student->level == 'hoc-xong-12' ? 'selected' : '' }}>
                  Học xong 12
                </option>
              </select>
              @error('level')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
              @enderror
            </div>
            <div class="col-md-6 mb-3">
              <label for="intro_code">{{ __('Mã người giới thiệu') }}</label>
              <input type="text" class="form-control @error('intro_code') is-invalid @enderror" name="intro_code"
                id="intro_code" value="{{ $student->intro_code }}" autocomplete="intro_code">
              @error('intro_code')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
              @enderror
            </div>
          </div>

          <!-- Số điện thoại -->
          <div class="row">
            <div class="col-md-6 mb-3">
              <label for="phone_number">{{ __('Số điện thoại thí sinh') }}</label>
              <input type="number" class="form-control @error('phone_number') is-invalid @enderror" name="phone_number"
                id="phone_number" value="{{ $student->phone_number }}" autocomplete="phone_number">
              @error('phone_number')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
              @enderror
            </div>
            <div class="col-md-6 mb-3">
              <label for="parent_phone_number">{{ __('Số điện thoại phụ huynh') }}</label>
              <input type="number" class="form-control @error('parent_phone_number') is-invalid @enderror"
                name="parent_phone_number" id="parent_phone_number" value="{{ $student->parent_phone_number }}"
                autocomplete="parent_phone_number">
              @error('parent_phone_number')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
              @enderror
            </div>
          </div>

          <!-- Tỉnh thành & trường học -->
          <div class="row">
            <div class="col-sm-6 mb-3">
              <label class="my-1 mr-2" for="province">{{ __('Tỉnh thành') }}</label>
              <select class="form-select" id="province" name="province_id" onchange="sch(this.value)">
                @foreach ($provinces as $province)
                <option value="{{ $province->id }}" @if ($student->province_id == $province->id) selected @endif>
                  {{ $province->name }}
                </option>
                @endforeach
              </select>
            </div>
            <div class="col-sm-6 mb-3">
              <label class="my-1 mr-2" for="school">{{ __('Trường học') }}</label>
              <select class="form-select" id="school" name="school_id">
                @foreach ($schools as $school)
                <option value="{{ $school->id }}" @if ($student->school_id == $school->id) selected @endif >
                  {{ $school->name }}
                </option>
                @endforeach
              </select>
            </div>
          </div>

          <!-- Bậc học & ngành học -->
          <div class="row">
            <div class="col-sm-6 mb-3">
              <label class="my-1 mr-2" for="step">{{ __('Bậc học') }}</label>
              <select class="form-select" id="step" name="step_id" onchange="mj(this.value)">
                @foreach ($steps as $step)
                <option value="{{ $step->id }}" @if ($student->step_id == $step->id) selected @endif>
                  {{ $step->name }}
                </option>
                @endforeach
              </select>
            </div>
            <div class="col-sm-6 mb-3">
              <label class="my-1 mr-2" for="major">{{ __('Ngành học') }}</label>
              <select class="form-select" id="major" name="major_id">
                @foreach ($majors as $major)
                <option value="{{ $major->id }}" @if ($student->major_id == $major->id) selected @endif>
                  {{ $major->name }}
                </option>
                @endforeach
              </select>
            </div>
          </div>

          <!-- Địa chỉ -->
          <div class="row">
            <div class="col-sm-12 mb-3">
              <label for="address">Địa chỉ nhận giấy báo nhập học</label>
              <textarea class="form-control" placeholder="Nhập chính xác địa chỉ nhận giấy báo" id="address"
                name="address" rows="4" spellcheck="false">{{ $student->address }}</textarea>
            </div>
          </div>

          <!-- Nút -->
          <div class="mt-3">
            <button type="submit" class="btn btn-secondary">
              {{ __('Cập nhật') }}
            </button>
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-form">
              {{ __('Gửi giấy báo') }}
            </button>
            <a href="{{ route('admin.student.index') }}" class="btn btn-primary">
              {{ __('Quay lại') }}
            </a>
          </div>
        </div>
      </div>
      <!-- end leftbar -->

      <!-- rightbar -->
      <div class="col-12 col-xl-4">
        <div class="card card-body bg-white border-light shadow-sm mb-4">
          <div class="row">
            <!-- Xác nhận gọi -->
            <div class="col-md-12 mb-3">
              <label for="call_confirm">{{ __('Xác nhận gọi') }}</label>
              <select class="form-select mb-0" id="call_confirm" aria-label="call_confirm select example" name="call_confirm">
                <option value="">-- Chọn --</option>
                <option value="goi-duoc" {{ $student->call_confirm == 'goi-duoc' ? 'selected' : '' }}>
                  Gọi được
                </option>
                <option value="khong-lien-lac-duoc" {{ $student->call_confirm == 'khong-lien-lac-duoc' ? 'selected' : '' }}>
                  Không liên lạc được
                </option>
              </select>
            </div>

            <!-- Mức độ quan tâm -->
            <div class="col-md-12 mb-3">
              <label for="care_level">{{ __('Mức độ quan tâm') }}</label>
              <select class="form-select mb-0" id="care_level" aria-label="care_level select example" name="care_level">
                <option value="">-- Chọn --</option>
                <option value="khong-quan-tam" {{ $student->care_level == 'khong-quan-tam' ? 'selected' : '' }}>
                  Không quan tâm
                </option>
                <option value="chac-chan-hoc" {{ $student->care_level == 'chac-chan-hoc' ? 'selected' : '' }}>
                  Chắc chắn học
                </option>
                <option value="do-dai-hoc" {{ $student->care_level == 'do-dai-hoc' ? 'selected' : '' }}>
                  Đổ đại học
                </option>
              </select>
              @error('gender')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
              @enderror
            </div>

            <!-- Yêu cầu nhắc lại -->
            <div class="col-sm-12 mb-3">
              <label for="advise">{{ __('Yêu cầu nhắc lại') }}</label>
              <input data-datepicker="" class="form-control" id="advise" name="advise" type="text" placeholder="dd/mm/yyyy"
                value="{{ $student->advise != null ? date('d-m-Y', strtotime($student->advise)) : '' }}" autocomplete="off">
            </div>

            <!-- Xác nhận gọi -->
            <div class="col-md-12 mb-3">
              <label for="print_cer">{{ __('In giấy báo') }}</label>
              <select class="form-select mb-0" id="print_cer" aria-label="print_cer select example" name="print_cer">
                <option value="">-- Chọn --</option>
                <option value="goi" {{ $student->print_cer == 'goi' ? 'selected' : '' }}>
                  Gởi
                </option>
                <option value="khong-goi" {{ $student->print_cer == 'khong-goi' ? 'selected' : '' }}>
                  Không gởi
                </option>
              </select>
            </div>

            <!-- Ghi chú -->
            <div class="col-sm-12 mb-3">
              <label for="note">Ghi chú</label>
              <textarea class="form-control" placeholder="Ghi chú của bạn" id="note" name="note" rows="4"
                spellcheck="false">{{ $student->note }}</textarea>
            </div>

            <!-- Lịch sử chỉnh sửa -->
            <div class="col-sm-12 mb-3">
              <label for="history">Lịch sử chỉnh sửa</label>
              <textarea class="form-control" placeholder="" id="history" name="history" rows="8" disabled
                spellcheck="false">{{ $student->history }}</textarea>
            </div>
          </div>
        </div>
      </div>
      <!-- end rightbar -->
    </div>
  </form>

  @include('admin.footer')
</main>
@endsection

@section('js')
<script>
  $(function() {
    $("#editUser").validate({
      errorClass: 'is-invalid',
      rules: {
        name: "required",
        province_id: "required",
        school_id: "required",
        step_id: "required",
        major_id: "required",
        birthday: "required",
        gender: "required",
        phone_number: "required",
        parent_phone_number: "required",
        email: {
          required: true,
          email: true
        },
        level: "required",
        address: "required",
      },
      messages: {
        name: "Vui lòng nhập tên của bạn",
        province_id: "Vui lòng chọn tỉnh thành của bạn",
        school_id: "Vui lòng chọn trường của bạn",
        step_id: "Vui lòng chọn bậc học của bạn",
        major_id: "Vui lòng chọn ngành học bạn mong muốn",
        birthday: "Vui lòng nhập ngày sinh của bạn",
        gender: "Vui lòng chọn giới tính của bạn",
        phone_number: "Vui lòng nhập số điện thoại của bạn",
        parent_phone_number: "Vui lòng nhập số điện thoại người thân của bạn",
        email: {
          required: "Vui lòng nhập mail của bạn",
          email: "Nhập đúng kiểu email",
        },
        level: "Vui lòng chọn trình độ của bạn",
        address: "Vui lòng nhập địa chỉ của bạn",
      },
      submitHandler: function(form) {
        form.submit();
      }
    });
  });

  function sch(id) {
    $.ajax({
      url: "{{ route('admin.student.ajaxSchool') }}",
      type: "GET",
      cache: false,
      data: {
        id: id,
      },
      success: function(data) {
        console.log(id);
        $("#school").replaceWith(data);
      },
      error: function() {
        console.log("Có lỗi");
      }
    });
  }

  function mj(id) {
    $.ajax({
      url: "{{ route('admin.student.ajaxMajor') }}",
      type: "GET",
      cache: false,
      data: {
        id: id,
      },
      success: function(data) {
        console.log(data);
        $("#major").replaceWith(data);
      },
      error: function() {
        console.log("Có lỗi");
      }
    });
  }
</script>
@endsection