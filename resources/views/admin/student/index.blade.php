@extends('admin.master')

@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="css/style.css">
@endsection

@section('content')
<!-- Modal -->
<div class="modal fade" id="modal-subscribe" tabindex="-1" role="dialog" aria-labelledby="modal-subscribe"
  aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-body text-center py-3">
        <h2 class="h4 mb-3">Chọn nhanh giáo viên</h2>

        <form action="{{ route('admin.student.quickAssign') }}" method="POST">
          @csrf
          <div class="row justify-content-center align-items-center mb-3">
            <div class="col-12 col-md-10 d-flex justify-content-center align-items-center">
              <label for="user_assign" class="m-0 w-25">{{ __('Chọn giáo viên') }}</label>
              <select class="form-select w-75" id="user_assign" name="user_assign">
                <option value="">-- Chọn --</option>
                @foreach($listUser as $user)
                <option value="{{ $user->id }}">
                  {{ $user->name }}
                </option>
                @endforeach
              </select>
            </div>
            <div class="col-12 col-md-2">
              <input class="btn btn-success btn-sm btn-block" type="submit" value="Lưu">
            </div>
          </div>
          <div class="row formTable">
            <div class="col-md-12">
              <table class="table" id="modalTable">
                <thead class="thead-river">
                  <tr>
                    <th style="font-size: 0.9rem !important;">
                      <input class="form-check-input" type="checkbox" id="selectAll">
                    </th>
                    <th style="font-size: 0.8rem !important;">Tên</th>
                    <th style="font-size: 0.8rem !important;">Tỉnh thành</th>
                    <th style="font-size: 0.8rem !important;">Chuyên ngành</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($quickAssignStudent as $student)
                  <tr>
                    <td>
                      <input class="form-check-input" type="checkbox" name="id[]" value="{{ $student->id }}">
                    </td>
                    <td>{{ $student->name }}</td>
                    <td>{{ $student->province->name }}</td>
                    <td>{{ $student->major->name }}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </form>

      </div>
    </div>
  </div>
</div>
<!-- end modal -->

<main class="content">

  @include('admin.header')

  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-4">
    <div class="btn-toolbar dropdown">
      <button class="btn btn-info btn-sm mr-2" data-toggle="modal" data-target="#modal-subscribe">
        <span class="fas fa-plus mr-2"></span>Chọn nhanh
      </button>
    </div>
  </div>

  <div class="row">
    <div class="col-sm-12">
      <div class="table-responsive py-4">
        <table class="table" id="myTable">
          <thead class="thead-light">
            <tr>
              <th>STT</th>
              <th>Tên</th>
              <th>Tỉnh thành</th>
              <th>Chuyên ngành</th>
              <th>Người tư vấn</th>
              <th>Trạng thái</th>
              <th>Ngày tạo</th>
              <th>action</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($listStudent as $student)
            @if ($student->user_id == null)
            <tr class="alert-danger" id="user-{{ $student->id }}">
              <td>{{ $loop->index+1 }}</td>
              <td>{{ $student->name }}</td>
              <td>{{ $student->province->name }}</td>
              <td>{{ $student->major->name }}</td>
              <td>
                <select class="form-select" id="role" name="role"
                  onchange="selectTeacher({{ $student->id }}, this.value)">
                  <option value="">-- Chọn --</option>
                  @foreach($listUser as $user)
                  <option value="{{ $user->id }}" @if ($user->id == $student->user_id) selected @endif>
                    {{ $user->name }}
                  </option>
                  @endforeach
                </select>
              </td>
              <td>
                <select class="form-select" id="status" name="status"
                  onchange="selectStatus({{ $student->id }}, this.value)">
                  <option value="">-- Chọn --</option>
                  @foreach($listStatus as $status)
                  <option value="{{ $status->id }}" @if ($status->id == $student->status_id) selected @endif>
                    {{ $status->name }}
                  </option>
                  @endforeach
                </select>
              </td>
              <td>
                {{ date('d-m-Y', strtotime($student->created_at)) }}
              </td>
              <td>
                <a class="btn btn-secondary py-1 px-2 font-small"
                  href="{{ route('admin.student.show', $student->id) }}">
                  <span class="font-weight-bold">Sửa</span>
                </a>
              </td>
            </tr>
            @else
            <tr class="alert-success" id="user-{{ $student->id }}">
              <td>{{ $loop->index+1 }}</td>
              <td>{{ $student->name }}</td>
              <td>{{ $student->province->name }}</td>
              <td>{{ $student->major->name }}</td>
              <td>
                <select class="form-select" id="role" name="role"
                  onchange="selectTeacher({{ $student->id }}, this.value)">
                  <option value="">-- Chọn --</option>
                  @foreach($listUser as $user)
                  <option value="{{ $user->id }}" @if ($user->id == $student->user_id) selected @endif>
                    {{ $user->name }}
                  </option>
                  @endforeach
                </select>
              </td>
              <td>
                <select class="form-select" id="status" name="status"
                  onchange="selectStatus({{ $student->id }}, this.value)">
                  <option value="">-- Chọn --</option>
                  @foreach($listStatus as $status)
                  <option value="{{ $status->id }}" @if ($status->id == $student->status_id) selected @endif>
                    {{ $status->name }}
                  </option>
                  @endforeach
                </select>
              </td>
              <td>
                {{ date('d-m-Y', strtotime($student->created_at)) }}
              </td>
              <td>
                <a class="btn btn-secondary py-1 px-2 font-small"
                  href="{{ route('admin.student.show', $student->id) }}">
                  <span class="font-weight-bold">Sửa</span>
                </a>
              </td>
            </tr>
            @endif
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>

  @include('admin.footer')
</main>
@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>

<script>
  $(document).ready(function () {
    $('#myTable').DataTable();
  });

  $(document).ready(function () {
    $('#modalTable').DataTable();
  });

  // change default number of rows
  $('#myTable').dataTable( {
    "pageLength": 50 // --> now set 25 row
  });

  $('#modalTable').dataTable( {
    "pageLength": 25 // --> now set 25 row
  });
</script>

<script>
  $('#selectAll').click(function(e) {
    if($(this).hasClass('checkedAll')) {
      $('input').prop('checked', false);   
      $(this).removeClass('checkedAll');
    } else {
      $('input').prop('checked', true);
      $(this).addClass('checkedAll');
    }
  });
</script>

<script>
  function selectTeacher(s_id, id) {
    $.ajax({
      url: "{{ route('admin.student.assign') }}",
      type: "GET",
      cache: false,
      data: {
        s_id: s_id,
        id: id,
      },
      success: function(data) {
        const e_danger = $("#user-"+s_id)[0].classList[0] == "alert-danger";
        if (id !== '' && e_danger === true) {
          $("#user-"+s_id).removeClass("alert-danger").addClass("alert-success");
        } else if (id !== '' && e_danger === false) {
          $("#user-"+s_id).removeClass("alert-danger").addClass("alert-success");
        } else {
          $("#user-"+s_id).removeClass("alert-success").addClass("alert-danger");
        }
        
      },
      error: function() {
        console.log("Có lỗi");
      }
    });
  }

  function selectStatus(s_id, id) {
    $.ajax({
      url: "{{ route('admin.student.status') }}",
      type: "GET",
      cache: false,
      data: {
        s_id: s_id,
        id: id,
      },
      success: function(data) {
        console.log(id);
      },
      error: function() {
        console.log("Có lỗi");
      }
    });
  }
</script>
@endsection