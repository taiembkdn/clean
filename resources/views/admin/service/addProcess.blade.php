@extends('templates.admin.master')
@section('content')
    
  <main id="main" class="main">

<div class="pagetitle">
  <h1>Thêm dịch vụ</h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="/admin">Dashboard</a></li>
      <li class="breadcrumb-item"><a href="{{ route('admin.service.index') }}">Dịch vụ</a></li>
      <li class="breadcrumb-item active">Thêm dịch vụ</li>
    </ol>
  </nav>
</div><!-- End Page Title -->

<section class="section">
  <div class="row">
    <div class="col-lg-12">

      <div class="card">
        <div class="card-body">
          <h5 class="card-title">Thêm dịch vụ mới</h5>

          <!-- General Form Elements -->
          <form action="{{ route('admin.service.addProcess', $id) }}" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
            <div class="row mb-3">
              <label for="title" class="col-sm-2 col-form-label">Tên tiến trình</label>
              <div class="col-sm-10">
                <input type="text" name="name" class="form-control" required value="{{ old('name') }}" id="title" />
              </div>
            </div>

            <div class="row mb-3">
              <label class="col-sm-2 col-form-label">Submit</label>
              <div class="col-sm-10">
                <button type="submit" class="btn btn-primary">Thêm</button>
              </div>
            </div>

          </form><!-- End General Form Elements -->

        </div>
      </div>

    </div>

  </div>
</section>

</main><!-- End #main -->
@endsection