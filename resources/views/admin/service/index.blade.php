@extends('templates.admin.master')
@section('content')
    

<main id="main" class="main">

<div class="pagetitle">
  <h1>Dịch vụ</h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="/admin">Dashboard</a></li>
      <li class="breadcrumb-item active">Dịch vụ</li>
    </ol>
  </nav>
</div><!-- End Page Title -->

<section class="section">
  <div class="row">
    <div class="col-lg-12">

      <div class="card">
        <div class="card-body">
            @include('errors.msg')
          <div class="py-2">
            <a href="{{ route('admin.service.create') }}" class="btn btn-success rounded-pill"><i class="bi bi-star me-1"></i> Thêm
              mới</a>
          </div>

          <!-- Table with stripped rows -->
          <table class="table datatable">
            <thead>
              <tr>
                <th scope="col">STT</th>
                <th scope="col">Tên dịch vụ</th>
                <th scope="col">Hình ảnh</th>
                <th scope="col">Thêm tiến trình</th>
                <th scope="col">Trạng thái</th>
                <th scope="col">Chức năng</th>
              </tr>
            </thead>
            <tbody>
            @foreach($services as $key => $service)
              <tr>
                <th scope="row">{{ $key + 1 }}</th>
                <td>{{ $service->title }}</td>
                <td><img class="object-fit" src="/upload/{{ $service->picture }}" height="150px" width="200px" /></td>
                <td>
                  @foreach($service->process as $process)
                    <p>
                      {{ str_limit($process->name, 50) }}
                      <a href="{{ route('admin.service.editProcess', $process->id) }}" class="btn btn-warning rounded-pill btn-sm text-white"><i
                      class="bi bi-pen"></i>
                    Sửa</a>
                      <a href="{{ route('admin.service.deleteProcess', $process->id) }}" class="btn btn-danger rounded-pill btn-sm"><i class="bi bi-trash"></i>
                    Xóa</a>
                    </p>
                  @endforeach
                  <a href="{{ route('admin.service.addProcess', $service->id) }}">Thêm tiến trình</a>
                </td>
                @if($service->active == 1)
                <td id="result-{{ $service->id }}"><a onclick="return getActive({{$service->id}})" class="text-success" href="javascript:void(0)">Có hiển thị</a></td>
                @else
                <td id="result-{{ $service->id }}"><a onclick="return getActive({{$service->id}})" class="text-danger" href="javascript:void(0)">Không hiển thị</a></td>
                @endif
                <td>
                  <a href="{{ route('admin.service.edit', $service->id) }}" class="btn btn-warning rounded-pill btn-sm text-white"><i
                      class="bi bi-pen"></i>
                    Sửa</a>
                  <a href="{{ route('admin.service.delete', $service->id) }}" class="btn btn-danger rounded-pill btn-sm"><i class="bi bi-trash"></i>
                    Xóa</a>
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
          <!-- End Table with stripped rows -->

        </div>
      </div>

    </div>
  </div>
</section>

</main><!-- End #main -->

<script>
      function getActive(id){
        $.ajax({
          url: "{{ route('admin.service.active') }}",
          type: 'GET',
          cache: false,
          data: {
                id: id,
            },
          success: function(data){
            console.log('success')
            $('#result-'+id).html(data);
          }, 
          error: function() {
           alert("Có lỗi");
         }
       }); 
        return false;
      }

</script>
@endsection