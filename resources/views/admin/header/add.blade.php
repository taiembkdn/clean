@extends('templates.admin.master')
@section('content')
    
  <main id="main" class="main">

<div class="pagetitle">
  <h1>Thêm trang seo</h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="/admin">Dashboard</a></li>
      <li class="breadcrumb-item"><a href="{{ route('admin.header.index') }}">header</a></li>
      <li class="breadcrumb-item active">Thêm trang seo</li>
    </ol>
  </nav>
</div><!-- End Page Title -->

<section class="section">
  <div class="row">
    <div class="col-lg-12">

      <div class="card">
        <div class="card-body">
          <h5 class="card-title">Thêm trang seo mới</h5>

          <!-- General Form Elements -->
          <form action="{{ route('admin.header.store') }}" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
            <div class="row mb-3">
              <label for="url" class="col-sm-2 col-form-label">Đường dẫn</label>
              <div class="col-sm-10">
                <input type="text" name="url" class="form-control" required value="{{ old('url') }}" id="url" />
              </div>
            </div>
                
            <div class="row mb-3">
              <label for="header1" class="col-sm-2 col-form-label">Chèn vào đầu trang</label>
              <div class="col-sm-10">
                <textarea class="form-control" name="header" style="height: 100px" id="header1">{{ old('header') }}</textarea>
              </div>
            </div>
                
            <div class="row mb-3">
              <label for="footer1" class="col-sm-2 col-form-label">Chèn vào cuối trang</label>
              <div class="col-sm-10">
                <textarea class="form-control" name="footer" style="height: 100px" id="footer1">{{ old('footer') }}</textarea>
              </div>
            </div>

            <div class="row mb-3">
              <label class="col-sm-2 col-form-label">Submit</label>
              <div class="col-sm-10">
                <button type="submit" class="btn btn-primary">Thêm</button>
              </div>
            </div>

          </form><!-- End General Form Elements -->

        </div>
      </div>

    </div>

  </div>
</section>

</main><!-- End #main -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/ckeditor/4.18.0/ckeditor.js"></script>

<script>
  var options = {
    filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
    filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
    filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
    filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
  };
</script>
<script>
CKEDITOR.replace('detail', options);
</script>

@endsection