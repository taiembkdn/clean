@extends('templates.admin.master')
@section('content')
    
  <main id="main" class="main">

<div class="pagetitle">
  <h1>Sửa trang SEO</h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="/admin">Dashboard</a></li>
      <li class="breadcrumb-item"><a href="{{ route('admin.header.index') }}">header</a></li>
      <li class="breadcrumb-item active">Sửa trang SEO</li>
    </ol>
  </nav>
</div><!-- End Page Title -->

<section class="section">
  <div class="row">
    <div class="col-lg-12">

      <div class="card">
        <div class="card-body">
          <h5 class="card-title">Sửa trang SEO</h5>

          <!-- General Form Elements -->
          <form action="{{ route('admin.header.update', $header->id) }}" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              @method('put')
            <div class="row mb-3">
              <label for="url" class="col-sm-2 col-form-label">Đường dẫn</label>
              <div class="col-sm-10">
                <input type="text" name="url" class="form-control" required value="{{ $header->url }}" id="url" />
              </div>
            </div>
                
            <div class="row mb-3">
              <label for="header1" class="col-sm-2 col-form-label">Header</label>
              <div class="col-sm-10">
                <textarea class="form-control" name="header" style="height: 100px" id="header1">{{ $header->header }}</textarea>
              </div>
            </div>
                
            <div class="row mb-3">
              <label for="footer1" class="col-sm-2 col-form-label">Footer</label>
              <div class="col-sm-10">
                <textarea class="form-control" name="footer" style="height: 100px" id="footer1">{{ $header->footer }}</textarea>
              </div>
            </div>

            <div class="row mb-3">
              <label class="col-sm-2 col-form-label">Submit</label>
              <div class="col-sm-10">
                <button type="submit" class="btn btn-primary">Sửa</button>
              </div>
            </div>

          </form><!-- End General Form Elements -->

        </div>
      </div>

    </div>

  </div>
</section>

</main><!-- End #main -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/ckeditor/4.18.0/ckeditor.js"></script>

<script>
  var options = {
    filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
    filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
    filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
    filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
  };
</script>
<script>
CKEDITOR.replace('detail', options);
</script>
@endsection