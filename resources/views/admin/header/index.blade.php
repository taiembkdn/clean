@extends('templates.admin.master')
@section('content')
    

<main id="main" class="main">

<div class="pagetitle">
  <h1>Quản lý SEO</h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="/admin">Dashboard</a></li>
      <li class="breadcrumb-item active">Quản lý SEO</li>
    </ol>
  </nav>
</div><!-- End Page Title -->

<section class="section">
  <div class="row">
    <div class="col-lg-12">

      <div class="card">
        <div class="card-body">
            @include('errors.msg')
          <div class="py-2">
            <a href="{{ route('admin.header.create') }}" class="btn btn-success rounded-pill"><i class="bi bi-star me-1"></i> Thêm
              mới</a>
          </div>

          <!-- Table with stripped rows -->
          <table class="table datatable">
            <thead>
              <tr>
                <th scope="col">STT</th>
                <th scope="col">Đường dẫn</th>
                <th scope="col">Chức năng</th>
              </tr>
            </thead>
            <tbody>
            @foreach($headers as $key => $header)
              <tr>
                <th scope="row">{{ $key + 1 }}</th>
                <td>{{ $header->url }}</td>
                <td>
                  <a href="{{ route('admin.header.edit', $header->id) }}" class="btn btn-warning rounded-pill btn-sm text-white"><i
                      class="bi bi-pen"></i>
                    Sửa</a>
                  <a href="{{ route('admin.header.delete', $header->id) }}" class="btn btn-danger rounded-pill btn-sm"><i class="bi bi-trash"></i>
                    Xóa</a>
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
          <!-- End Table with stripped rows -->

        </div>
      </div>

    </div>
  </div>
</section>

</main><!-- End #main -->
@endsection