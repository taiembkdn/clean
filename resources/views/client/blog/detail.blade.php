@extends('templates.client.master')
@section('content')

<section class="py-4 padding-top-head">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="single-blog-item">
                        <img class="w-100" src="/upload/{{ $blog->picture }}" alt="{{ $blog->title }}"
                            class="img-fluid img-avater">

                        <div class="blog-item-content mt-3">
                            <div class="blog-item-meta mb-3">
                                <span class="text-color-2 text-capitalize mr-3"><i class="bi bi-folder-fill"></i>
                                    Dự án</span>
                                <span class="text-muted text-capitalize mr-3"><i class="bi bi-calendar"></i>
                                {{ $blog->view }}    Lượt xem</span>
                                <span class="text-black text-capitalize mr-3"><i class="bi bi-calendar"></i>{{ date_format($blog->created_at, 'h:i:s d-m-Y') }}</span>
                            </div>

                            <h2 class="mb-4 text-md">{{ $blog->title }}</h2>

                            {!! $blog->detail !!}

                            <{{-- div class="mt-5 clearfix">
                                <ul class="float-left list-inline tag-option">
                                    <li class="list-inline-item"><a href="#">Giới thiệu</a></li>
                                    <li class="list-inline-item"><a href="#">Dịch vụ</a></li>
                                    <li class="list-inline-item"><a href="#">Liên hệ</a></li>
                                </ul>


                            </di --}}v>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">

                    <div class="service_orther mb-3">
                        <div class="title_procedure">Bài viết phổ biến</div>
                        <div class="underline_blue"></div>
                        <ul class="posts-list list-small list-unstyled">
                            @foreach($rand_blogs as $rand)
                            <li class="post cf">
                                <a href="#" class="image-link">
                                    <img width="99" height="66" src="/upload/{{ $rand->picture }}"
                                        class="attachment-smart-blog-thumb wp-post-image" alt="618"
                                        title="{{ $rand->title }}"> </a>
                                <div class="content">
                                    <a href="{{ route('client.blog.detail', $rand->slug) }}" class="title-link">{{ $rand->title }}</a>
                                    <div class="post-meta">
                                        <span class="post-cat">
                                            <a href="{{ route('client.blog.category', $rand->category->slug) }}">{{ $rand->category->name }}</a></span>
                                        <span class="meta-sep"></span>
                                        <time class="post-date" datetime="2015-10-22T19:11:55+00:00">{{ date_format($rand->created_at, 'h:i:s d-m-Y') }}</time>
                                    </div>
                                </div>
                            </li>
                            @endforeach

                        </ul>
                    </div>
                    <div class="service_orther mb-3">
                        <div class="title_procedure">Danh mục khác</div>
                        <div class="underline_blue"></div>
                        @foreach($categories as $category)
                        <div class="service_detail">
                           
                            <div class="service_content">
                                <a href="{{ route('client.blog.category', $category->slug) }}">{{ $category->name }}</a>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <div class="contact_hotline mb-3">
                        <div class="icon_box_hotline">
                            <i class="bi bi-telephone-inbound"></i>
                            <div class="number_phone">
                                <div class="content_phone">Chăm sóc khách hàng 24/7</div>
                                <div class="smal_content">Đối với dịch vụ khẩn cấp</div>
                            </div>
                        </div>
                        <a class="contact_number" href="{{ route('client.index.contact') }}">
                            <div class="contact_site"> <span>Liên hệ</span></div>
                        </a>
                        <div class="info_contact">
                            <p class="title_lienhe"><strong>Liên hệ</strong></p>
                            <p><strong>Tel:</strong> (+321)-123-45-678</p>
                            <p><strong>Email:</strong> Mailus@example.com</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('css')
<link rel="stylesheet" href="/templates/client/assets/css/blog.css">
@endsection

@section('seo')

@endsection