@extends('templates.client.master')
@section('content')

<section class="bannerHeader padding-top-head">
        <div class="my-container">
            <h3 class="underline">FreshHouse</h3>
            <h2>Blog </h2>
            <div class="breadcrumbs">
                <a href="/">Trang chủ</a> »
                <a href="">Blog </a>

                <img src="/templates/client/assets/img/bannerHeader.jpg">
            </div>
        </div>

    </section>
    <section class="py-4">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="row pt-4">
                        @foreach($blogs as $blog)
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="news_updates_inner">
                                <div class="image_inner">
                                    <div class="image_before">
                                        <a href="{{ route('client.blog.detail', $blog->slug) }}"><img width="770" height="420" src="/upload/{{ $blog->picture }}"
                                            class="img-fluid wp-post-image" alt=""></a><span
                                            class="flaticon-zoom flaticon-zoom-increasing-symbol"></span>
                                    </div>
                                </div>
                                <!--<div class="date">-->
                                <!--    <span>{{ date_format($blog->created_at, 'h:i:s d-m-Y') }}</span>-->
                                <!--</div>-->

                                <div class="content">
                                    <ul>
                                        <li><span class="flaticon-folder"><i class="bi bi-folder-fill"></i></span>{{ $blog->category->name }}</li>
                                        <li><span class="flaticon-comment"><i class="bi bi-eye"></i></span>{{ $blog->view }} Lượt xem
                                        </li>
                                    </ul>
                                    <h2><a href="{{ route('client.blog.detail', $blog->slug) }}">{{ $blog->title }}</a></h2>

                                </div>

                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-lg-4">

                    <div class="service_orther mt-3">
                        <div class="title_procedure">Bài viết phổ biến</div>
                        <div class="underline_blue"></div>
                        <ul class="posts-list list-small list-unstyled">
                            @foreach($rand_blogs as $rand)
                            <li class="post cf">
                                <a href="#" class="image-link">
                                    <img width="99" height="66" src="/upload/{{ $rand->picture }}"
                                        class="attachment-smart-blog-thumb wp-post-image" alt="618"
                                        title="{{ $rand->title }}"> </a>
                                <div class="content">
                                    <a href="{{ route('client.blog.detail', $rand->slug) }}" class="title-link">{{ $rand->title }}</a>
                                    <div class="post-meta">
                                        <span class="post-cat">
                                            <a href="{{ route('client.blog.category', $rand->category->slug) }}">{{ $rand->category->name }}</a></span>
                                        <span class="meta-sep"></span>
                                        <time class="post-date" >{{ date_format($rand->created_at, 'h:i:s d-m-Y') }}</time>
                                    </div>
                                </div>
                            </li>
                            @endforeach

                        </ul>
                    </div>
                    <div class="service_orther mt-3">
                        <div class="title_procedure">Dịch vụ khác</div>
                        <div class="underline_blue"></div>
                        @foreach($categories as $category)
                        <div class="service_detail">
                            <div class="service_content">
                                <a href="{{ route('client.blog.category', $category->slug) }}">{{ $category->name }}</a>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <div class="contact_hotline mt-3">
                        <div class="icon_box_hotline">
                            <i class="bi bi-telephone-inbound"></i>
                            <div class="number_phone">
                                <div class="content_phone">Chăm sóc khách hàng 24/7</div>
                                <div class="smal_content">Đối với dịch vụ khẩn cấp</div>
                            </div>
                        </div>
                        <a class="contact_number" href="{{ route('client.index.contact') }}">
                            <div class="contact_site"> <span>Liên hệ</span></div>
                        </a>
                        <div class="info_contact">
                            <p class="title_lienhe"><strong>Liên hệ</strong></p>
                            <p><strong>Tel:</strong> (+321)-123-45-678</p>
                            <p><strong>Email:</strong> Mailus@example.com</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('css')
<link rel="stylesheet" href="/templates/client/assets/css/blog.css">
@endsection

@section('seo')

@endsection