@extends('templates.client.master')
@section('content')

<section class="bannerHeader padding-top-head">
        <div class="my-container">
            <h3 class="underline"> Dịch vụ FreshHouse</h3>
            <h2> Reguler Clean - Lau Nhà Định Kỳ</h2>
            <div class="breadcrumbs">
                <a href="https://freshhouse.vn/">Trang chủ</a> »
                <a href="{{ route('client.service.category') }}">Dịch vụ</a> »
                <a> {{ $service->title }}</a>
                <img src="/templates/client/assets/img/bannerHeader.jpg">
            </div>

    </section>
    <section class="body_content py-4">
        <div class="container py-4">
            <div class="row">
                <div class="col-lg-6 imgIntro_tablet">
                    <div class="imgIntro">
                        <img src="/upload/{{ $service->picture }}" alt="">
                    </div>
                </div>
                <div class="col-lg-6">
                    <h4 class="body_content_intro">Giới thiệu</h4>
                    <h3 class="body_content_name ">Reguler Clean - Lau Nhà Định Kỳ</h3>
                    <div class="underline_blue"></div>
                    <div class="body_content_desc">
                       <p> {{ $service->description }}</p>
                    </div>
                    <ul class="highLight list-unstyled">
                        <li><i class="bi bi-star-fill"></i> Máy móc, dụng cụ hiện đại</li>
                        <li><i class="bi bi-star-fill"></i> Đảm bảo an toàn </li>
                        <li><i class="bi bi-star-fill"></i> Làm sạch hoàn toàn các vết bẩn</li>
                        <li><i class="bi bi-star-fill"></i> Làm khô ngay để khách sử dụng </li>
                    </ul>
                    <div class="body_content_contact">
                        <a class="book" href="/dat-lich/{{ $service->slug }}">Book Hẹn <i class="bi bi-chevron-right"></i></a>
                        <a class="contact" href="tel:1133"><i class="bi bi-telephone-fill"></i>Liên hệ chúng tôi</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container py-4">
            <div class="row">
                <div class="col-lg-8">
                    <div class="title_procedure">Quy trình</div>
                    <div class="procedure">
                        <ul class="list-unstyled">
                            @foreach($service->process as $key => $process)
                            <li>
                                <div class="icon-box ">
                                    <div class="icon-box-img">
                                        <div class="icon">
                                            <div class="icon-inner">
                                                <!--?xml version="1.0" encoding="UTF-8"?-->
                                                <svg width="50px" height="50px" viewBox="0 0 58 58" version="1.1"
                                                    xmlns="http://www.w3.org/2000/svg"
                                                    xmlns:xlink="http://www.w3.org/1999/xlink">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <g transform="translate(-766.000000, -1095.000000)">
                                                            <g transform="translate(766.000000, 1095.000000)">
                                                                <circle id="Oval" fill="#285A84" cx="29" cy="29" r="29">
                                                                </circle>
                                                                <text font-family="Helvetica" font-size="20"
                                                                    font-weight="normal" letter-spacing="-0.333333333"
                                                                    fill="#FFFFFF">
                                                                    <tspan x="18.7102865" y="44">0{{ $key+1 }}</tspan>
                                                                </text>
                                                                <text font-family="Helvetica" font-size="12"
                                                                    font-weight="normal" letter-spacing="-0.2"
                                                                    fill="#FFFFFF">
                                                                    <tspan x="14.6568359" y="21">Bước</tspan>
                                                                </text>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="icon-box-text ">
                                        <p>{{ $process->name }}.</p>
                                    </div>
                                </div>
                            </li>
                            @endforeach

                        </ul>
                    </div>
                    <div class="div_why">
                        <div class="row">
                            <div class="col-lg-3 text-center col-md-3 col-6">
                                <img class="w-25" src="/templates/client/assets/img/household-xanh.png" alt="">
                                <div class="heading-text">
                                    <strong>Dịch vụ</strong>
                                    <br>Nhân viên chuyên nghiệp<br>
                                </div>
                            </div>
                            <div class="col-lg-3 text-center col-md-3 col-6">
                                <img class="w-25" src="/templates/client/assets/img/growth-xanh.png" alt="">
                                <div class="heading-text">
                                    <strong>An tâm </strong>
                                    <br>Thỏa mãn yêu cầu <br>
                                </div>
                            </div>
                            <div class="col-lg-3 text-center col-md-3 col-6">
                                <img class="w-25" src="/templates/client/assets/img/line-xanh.png" alt="">
                                <div class="heading-text">
                                    <strong>An toàn</strong>
                                    <br>Sức khỏe - Môi trường <br>
                                </div>
                            </div>
                            <div class="col-lg-3 text-center col-md-3 col-6">
                                <img class="w-25" src="/templates/client/assets/img/money-xanh.png" alt="">
                                <div class="heading-text">
                                    <strong>Tiết kiệm </strong>
                                    <br>Giá ưu đãi đến 30%<br>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="title_procedure">ƯU ĐIỂM NỔI BẬT</div>
                    <div class="read-more js-read-more content_highLight" data-rm-words="480">
                        {!! $service->detail !!}
                    </div>
                </div>
                <div class="col-lg-4 col-md-7 d-md-none d-lg-block">
                    <div class="title_procedure">FRESHOUSE</div>
                    <div class="contact_hotline ">
                        <div class="icon_box_hotline">
                            <i class="bi bi-telephone-inbound"></i>
                            <div class="number_phone">
                                <div class="content_phone">Chăm sóc khách hàng 24/7</div>
                                <div class="smal_content">Đối với dịch vụ khẩn cấp</div>
                            </div>
                        </div>
                        <a class="contact_number" href="/dat-lich">
                            <div class="contact_site"> <span>Book hẹn</span></div>
                        </a>
                        <div class="info_contact">
                            <p class="title_lienhe"><strong>Liên hệ</strong></p>
                            <p><strong>Tel:</strong> 0983 19 15 87</p>
                            <p><strong>Email:</strong> vscnfreshhouse@gmail.com</p>
                        </div>
                    </div>
                    <div class="service_orther mt-3">
                        <div class="title_procedure">Dịch vụ khác</div>
                        <div class="underline_blue"></div>
                        <div class="service_detail">
                            <div class="service_img">
                                <img src="/templates/client/assets/img/Depositphotos_88779358_original.jpg" alt="">
                            </div>
                            <div class="service_content">
                                <a href="#">Move In/Out - Lau Dọn
                                    Phụ Chuyển Nhà</a>
                            </div>
                        </div>
                        <div class="service_detail">
                            <div class="service_img">
                                <img src="/templates/client/assets/img/Depositphotos_88779358_original.jpg" alt="">
                            </div>
                            <div class="service_content">
                                <a href="#"> Deep Clean - Đang Nghiên Cứu</a>
                            </div>
                        </div>
                    </div>
                    <div class="reasonsWhy mt-3">
                        <div class="reasonsWhy_content text-center">
                            <img src="/templates/client/assets/img/dich-vu-ve-sinh-theo-gio.jpg" class="w-100 avater_reason" alt="">
                            <img src="/templates/client/assets/img/line-xanh.png" width="75" alt="">
                            <div class="pt-3">
                                <span class="reasonsWhy_content-heading-text">
                                    <strong>100%</strong>
                                    <br>Bạn sẽ hài lòng dịch vụ<br>
                                    của chúng tôi</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('css')
<link rel="stylesheet" href="/templates/client/assets/css/detail-service.css">
@endsection

@section('seo')

@endsection