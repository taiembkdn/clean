@extends('templates.client.master')
@section('content')

  <style>
        .succes_gift {
            max-width: 1000px;
            margin: 30px auto;
            background-color: #e8eaf6;
            padding: 35px;
            border-radius: 20px;
        }

        .container_gift-card .payment_gift {
            background-image: -webkit-gradient(linear,
                    left top,
                    left bottom,
                    from(#f5c56b),
                    to(#ff8f2b));
            background-image: linear-gradient(180deg, #f5c56b, #ff8f2b);
            color: #fff;
            padding: 10px 20px;
            border-radius: 20px;
        }

        .succes_gift .box {
            padding: 30px 25px;
            background-color: white;
            border-radius: 15px;
        }
    </style>
    <div class="container container_gift-card  padding-top-head  ">
        <div class="row justify-content-center py-5">
            <div class="col-lg-8 py-5">
                <div class="succes_gift">
                    <div class="row ">
                        <div class=" col-12">
                            <div class="row box">
                                <div class="col-12 mb-4 text-center">


                                    <div class="box-right d-flex justify-content-center">
                                        <img src="./assets/img/Illustratioon-Office-1.png"
                                            class="w-25 d-none d-md-block" alt="">
                                        <img src="./assets/img/Illustratioon-Office-1.png"
                                            class="w-50 d-block d-md-none" alt="">
                                    </div>

                                    <p class="h1 py-3"> Cảm ơn bạn đã liên hệ! </p>
                                    <p class="mb-2">Chúng tôi đánh giá cao việc bạn liên hệ với chúng tôi
                                        qua việc booking để sử dụng dịch vụ. Một trong những thành viên của
                                        chúng
                                        tôi sẽ liên
                                        hệ lại với bạn trong thời gian ngắn.
                                    </p>
                                    <p class="mb-2">Cảm ơn trước sự kiên nhẫn của bạn.</p>
                                    <p class="mb-2">Chúc bạn có một ngày tuyệt vời!</p>
                                    <p></p>
                                    <div class=" pt-3">
                                        <a href="{{ route('client.service.success') }}" class="payment_gift"> Hoàn thành</a>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection


@section('seo')

@endsection