@extends('templates.client.master')
@section('content')

<section class="bannerHeader padding-top-head">
        <div class="my-container">
            <h3 class="underline">Book hẹn FreshHouse</h3>
            <h2>Book hẹn sử dụng dịch vụ</h2>
            <div class="breadcrumbs">
                <a href="/">Trang chủ</a> »
                <a href="{{ route('client.service.category') }}">Dịch vụ</a> »
                <a>Book hẹn</a>
                <img src="/templates/client/assets/img/bannerHeader.jpg">
            </div>
        </div>

    </section>
    <section class="booking py-4">
        <div class="container  ">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <form class="form_booking" action="{{ route('client.service.booking') }}" method="post">
                        @csrf
                        <div class="title text-center">
                            <h2>
                                Thông tin Book hẹn. </h2>
                            <h3>
                                Vài chi tiết để chúng tôi có thể hoàn thành
                                việc sử dụng dịch vụ của bạn.
                            </h3>
                        </div>
                        <div class="choose_service py-3 under_boder_bottom">
                            <div class="title_form_booking">CHỌN DỊCH VỤ</div>
                            <select name="service_id" class="form-select" aria-label="Default select example">
                                @foreach($services as $service)
                                @if($service->slug == $slug)
                                @php $selected = 'selected'; @endphp
                                @else
                                @php $selected = ''; @endphp
                                @endif

                                <option value="{{ $service->id }}" {{ $selected }}>{{ $service->title }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="detail_room py-3 under_boder_bottom">
                            <div class="title_form_booking">HÃY ĐƯA CHI TIẾT CĂN NHÀ CỦA BẠN</div>
                            <div class="row">
                                @foreach($rooms as $room)
                                <div class="col-lg-6 col-md-6 col-6 pb-3">
                                    <select name="room[]" class="form-select" aria-label="Default select example">
                                        @foreach($room->room as $rom)
                                        <option value="{{ $rom->id }}">{{ $rom->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="add_extras py-3 under_boder_bottom">
                            <div class="title_form_booking">YÊU CẦU ĐẶC BIỆT</div>
                            <div class="row">
                                @foreach($specials as $special)
                                <div class="col-lg-3 col-md-3 col-6">
                                    <div class="form-check">
                                        <input class="form-check-input d-none" name="special[]" type="checkbox" value="{{ $special->id }}"
                                            id="check-{{ $special->id }}">
                                        <label class="form-check-label" for="check-{{ $special->id }}">
                                            <div class="choose_extras">
                                                <div class="img_extras">
                                                    <img src="/upload/{{ $special->icon }}" alt="">
                                                </div>
                                                <div class="title_extras">
                                                    {{ $special->name }}
                                                </div>
                                            </div>

                                        </label>
                                    </div>
                                </div>
                                @endforeach
                                <div class="col-lg-12">
                                    <div class="mb-3">
                                        <label for="exampleInputText" class="form-label">Khác</label>
                                        <input type="text" class="form-control" name="other" id="orther"
                                            aria-describedby="emailHelp"
                                            placeholder="Ghi thêm yêu cầu đặc biệt ( Nếu có)">
                                        <div id="ortherlHelp" class="form-text"><small>Nếu có thêm các yêu cầu đặt biệt
                                                khác
                                                nào nằm ngoài dịch vụ trên. XIn vui lòng điền vào form ở trên.</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="times_service py-3 under_boder_bottom">
                            <div class="title_form_booking">THỜI GIAN BẠN MUỐN CHÚNG TÔI ĐẾN KHẢO SÁT</div>
                            <p>Chọn <strong>ngày và thời gian *</strong> phù hợp với bạn. Nếu bạn cần một cuộc hẹn vào
                                phút cuối, hãy gọi cho
                                chúng tôi theo số 1800 5678.
                            </p>
                            <p>* Xin lưu ý: Dưới đây là khoảng thời gian mà nhân viên dọn dẹp có thể đến.</p>
                            <div class="row justify-content-center">
                                <div class="col-lg-6 col-sm-6">
                                    <label class="form-control-label ">Chọn ngày</label>
                                    <input name="date" id="startDate" class="form-control" type="date" />
                                    <span id="startDateSelected"></span>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <label class="form-control-label">Chọn giờ</label>
                                    <input name="hour" type="text" class="form-control" name="orther" id="orther"
                                            aria-describedby="emailHelp"
                                            placeholder="Nhập thời gian nhân viên có thể dọn dẹp">
                                </div>
                            </div>
                        </div>
                        <div class="use_service py-3 under_boder_bottom">
                            <div class="title_form_booking">BẠN MUỐN SỬ DỤNG DỊCH VỤ CHÚNG TÔI TRONG BAO LÂU?</div>
                            <p>Đó là tất cả về việc phù hợp với bạn với sự sạch sẽ hoàn hảo cho ngôi nhà của bạn. Lập
                                lịch trình rất
                                linh hoạt. Hủy hoặc lên lịch lại bất cứ lúc nào.</p>
                            <div class="row">
                                <div class="col-md-3 col-6">
                                    <div class="form-check">
                                        <input class="form-check-input d-none" type="radio" name="calendar"
                                            id="flexRadioDefault1" checked value="1 lần">
                                        <label class="form-check-label" for="flexRadioDefault1">
                                            1 lần
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-3 col-6">
                                    <div class="form-check">
                                        <input class="form-check-input d-none" type="radio" name="calendar"
                                            id="flexRadioDefault2" value="Hàng tuần">
                                        <label class="form-check-label" for="flexRadioDefault2">
                                            Hàng tuần
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-3 pt-3 pt-md-0 pt-lg-0 col-6">
                                    <div class="form-check">
                                        <input class="form-check-input d-none" type="radio" name="calendar"
                                            id="flexRadioDefault3" value="2 tuần 1 lần">
                                        <label class="form-check-label" for="flexRadioDefault3">
                                            2 tuần 1 lần
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-3 pt-3 pt-md-0 pt-lg-0 col-6">
                                    <div class="form-check">
                                        <input class="form-check-input d-none" type="radio" name="calendar"
                                            id="flexRadioDefault4" value="Hàng tháng">
                                        <label class="form-check-label" for="flexRadioDefault4">
                                            Hàng tháng
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @if(Auth::guard('web')->user())
                        <div class="info_customer py-3 under_boder_bottom">
                            <div class="title_form_booking">THÔNG TIN CỦA BẠN</div>
                            <p>Thông tin này sẽ được sử dụng để liên hệ với bạn về dịch vụ của bạn.</p>
                            <div class="row">
                                <div class="col-12">
                                    <input type="text" name="name" class="form-control" placeholder="Họ và tên *" value="{{ Auth::guard('web')->user()->name }}" readonly>
                                </div>
                                <div class="col-6 pt-3">
                                    <input type="email" name="email" class="form-control" placeholder="Email*" value="{{ Auth::guard('web')->user()->email }}" readonly>
                                </div>
                                <div class="col-6 pt-3">
                                    <input type="text" name="phone_number" class="form-control" placeholder="Số điện thoại*" value="{{ Auth::guard('web')->user()->phone_number }}">
                                </div>
                            </div>
                        </div>
                        <div class="address_customer py-3 under_boder_bottom">
                            <div class="title_form_booking">ĐỊA CHỈ</div>
                            <p>Bạn muốn chúng tôi làm sạch ở đâu?</p>
                            <div class="row">
                                <div class="col-12">
                                    <input type="text" name="address" class="form-control" placeholder="Số nhà địa chỉ đường*" value="{{ Auth::guard('web')->user()->address }}">
                                </div>
                                <div class="col-6 pt-3">
                                    <input type="text" name="district" class="form-control" placeholder="Quận *" value="{{ Auth::guard('web')->user()->district }}">
                                </div>
                                <div class="col-6 pt-3">
                                    <input type="text" name="city" class="form-control" placeholder="Thành phố *" value="{{ Auth::guard('web')->user()->city }}">
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="info_customer py-3 under_boder_bottom">
                            <div class="title_form_booking">THÔNG TIN CỦA BẠN</div>
                            <p>Thông tin này sẽ được sử dụng để liên hệ với bạn về dịch vụ của bạn.</p>
                            <div class="row">
                                <div class="col-12">
                                    <input type="text" name="name" class="form-control" placeholder="Họ và tên *">
                                </div>
                                <div class="col-6 pt-3">
                                    <input type="email" name="email" class="form-control" placeholder="Email*">
                                </div>
                                <div class="col-6 pt-3">
                                    <input type="text" name="phone_number" class="form-control" placeholder="Số điện thoại*">
                                </div>
                            </div>
                        </div>
                        <div class="address_customer py-3 under_boder_bottom">
                            <div class="title_form_booking">ĐỊA CHỈ</div>
                            <p>Bạn muốn chúng tôi làm sạch ở đâu?</p>
                            <div class="row">
                                <div class="col-12">
                                    <input type="text" name="address" class="form-control" placeholder="Số nhà địa chỉ đường*">
                                </div>
                                <div class="col-6 pt-3">
                                    <input type="text" name="district" class="form-control" placeholder="Quận *">
                                </div>
                                <div class="col-6 pt-3">
                                    <input type="text" name="city" class="form-control" placeholder="Thành phố *">
                                </div>
                            </div>
                        </div>
                    @endif
                        <div class="why_service py-3 under_boder_bottom">
                            <div class="title_form_booking">BẠN BIẾT ĐẾN CHÚNG TÔI NHƯ THẾ NÀO?</div>
                            <div class="row">
                                <div class="col-12">
                                    <textarea rows="3" class="form-control" name="know"
                                        placeholder="Ví dụ: Người quen, Mạng xã hội, Google"></textarea>

                                </div>
                            </div>
                        </div>
                        <div class="other_service py-3 ">
                            <div class="title_form_booking">BẠN CÓ YÊU CẦU GÌ KHÁC</div>
                            <div class="row">
                                <div class="col-12">
                                    <textarea rows="5" class="form-control" name="other_request"
                                        placeholder="Nếu bạn có những yêu cầu nào khác bạn vui lòng điền ở đây..."></textarea>

                                </div>
                            </div>
                        </div>
                        <div class="submit_formbooking">
                            <button class="contact_number" type="submit">
                                <div class="contact_site"> <span>Book hẹn</span></div>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('css')
<link rel="stylesheet" href="/templates/client/assets/css/book.css">
@endsection

@section('seo')

@endsection