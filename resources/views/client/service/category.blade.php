@extends('templates.client.master')
@section('content')

<section class="bannerHeader padding-top-head">
        <div class="my-container">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7">

                        <h2>Cung cấp Dịch vụ Vệ sinh &
                            Vệ sinh Công trình Chất lượng
                            Cho Mọi Người.</h2>
                        <p class="text-white">
                            Công ty FreshHouse luôn coi khách hàng là người thân, môi trường của khách hàng
                            cũng là môi
                            trường của
                            chúng tôi. Đối với chúng tôi, học hỏi không phải ở một khách hàng trong một thời
                            điểm nào đó
                            mà
                            là học
                            hỏi ở nhiều khách hàng trong suốt một quá trình dài.
                        </p>
                        <div class="cus_button">
                            <a class="contact_number" href="#second_dev">
                                <div class="contact_site"> <span>Book Hẹn</span></div>
                            </a>
                        </div>

                        <div class="breadcrumbs">
                            <a href="https://freshhouse.vn/">Trang chủ</a> »
                            <a>Dịch vụ</a>
                            <img src="/templates/client/assets/img/9.png">
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <section class="section_service" id="second_dev">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="heading">
                        <div class="h-title">DỊCH VỤ CỦA CHÚNG TÔI</div>
                        <div class="border_line"></div>
                        <div class="sub_heading">Dịch vụ FreshHouse cung cấp cho bạn</div>
                        <p class="sub_desc">Với đội ngũ nhân viên được tuyển chọn &amp; huấn luyện chuyên nghiệp chúng
                            tôi
                            <br>
                            hi vọng sẽ mang đến sự hài lòng cho quý khách!
                        </p>
                    </div>
                </div>
                @foreach($services as $service)
                <div class="col-lg-4 col-md-6 pb-3">
                    <div class="single-service-box">
                        <div class="service-thumb">
                            <a href="{{ route('client.service.detail', $service->slug) }}"><img src="/upload/{{ $service->picture }}" alt="{{ $service->title }}"></a>
                        </div>
                        <div class="service-content ">
                            <div class="service-title">
                                <a href="{{ route('client.service.detail', $service->slug) }}" tabindex="0">{{ $service->title }}</a>
                            </div>
                            <div class="service-desc">
                                <p class="mb-1">
                                    {{ $service->description }}
                                </p>
                            </div>
                            <div class=" py-2">

                                <div class="div_button div_button_contact d-flex justify-content-start">
                                    <a href="{{ route('client.service.detail', $service->slug) }}">
                                        Chi tiết dịch vụ <i class="bi bi-chevron-right"></i>
                                    </a>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
                @endforeach
            </div>

        </div>
        <div class="container py-5">
            <div class="row">
                <div class="col-lg-8">
                    <div class="div_why">
                        <div class="row">
                            <div class="col-lg-3 text-center col-md-3 col-6">
                                <img class="w-25" src="/templates/client/assets/img/household-xanh.png" alt="">
                                <div class="heading-text">
                                    <strong>Dịch vụ</strong>
                                    <br>Nhân viên chuyên nghiệp<br>
                                </div>
                            </div>
                            <div class="col-lg-3 text-center col-md-3 col-6">
                                <img class="w-25" src="/templates/client/assets/img/growth-xanh.png" alt="">
                                <div class="heading-text">
                                    <strong>An tâm </strong>
                                    <br>Thỏa mãn yêu cầu <br>
                                </div>
                            </div>
                            <div class="col-lg-3 text-center col-md-3 col-6">
                                <img class="w-25" src="/templates/client/assets/img/line-xanh.png" alt="">
                                <div class="heading-text">
                                    <strong>An toàn</strong>
                                    <br>Sức khỏe - Môi trường <br>
                                </div>
                            </div>
                            <div class="col-lg-3 text-center col-md-3 col-6">
                                <img class="w-25" src="/templates/client/assets/img/money-xanh.png" alt="">
                                <div class="heading-text">
                                    <strong>Tiết kiệm </strong>
                                    <br>Giá ưu đãi đến 30%<br>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="freshHouse">
                        <div class="freshHouse_cons">
                            <div class="img_fresh">
                                <img src="/templates/client/assets/img/apartment-cleaning-hero-iamge_orig.jpg" alt="">
                            </div>
                            <div class="text_fresh">
                                <div class="text_fresh_title ">
                                    FreshHouse
                                </div>
                                <div class="underline_blue_center" style="margin:7px 0;"></div>

                                <p>Trước tiên Thay mặt toàn thể thành viên FreshHouse xin gởi đến quý khách lời chúc
                                    Bình An – Hạnh Phúc – Thịnh Vượng.</p>
                                <p>Thấu hiểu điều đó, FreshHouse ra đời với mong muốn chia sẻ gánh nặng cùng quý khách
                                    qua những công việc “không tên” nhằm giúp quý khách “có thêm
                                    thời gian cho cuộc sống.</p>
                                <div class="about-list mt-none-25">
                                    <div class="single-item d-flex align-items-center mt-25">
                                        <div class="icon">
                                            <i class="bi bi-check2-circle"></i>
                                        </div>
                                        <span> Tránh lãng phí vì giá cả linh hoạt theo giờ</span>
                                    </div>
                                    <div class="single-item d-flex align-items-center mt-25">
                                        <div class="icon">
                                            <i class="bi bi-check2-circle"></i>
                                        </div>
                                        <span> Đảm bảo không gian sống riêng tư, thoải mái</span>
                                    </div>
                                    <div class="single-item d-flex align-items-center mt-25">
                                        <div class="icon">
                                            <i class="bi bi-check2-circle"></i>
                                        </div>
                                        <span> Nguồn gốc nhân viên rõ ràng</span>
                                    </div>
                                    <div class="single-item d-flex align-items-center mt-25">
                                        <div class="icon">
                                            <i class="bi bi-check2-circle"></i>
                                        </div>
                                        <span> Không mất thời gian tìm người</span>
                                    </div>
                                    <div class="single-item d-flex align-items-center mt-25">
                                        <div class="icon">
                                            <i class="bi bi-check2-circle"></i>
                                        </div>
                                        <span> Có thêm thời gian tận hưởng cuộc sống</span>
                                    </div>
                                    <div class="single-item d-flex align-items-center mt-25">
                                        <div class="icon">
                                            <i class="bi bi-check2-circle"></i>
                                        </div>
                                        <span> Làm sạch hoàn toàn các vết bẩn</span>
                                    </div>
                                </div>
                                <div class="div_button div_button-2 pb-3">
                                    <a href="{{ route('client.index.about') }}">
                                        Dịch vụ của chúng tôi <i class="bi bi-chevron-right"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="about-us-area">
                        <div class="right-info-box">

                            <div class="top">

                                <div class="iocn-holder">

                                    <span class="flaticon-24-hours-3"><i class="bi bi-telephone-fill"></i></span>

                                </div>

                                <div class="text-holder">

                                    <div class="h3">Chăm sóc khách hàng 24/7</div>
                                    <span>Đối với dịch vụ khẩn cấp</span>

                                </div>

                            </div>

                            <div class="middle">

                                <div class="h1">0983 19 15 87</div>

                                <p>Chúng tôi có thể cung cấp Dịch
                                    vụ khẩn cấp 24 giờ chuyên nghiệp,
                                    Liên hệ khi bạn cần !.</p>

                            </div>

                            <div class="bottom">

                                <div class="h4">Liên hệ:</div>

                                <ul>

                                    <li><span>Tel:</span> 0983 19 15 87</li>

                                    <li><span>Email:</span> vscnfreshhouse@gmail.com </li>

                                </ul>

                            </div>

                        </div>
                    </div>
                    <div class="reasonsWhy mt-3">
                        <div class="reasonsWhy_content text-center">
                            <img src="/templates/client/assets/img/line-xanh.png" width="75" alt="">
                            <div class="pt-3">
                                <span class="reasonsWhy_content-heading-text">
                                    <strong>100%</strong>
                                    <br>Bạn sẽ hài lòng dịch vụ<br>
                                    của chúng tôi</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('css')
<link rel="stylesheet" href="/templates/client/assets/css/service.css">
@endsection

@section('seo')

@endsection