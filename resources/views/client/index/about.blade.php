@extends('templates.client.master')
@section('content')

<section class="bannerHeader padding-top-head">
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <img src="/templates/client/assets/img/houseCleaning-Img.png" class="w-100" alt="">
                </div>
                <div class="col-lg-7">
                    <div class="intro_about">
                        ABOUT
                    </div>
                    <div class="intro_content">
                        <div class="intro_us">US</div>
                        <div class="intro_desc">
                            <p>“Mang không gian xanh - sạch - đẹp đến ngôi nhà bạn”</p>
                            <p>Quý khách hàng kính mến!</p>
                            <p>
                                Trước tiên Thay mặt toàn thể thành viên FreshHouse xin gởi đến quý khách
                                lời chúc Bình An – Hạnh Phúc – Thịnh Vượng. Kính thưa quý khách, cuộc
                                sống hiện đại ngày càng bận rộn, áp lực công việc ngày một gia tăng. Dường
                                như chúng ta không có đủ thời gian để vừa làm tốt công việc nơi công sở
                                vừa làm tròn trách nhiệm con, dâu, cha, mẹ trong gia đình.
                            </p>
                            <p>Thấu hiểu điều đó, FreshHouse ra đời với mong muốn chia sẻ gánh nặng cùng
                                quý khách qua những công việc “không tên” nhằm giúp quý khách “có thêm
                                thời gian cho cuộc sống.</p>
                            <div class="authour">

                                <img src="/templates/client/assets/img/welcome_sign (1).png" class="img-fluid" alt="Awesome Image">

                                <div class="text">

                                    <h2>FreshHouse</h2>

                                    <p>CEO &amp; Founder of FreshHouse</p>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <section>

        <div class="container">
            <div class="we_intro">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="fl-rich-text">
                            <div class="title_about_intro text-center">
                                Về chúng tôi
                            </div>
                            <div class="underline_blue_center"></div>
                            <p> Bỏ qua một số công việc lớn khỏi danh sách việc cần làm của bạn bằng cách để chúng tôi
                                lo việc dọn dẹp nhà cửa. Sau đó, hãy tận hưởng niềm vui khi biết toàn bộ ngôi nhà của
                                bạn đã được dọn dẹp bởi (những) thành viên trong nhóm chuyên nghiệp mà bạn có thể tin
                                tưởng. </p>
                            <p> Dịch vụ vệ sinh của chúng tôi rất kỹ lưỡng, nhất quán và tùy chỉnh. Nếu bạn muốn yêu cầu
                                một dịch vụ đặc biệt, thay đổi lịch trình dọn dẹp của bạn hoặc bỏ qua một khu vực trong
                                nhà của bạn, hãy cho chúng tôi biết! Chúng tôi sẵn lòng đáp ứng mọi yêu cầu để đáp ứng
                                hơn cả sự mong đợi của bạn. </p>
                            <p> Dịch vụ dọn dẹp nhà cửa của FreshHouse được cung cấp hàng tuần, cách tuần, hàng tháng
                                hoặc
                                một lần. Mỗi lần đến thăm, nhóm FreshHouse của bạn sẽ quét dọn, hút bụi, giặt giũ và vệ
                                sinh
                                từng phòng. Sử dụng thiết bị của chúng tôi và các sản phẩm có công thức đặc biệt, chúng
                                được làm sạch từ trái sang phải, từ trên xuống dưới, vì vậy không có chi tiết nào bị bỏ
                                sót. </p>
                        </div>
                    </div>
                    <div class="col-lg-4 ">
                        <div class="about-us-area">
                            <div class="right-info-box">

                                <div class="top">

                                    <div class="iocn-holder">

                                        <span class="flaticon-24-hours-3"><i class="bi bi-telephone-fill"></i></span>

                                    </div>

                                    <div class="text-holder">

                                        <div class="h3">Chăm sóc khách hàng 24/7</div>
                                        <span>Đối với dịch vụ khẩn cấp</span>

                                    </div>

                                </div>

                                <div class="middle">

                                    <div class="h1">0983 19 15 87</div>

                                    <p>Chúng tôi có thể cung cấp Dịch
                                        vụ khẩn cấp 24 giờ chuyên nghiệp,
                                        Liên hệ khi bạn cần !.</p>

                                </div>

                                <div class="bottom">

                                    <div class="h4">Liên hệ:</div>

                                    <ul>

                                        <li><span>Tel:</span> 0983 19 15 87</li>

                                        <li><span>Email:</span> vscnfreshhouse@gmail.com </li>

                                    </ul>

                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="container py-5">

            <div class="row justify-content-center pt-5">
                <div class="col-lg-5 d-flex justify-content-center">
                    <div class="img_about d-flex justify-content-center pb-3">
                        <img src="/templates/client/assets/img/apartment-cleaning-hero-iamge_orig.jpg" alt="">
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="title_about_intro ">
                        FreshHouse
                    </div>
                    <div class="underline_blue_center" style="margin: 0;"></div>
                    <div class="div_button pb-1 pt-1"></div>
                    <p>Với đội ngũ nhân viên trẻ, chuyên nghiệp chắc chắn sẽ làm hài lòng quý khách hàng và đóng góp vào
                        tốc độ phát triển của ca nước.</p>
                    <p><strong>Cung cấp dịch vụ vệ sinh thường xuyên: </strong>công ty FreshHouse là một trong những
                        công ty có bề dày kinh nghiệm về các dịch vụ vệ sinh công nghiệp:
                        cung cấp dịch vụ vệ sinh thường xuyên và định kỳ trọn gói cho Bệnh Viện, Cao Ốc Văn Phòng, Siêu
                        Thị – Trung Tâm Thương Mại, Khu Công Nghiệp, Khu Du Lịch, Trường Học Và Căn Hộ bao gồm: nhân
                        viên chuyên nghiệp, máy móc thiết bị hiện đại và hóa chất chuyên dùng.</p>
                    <p><strong>Kiểm soát chất lượng: </strong>Trong thực tế, chúng tôi tự hào là chúng tôi đang liên tục
                        theo dõi hiệu suất làm việc. Chúng tôi cố gắng để có những nhân viên tốt nhất, các trang thiết
                        bị tốt nhất, đào tạo tốt nhất và hệ thống hồ sơ tốt nhất để chúng tôi có thể cung cấp những dịch
                        vụ tốt nhất</p>
                    <p><strong>Trang thiết bị, máy móc: </strong>Chúng tôi chỉ sử dụng những sản phẩm, thiết bị có hiệu
                        suất cao, có tác động tối thiểu đến môi trường làm việc xung quanh và tuân thủ tiêu chuẩn an
                        toàn Sức khỏe.</p>

                    <p><strong>Lợi ích của khách hàng: </strong>Đội ngũ quản lý giàu kinh nghiệm. Tập trung vào sự hài
                        lòng của khách hàng. 24 / 7 sẵn có và các dịch vụ khẩn cấp toàn diện. Một đội ngũ hoàn toàn
                        chuyên nghiệp. Chúng tôi liên tục cải tiến các dịch vụ và hệ thống.</p>
                    <div class="div_button pb-3">
                        <a href="/dich-vu">
                            Dịch vụ của chúng tôi <i class="bi bi-chevron-right"></i>
                        </a>
                    </div>

                </div>
            </div>
            <div class="row pt-4">
                <div class="col-12">
                    <div class="title_about_intro ">
                        Con người FreshHouse
                    </div>
                    <div class="underline_blue_center" style="margin: 0;"></div>
                    <p class="py-3">Tôn trọng sự “khác biệt” để làm nên đột phá, chúng tôi với đội ngũ nhân sự trẻ, giàu
                        kinh nghiệm
                        coi khả năng “sáng tạo không ngừng” là yếu tố cốt lõi tạo nên sự thành công, là tiền đề hướng
                        đến tương lai phát triển cho đối tác, cộng đồng và xã hội.</p>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-3">
                    <a href="" class="pxp-agents-1-item">
                        <div class="pxp-agents-1-item-fig-container rounded-lg">
                            <div class="pxp-agents-1-item-fig pxp-cover"
                                style="background-image: url(/templates/client/assets/img/Depositphotos_88779358_original.jpg); background-position: top center;    background-size: cover;">
                            </div>
                        </div>
                        <div class="pxp-agents-1-item-details rounded-lg">
                            <div class="pxp-agents-1-item-details-name">Chuyên Nghiệp</div>


                        </div>

                    </a>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-3">
                    <a href="" class="pxp-agents-1-item">
                        <div class="pxp-agents-1-item-fig-container rounded-lg">
                            <div class="pxp-agents-1-item-fig pxp-cover"
                                style="background-image: url(/templates/client/assets/img/Depositphotos_89551394_original.jpg); background-position: top center;    background-size: cover;">
                            </div>
                        </div>
                        <div class="pxp-agents-1-item-details rounded-lg">
                            <div class="pxp-agents-1-item-details-name">Nhiệt huyết</div>


                        </div>

                    </a>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-3">
                    <a href="" class="pxp-agents-1-item">
                        <div class="pxp-agents-1-item-fig-container rounded-lg">
                            <div class="pxp-agents-1-item-fig pxp-cover"
                                style="background-image: url(/templates/client/assets/img/Depositphotos_28062927_original.jpg); background-position: top center;    background-size: cover;">
                            </div>
                        </div>
                        <div class="pxp-agents-1-item-details rounded-lg">
                            <div class="pxp-agents-1-item-details-name">Năng động</div>


                        </div>

                    </a>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-3">
                    <a href="" class="pxp-agents-1-item">
                        <div class="pxp-agents-1-item-fig-container rounded-lg">
                            <div class="pxp-agents-1-item-fig pxp-cover"
                                style="background-image: url(/templates/client/assets/img/Depositphotos_95376934_original.jpg); background-position: top center;    background-size: cover;">
                            </div>
                        </div>
                        <div class="pxp-agents-1-item-details rounded-lg">
                            <div class="pxp-agents-1-item-details-name">Đoàn kết</div>


                        </div>

                    </a>
                </div>
            </div>
        </div>

    </section>
@endsection

@section('css')
<link rel="stylesheet" href="/templates/client/assets/css/about.css">
@endsection

@section('seo')

@endsection