@extends('templates.client.master')
@section('content')

<section class="bannerHeader padding-top-head">
        <div class="my-container">
            <h3 class="underline">FreshHouse</h3>
            <h2>Liên hệ với chúng tôi</h2>
            <div class="breadcrumbs">
                <a href="https://freshhouse.vn/">Trang chủ</a> »
                <a>Liên hệ</a>
                <img src="/templates/client/assets/img/bannerHeader.jpg">
            </div>
        </div>

    </section>
 <section class="testimonial_section">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 py-5 px-3 px-lg-5">
                    <div class="heading">
                        <div class="h-title">LIÊN HỆ</div>
                        <div class="border_line"></div>
                        <div class="sub_heading">Chúng tôi rất muốn lắng nghe ý kiến của bạn</div>
                        <p class="sub_desc">Chúng tôi sẽ phản hồi kịp thời các nhu cầu dịch vụ của bạn.<br> Đừng ngần
                            ngại khi gọi hoặc gửi tin nhắn.
                        </p>
                        <div class="accord-content " style="display: block;">
                            <ul class="contact-info-list">
                                <li>
                                    <div class="icon-holder">
                                        <i class="bi bi-geo-alt-fill"></i>
                                    </div>
                                    <div class="text-holder">
                                        <div><span>183 Quách Thị Trang,</span><br> Cảm Lệ, Tp. Đà Nẵng

                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="icon-holder">
                                        <i class="bi bi-telephone-fill"></i>
                                    </div>
                                    <div class="text-holder">
                                        <div><span>Liên hệ</span><br>0983 19 15 87</div>
                                    </div>
                                </li>
                                <li>
                                    <div class="icon-holder">
                                        <i class="bi bi-envelope-fill"></i>
                                    </div>
                                    <div class="text-holder">
                                        <div><span>Gửi mail</span><br>vscnfreshhouse@gmail.com</div>
                                    </div>
                                </li>
                                <li>
                                    <div class="icon-holder">
                                        <i class="bi bi-clock-fill"></i>
                                    </div>
                                    <div class="text-holder">
                                        <div><span>Thời gian</span><br>Chủ nhật: 09.00am to 18.00pm</div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 ">
                    <div class="schedule_appointment">
                        <div class="appointment_inner">
                            <div class="topic">
                                <div>Gửi yêu cầu</div>
                            </div>
@include('errors.msg')
<form action="{{ route('client.index.contact') }}" method="post">
    @csrf
    <div class="form_inner">
        <div class="row">
            <div class="col-12">
                <input type="text" class="form-control" placeholder="Họ và tên *" name="name" value="{{ old('name') }}">
                <p class="text-danger">@if($errors->has('name')){{ $errors->first('name') }}@endif</p>
            </div>
            <div class="col-6 pt-3">
                <input type="text" class="form-control" placeholder="Số điện thoại *" name="phone"
                    value="{{ old('phone') }}">
                <p class="text-danger">@if($errors->has('phone')){{ $errors->first('phone') }}@endif</p>
            </div>
            <div class="col-6 pt-3">
                <input type="email" class="form-control" placeholder="Email của bạn*" name="email"
                    value="{{ old('email') }}">
                <p class="text-danger">@if($errors->has('email')){{ $errors->first('email') }}@endif</p>
            </div>
            <div class="col-12 pt-3">
                <input type="text" class="form-control" placeholder="Tiêu đề*" name="title" value="{{ old('title') }}">
                <p class="text-danger">@if($errors->has('title')){{ $errors->first('title') }}@endif</p>
            </div>
            <div class="col-12 pt-3">
                <textarea name="description" class="form-control" rows="3"
                    placeholder="Nội dung đề cập*">{{ old('description') }}</textarea>
                <p class="text-danger">@if($errors->has('description')){{ $errors->first('description') }}@endif</p>
            </div>
            <div class="col-12">
                <div class="col-12 pt-3">
                    <div class="row">
                        <div class="col-lg-4 col-md-5 col-sm-12">
                            <div class="form-group">
                                <button type="submit" class="wpcf7-form-control wpcf7-submit theme-btn"><span
                                        class="ajax-loader"> Gửi tin nhắn</span></button>
                            </div>
                        </div>
                        <div class="col-lg-8 col-md-7 col-sm-12 d-flex align-items-center">
                            <p class="mb-0">* Nhóm hỗ trợ khách hàng của chúng tôi sẽ liên hệ
                                với bạn trong vòng 24 giờ.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
            <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <iframe
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3826.0218019895424!2d107.60017341481321!3d16.474433688631642!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3141a1194a10b7e1%3A0x8a8d920e21c41183!2zTMawdSBI4buvdSBQaMaw4bubYywgVuG7uSBE4bqhLCBUaMOgbmggcGjhu5EgSHXhur8sIFRo4burYSBUaGnDqm4gSHXhur8sIFZp4buHdCBOYW0!5e0!3m2!1svi!2s!4v1650955919628!5m2!1svi!2s"
                    width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"
                    referrerpolicy="no-referrer-when-downgrade"></iframe>
            </div>
        </div>
    </div>
    </section>
@endsection

@section('css')
<link rel="stylesheet" href="/templates/client/assets/css/contact.css">
<link rel="stylesheet" href="/templates/client/assets/css/index.css">
@endsection

@section('seo')

@endsection