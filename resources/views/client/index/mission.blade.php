@extends('templates.client.master')
@section('content')

<section class="bannerHeader padding-top-head">
        <div class="my-container">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-4">
                        <img class="w-100 img-baner" src="/templates/client/assets/img/Illustratioon-Office-1.png" alt="">
                    </div>
                    <div class="col-lg-8 col-md-8 text-left">


                        <div class="container">
                            <div class="row justify-content-start">
                                <div class="col-lg-12 col-12 col-md-12  text-left">
                                    <h3 class="underline"> FreshHouse</h3>
                                    <h2>Sứ Mệnh của FreshHouse</h2>
                                    <p class="text-white">
                                        Công ty FreshHouse luôn coi khách hàng là người thân, môi trường của khách hàng
                                        cũng là môi
                                        trường của
                                        chúng tôi. Đối với chúng tôi, học hỏi không phải ở một khách hàng trong một thời
                                        điểm nào đó
                                        mà
                                        là học
                                        hỏi ở nhiều khách hàng trong suốt một quá trình dài.

                                    </p>
                                    <div class="cus_button">
                                        <a class="contact_number" href="#">
                                            <div class="contact_site"> <span>Liên hệ</span></div>
                                        </a>
                                    </div>
                                    <div class="breadcrumbs">
                                        <a href="https://freshhouse.vn/">Trang chủ</a> »
                                        <a >Sứ Mệnh của FreshHouse</a>
                                        <img class="d-none d-md-block" src="/templates/client/assets/img/8 (1).png">
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </section>
    <section class="body_content py-5">
        <div class="container">
            <div class="row">
                <div class="col-12 pb-3">
                    <div class="widgettitle">TẦM NHÌN, SỨ MỆNH VÀ GIÁ TRỊ CỐT LÕI
                    </div>
                </div>
                <div class="col-lg-3 col-md-4">
                    <div class="headding_tabs">
                        FreshHouse
                    </div>
                    <ul class="nav vav_mission mb-3" id="myTab" role="tablist">
                        <li class="nav-item active"><a class="nav-link active" href="#home" role="tab"
                                aria-controls="home" data-bs-toggle="tab"><i class="bi bi-reply-fill"></i>Tầm nhìn
                                FreshHouse</a></li>
                        <li class="nav-item"><a class="nav-link" href="#profile" role="tab" aria-controls="profile"
                                data-bs-toggle="tab"><i class="bi bi-reply-fill"></i>Sứ mệnh FreshHouse</a></li>
                        <li class="nav-item"><a class="nav-link" href="#messages" role="tab" aria-controls="messages"
                                data-bs-toggle="tab"><i class="bi bi-reply-fill"></i>Giá trị FreshHouse</a></li>

                    </ul>
                    <div class="reasonsWhy mb-3 d-none d-md-block">
                        <div class="reasonsWhy_content text-center">
                            <img src="/templates/client/assets/img/line-xanh.png" width="75" alt="">
                            <div class="pt-3">
                                <span class="reasonsWhy_content-heading-text">
                                    <strong>100%</strong>
                                    <br>Bạn sẽ hài lòng dịch vụ<br>
                                    của chúng tôi</span>
                            </div>
                        </div>
                    </div>
                    <div class="div_why mb-3  d-none d-md-block">
                        <div class="row">
                            <div class="col-lg-12 text-center col-md-12 col-12 pb-3">
                                <img class="w-25" src="/templates/client/assets/img/household-xanh.png" alt="">
                                <div class="heading-text">
                                    <strong>Dịch vụ</strong>
                                    <br>Nhân viên chuyên nghiệp<br>
                                </div>
                            </div>
                            <div class="col-lg-12 text-center col-md-12 col-12 pb-3">
                                <img class="w-25" src="/templates/client/assets/img/growth-xanh.png" alt="">
                                <div class="heading-text">
                                    <strong>An tâm </strong>
                                    <br>Thỏa mãn yêu cầu <br>
                                </div>
                            </div>
                            <div class="col-lg-12 text-center col-md-12 col-12 pb-3">
                                <img class="w-25" src="/templates/client/assets/img/line-xanh.png" alt="">
                                <div class="heading-text">
                                    <strong>An toàn</strong>
                                    <br>Sức khỏe - Môi trường <br>
                                </div>
                            </div>
                            <div class="col-lg-12 text-center col-md-12 col-12 pb-3">
                                <img class="w-25" src="/templates/client/assets/img/money-xanh.png" alt="">
                                <div class="heading-text">
                                    <strong>Tiết kiệm </strong>
                                    <br>Giá ưu đãi đến 30%<br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-md-8">


                    <div class="tab-container-one">

                        <div class="tab-content">
                            <div class="tab-pane active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="heading_title_mission">
                                            Tập đoàn FreshHouse có hơn 15 năm kinh nghiệm vệ sinh văn phòng, làm việc
                                            với các công ty
                                            đa quốc gia cũng như các doanh nghiệp độc lập nhỏ hơn.
                                        </div>
                                        <ul style="list-style-type: circle;">
                                            <li>Sức khỏe của khách hàng là điều mà Vua Sạch luôn muốn hướng đến. Với
                                                nhiệt độ của hơi nước
                                                lên đến 500 độ C cùng dung dịch kháng khuẩn tốt chúng tôi sẽ loại bỏ
                                                hoàn toàn Virus và Vi
                                                khuẩn ẩn sâu bên trong ghế sofa của bạn.</li>
                                            <li>Sức khỏe của khách hàng là điều mà Vua Sạch luôn muốn hướng đến. Với
                                                nhiệt độ của hơi nước
                                                lên đến 500 độ C cùng dung dịch kháng khuẩn tốt chúng tôi sẽ loại bỏ
                                                hoàn toàn Virus và Vi
                                                khuẩn ẩn sâu bên trong ghế sofa của bạn.</li>
                                            <li>Sức khỏe của khách hàng là điều mà Vua Sạch luôn muốn hướng đến. Với
                                                nhiệt độ của hơi nước
                                                lên đến 500 độ C cùng dung dịch kháng khuẩn tốt chúng tôi sẽ loại bỏ
                                                hoàn toàn Virus và Vi
                                                khuẩn ẩn sâu bên trong ghế sofa của bạn.</li>
                                            <li>Sức khỏe của khách hàng là điều mà Vua Sạch luôn muốn hướng đến. Với
                                                nhiệt độ của hơi nước
                                                lên đến 500 độ C cùng dung dịch kháng khuẩn tốt chúng tôi sẽ loại bỏ
                                                hoàn toàn Virus và Vi
                                                khuẩn ẩn sâu bên trong ghế sofa của bạn.</li>
                                            <li>Sức khỏe của khách hàng là điều mà Vua Sạch luôn muốn hướng đến. Với
                                                nhiệt độ của hơi nước
                                                lên đến 500 độ C cùng dung dịch kháng khuẩn tốt chúng tôi sẽ loại bỏ
                                                hoàn toàn Virus và Vi
                                                khuẩn ẩn sâu bên trong ghế sofa của bạn.</li>
                                        </ul>
                                    </div>

                                </div>
                            </div>
                            <div class="tab-pane" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                Profile...</div>
                            <div class="tab-pane" id="messages" role="tabpanel" aria-labelledby="messages-tab">
                                Messages...</div>

                        </div>

                    </div>
                    <div class="forrm_submit  d-none d-md-block">
                        <div class="row align-items-center justify-content-center">
                            <div class="col-lg-11 col-md-12">
                                <div class="h3">
                                    Đăng ký để biết các cải tiến mới nhất:
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4"><input type="text" class="form-control"
                                    placeholder="Họ và tên *">
                            </div>
                            <div class="col-lg-4 col-md-4"><input type="text" class="form-control"
                                    placeholder="Địa chỉ Email*">
                            </div>
                            <div class="col-lg-3 col-md-4 justify-content-center">
                                <a href="" target="_self" class="request" role="button">
                                    <span class="button-text">Đăng ký</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="row pt-4">
                        <div class="col-12">
                            <div class="title_about_intro ">
                                Con người FreshHouse
                            </div>

                            <p class="py-3 mb-0">Tôn trọng sự “khác biệt” để làm nên đột phá, chúng tôi với đội ngũ nhân
                                sự
                                trẻ, giàu
                                kinh nghiệm
                                coi khả năng “sáng tạo không ngừng” là yếu tố cốt lõi tạo nên sự thành công, là tiền đề
                                hướng
                                đến tương lai phát triển cho đối tác, cộng đồng và xã hội.</p>
                        </div>
                        <div class="col-6 col-md-6 col-lg-3">
                            <a href="" class="pxp-agents-1-item">
                                <div class="pxp-agents-1-item-fig-container rounded-lg">
                                    <div class="pxp-agents-1-item-fig pxp-cover"
                                        style="background-image: url(/templates/client/assets/img/Depositphotos_88779358_original.jpg); background-position: top center;    background-size: cover;">
                                    </div>
                                </div>
                                <div class="pxp-agents-1-item-details rounded-lg">
                                    <div class="pxp-agents-1-item-details-name">Chuyên Nghiệp</div>


                                </div>

                            </a>
                        </div>
                        <div class="col-6 col-md-6 col-lg-3">
                            <a href="" class="pxp-agents-1-item">
                                <div class="pxp-agents-1-item-fig-container rounded-lg">
                                    <div class="pxp-agents-1-item-fig pxp-cover"
                                        style="background-image: url(/templates/client/assets/img/Depositphotos_89551394_original.jpg); background-position: top center;    background-size: cover;">
                                    </div>
                                </div>
                                <div class="pxp-agents-1-item-details rounded-lg">
                                    <div class="pxp-agents-1-item-details-name">Nhiệt huyết</div>


                                </div>

                            </a>
                        </div>
                        <div class="col-6 col-md-6 col-lg-3">
                            <a href="" class="pxp-agents-1-item">
                                <div class="pxp-agents-1-item-fig-container rounded-lg">
                                    <div class="pxp-agents-1-item-fig pxp-cover"
                                        style="background-image: url(/templates/client/assets/img/Depositphotos_28062927_original.jpg); background-position: top center;    background-size: cover;">
                                    </div>
                                </div>
                                <div class="pxp-agents-1-item-details rounded-lg">
                                    <div class="pxp-agents-1-item-details-name">Năng động</div>


                                </div>

                            </a>
                        </div>
                        <div class="col-6 col-md-6 col-lg-3">
                            <a href="" class="pxp-agents-1-item">
                                <div class="pxp-agents-1-item-fig-container rounded-lg">
                                    <div class="pxp-agents-1-item-fig pxp-cover"
                                        style="background-image: url(/templates/client/assets/img/Depositphotos_95376934_original.jpg); background-position: top center;    background-size: cover;">
                                    </div>
                                </div>
                                <div class="pxp-agents-1-item-details rounded-lg">
                                    <div class="pxp-agents-1-item-details-name">Đoàn kết</div>


                                </div>

                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('css')
<link rel="stylesheet" href="/templates/client/assets/css/company-mission.css">
@endsection

@section('seo')

@endsection