@extends('templates.client.master')
@section('content')

<section class="bannerHeader padding-top-head">
        <div class="my-container">
            <h3 class="underline">FreshHouse</h3>
            <h2>Công Nghệ Sử Dụng</h2>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-8 col-12 col-md-12">
                        <p class="text-white">
                            Công ty FreshHouse luôn coi khách hàng là người thân, môi trường của khách hàng cũng là môi
                            trường của
                            chúng tôi. Đối với chúng tôi, học hỏi không phải ở một khách hàng trong một thời điểm nào đó
                            mà
                            là học
                            hỏi ở nhiều khách hàng trong suốt một quá trình dài.
                        </p>

                    </div>
                    <div class="col-lg-5 col-md-8">
                        <div class="video-holder">
                            <div class="video_holder_inner clearfix">
                                <div class="icon">
                                    <a class="html5lightbox bg" title="Home"
                                        href="https://www.youtube.com/watch?v=b9B1nOyzNvI"><span
                                            class="flaticon-multimedia flaticon-play-arrow"><i
                                                class="bi bi-play-fill"></i></span></a>
                                </div>
                                <div class="text ">
                                    <h6>Xem video &amp;<br> Biết cách chúng tôi làm việc</h6>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="breadcrumbs">
                <a href="https://freshhouse.vn/">Trang chủ</a> »
                <a > Công Nghệ Sử Dụng</a>
                <img src="/templates/client/assets/img/8 (1).png">
            </div>
        </div>

    </section>
    <section class="py-5">
        <div class="container ">
            <div class="row">
                <div class="col-lg-6">
                    <div class="title_1">
                        Các loại dung dịch khử khuẩn hiệu quả FreshHouse sử dung trong quá trình làm việc
                    </div>
                </div>
                <div class="col-lg-6">
                    <p>
                        Mỗi ngày, sứ mệnh của chúng tôi là xây dựng giá trị cho doanh nghiệp của bạn bằng cách giúp bạn
                        giảm chi phí hoạt động, giảm khiếu nại của khách hàng và cải thiện trải nghiệm tổng thể của
                        khách hàng ..
                    </p>
                    <p>
                        Mỗi ngày, sứ mệnh của chúng tôi là xây dựng giá trị cho doanh nghiệp của bạn bằng cách giúp bạn
                        giảm chi phí hoạt động, giảm khiếu nại của khách hàng và cải thiện trải nghiệm tổng thể của
                        khách hàng ..
                    </p>
                </div>
            </div>
        </div>
    </section>
    <section class="work_gallery">
        <div class="container">
            <div class="top_headings pb-3">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="heading">
                            <div class="h-title">HÌNH ẢNH CÔNG NGHỆ </div>
                            <div class="border_line"></div>
                        </div>
                        <h1 class="sub_heading">Hình ảnh các dự án </h1>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 col-6">

                    <div class="our_services_inner">
                        <div class="image">
                            <img width="370" height="270" src="/templates/client/assets/img/Depositphotos_88779358_original.jpg"
                                class="img-fluid wp-post-image" alt="">
                        </div>
                        <div class="over_content">
                            <div class="title_img">
                                FreshHouse
                            </div>
                            <p>Hình ảnh dự án thực tế</p>
                        </div>

                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 col-6">

                    <div class="our_services_inner">
                        <div class="image">
                            <img width="370" height="270" src="/templates/client/assets/img/Depositphotos_89551394_original.jpg"
                                class="img-fluid wp-post-image" alt="">
                        </div>
                        <div class="over_content">
                            <div class="title_img">
                                FreshHouse
                            </div>
                            <p>Hình ảnh dự án thực tế</p>
                        </div>

                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 col-6">

                    <div class="our_services_inner">
                        <div class="image">
                            <img width="370" height="270" src="/templates/client/assets/img/Depositphotos_28062927_original.jpg"
                                class="img-fluid wp-post-image" alt="">
                        </div>
                        <div class="over_content">
                            <div class="title_img">
                                FreshHouse
                            </div>
                            <p>Hình ảnh dự án thực tế</p>
                        </div>

                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 col-6">

                    <div class="our_services_inner">
                        <div class="image">
                            <img width="370" height="270" src="/templates/client/assets/img/Depositphotos_95376934_original.jpg"
                                class="img-fluid wp-post-image" alt="">
                        </div>
                        <div class="over_content">
                            <div class="title_img">
                                FreshHouse
                            </div>
                            <p>Hình ảnh dự án thực tế</p>
                        </div>

                    </div>
                </div>

            </div>
        </div>
        <div class="container con_1 ">
            <div class="row">
                <div class="col-lg-6 text_oder">
                    <div class="td3"><span style="color: #ff6600; font-size: 24px;">◈ </span>Tiêu Diệt Và Ngăn Ngừa Vi
                        khuẩn Hiệu Quả</div>
                    <ul style="list-style-type: circle;">
                        <li>Sức khỏe của khách hàng là điều mà Vua Sạch luôn muốn hướng đến. Với nhiệt độ của hơi nước
                            lên đến 500 độ C cùng dung dịch kháng khuẩn tốt chúng tôi sẽ loại bỏ hoàn toàn Virus và Vi
                            khuẩn ẩn sâu bên trong ghế sofa của bạn.</li>
                        <li>Sức khỏe của khách hàng là điều mà Vua Sạch luôn muốn hướng đến. Với nhiệt độ của hơi nước
                            lên đến 500 độ C cùng dung dịch kháng khuẩn tốt chúng tôi sẽ loại bỏ hoàn toàn Virus và Vi
                            khuẩn ẩn sâu bên trong ghế sofa của bạn.</li>
                    </ul>
                </div>
                <div class="col-lg-6 img_order">
                    <img class="w-100" src="/templates/client/assets/img/anh-1.jpg" alt="">
                </div>
            </div>
        </div>
        <div class="container con_1 ">
            <div class="row">
                <div class="col-lg-6">
                    <img class="w-100 img_order" src="/templates/client/assets/img/anh-3.jpg" alt="">
                </div>
                <div class="col-lg-6 text_oder">
                    <div class="td3"><span style="color: #ff6600; font-size: 24px;">◈ </span>Tiêu Diệt Và Ngăn Ngừa Vi
                        khuẩn Hiệu Quả</div>
                    <ul style="list-style-type: circle;">
                        <li>Sức khỏe của khách hàng là điều mà Vua Sạch luôn muốn hướng đến. Với nhiệt độ của hơi nước
                            lên đến 500 độ C cùng dung dịch kháng khuẩn tốt chúng tôi sẽ loại bỏ hoàn toàn Virus và Vi
                            khuẩn ẩn sâu bên trong ghế sofa của bạn.</li>
                        <li>Sức khỏe của khách hàng là điều mà Vua Sạch luôn muốn hướng đến. Với nhiệt độ của hơi nước
                            lên đến 500 độ C cùng dung dịch kháng khuẩn tốt chúng tôi sẽ loại bỏ hoàn toàn Virus và Vi
                            khuẩn ẩn sâu bên trong ghế sofa của bạn.</li>
                    </ul>
                </div>

            </div>
        </div>
        <div class="container con_1 ">
            <div class="row">
                <div class="col-lg-6 text_oder">
                    <div class="td3"><span style="color: #ff6600; font-size: 24px;">◈ </span>Tiêu Diệt Và Ngăn Ngừa Vi
                        khuẩn Hiệu Quả</div>
                    <ul style="list-style-type: circle;">
                        <li>Sức khỏe của khách hàng là điều mà Vua Sạch luôn muốn hướng đến. Với nhiệt độ của hơi nước
                            lên đến 500 độ C cùng dung dịch kháng khuẩn tốt chúng tôi sẽ loại bỏ hoàn toàn Virus và Vi
                            khuẩn ẩn sâu bên trong ghế sofa của bạn.</li>
                        <li>Sức khỏe của khách hàng là điều mà Vua Sạch luôn muốn hướng đến. Với nhiệt độ của hơi nước
                            lên đến 500 độ C cùng dung dịch kháng khuẩn tốt chúng tôi sẽ loại bỏ hoàn toàn Virus và Vi
                            khuẩn ẩn sâu bên trong ghế sofa của bạn.</li>
                    </ul>
                </div>
                <div class="col-lg-6">
                    <img class="w-100 img_order" src="/templates/client/assets/img/anh-4.jpeg" alt="">
                </div>
            </div>
        </div>
        <div class="container con_1 ">
            <div class="row">
                <div class="col-lg-6">
                    <img class="w-100 img_order" src="/templates/client/assets/img/anh-8.webp" alt="">
                </div>
                <div class="col-lg-6 text_oder">
                    <div class="td3">Thân Thiện <br>
                        Sức Khỏe - Môi Trường</div>
                    <p>
                        Mỗi ngày, sứ mệnh của chúng tôi là xây dựng giá trị cho doanh nghiệp của bạn bằng cách giúp bạn
                        giảm chi phí hoạt động, giảm khiếu nại của khách hàng và cải thiện trải nghiệm tổng thể của
                        khách hàng ..
                    </p>
                    <p>
                        Mỗi ngày, sứ mệnh của chúng tôi là xây dựng giá trị cho doanh nghiệp của bạn bằng cách giúp bạn
                        giảm chi phí hoạt động.
                    </p>
                    <p>Mỗi ngày, sứ mệnh của chúng tôi là xây dựng giá trị cho doanh nghiệp của bạn bằng cách giúp bạn
                        giảm chi phí hoạt động, giảm khiếu nại của khách hàng và cải thiện trải nghiệm.</p>
                </div>

            </div>
        </div>
    </section>
@endsection

@section('css')
<link rel="stylesheet" href="/templates/client/assets/css/technology-use.css">
@endsection

@section('seo')

@endsection