@extends('templates.client.master')
@section('content')
<section class="hero">

        <div class="hero-fluid ">
            <div class="container">
                <div class="row justify-content-end">
                    <div class="col-lg-7">
                        <div class="row">
                            <div class="col-lg-10">
                                <!-- <div class="home_freshhouse-hand">
                                    <img src="/templates/client/assets/img/Glove.png" alt="">
                                </div> -->
                                <div class="freshhouse-heading home_freshhouse-heading ">

                                    <span class="freshhouse_top"> CHÀO BẠN ĐẾN VỚI FRESHHOUSE</span>
                                    <div class="freshhouse_title ">Hãy Ra Ngoài &amp; Vui Chơi.</div>
                                    <div class="underline_blue_center"></div>
                                    <div class="freshhouse_bottom">Chúng tôi sẽ dọn dẹp cho bạn</div>
                                    <div class="freshhouse_desc">Hãy để FreshHouse xử lý công việc
                                        trong khi bạn tận hưởng thời gian rảnh rỗi</div>
                                    <a class="contact_number" href="/dat-lich">
                                        <div class="contact_site"> <span>Book hẹn</span></div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hero-bottom ">
                <img src="/templates/client/assets/img/Element-wave-bottom-Slider.png" alt="">
            </div>
        </div>

    </section>
<section class="welcome_section">
        <div class="container ">
            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12">
                    <div class="welcome_left">
                        <div class="image_inner">
                            <img src="http://a.ourhtmldemo.com/cooltek/wp-content/uploads/2019/09/welcome_left.png"
                                class="img-fluid" alt="Awesome Image">
                        </div>
                        <div class="wel_content_left">
                            <h2>FreshHouse<br> Dịch vụ vệ sinh uy tính Việt Nam</h2>
                            <div class="video-holder">
                                <div class="video_holder_inner clearfix">
                                    <div class="icon">
                                        <a class="html5lightbox bg" title="Home"
                                            href="https://www.youtube.com/watch?v=b9B1nOyzNvI"><span
                                                class="flaticon-multimedia flaticon-play-arrow"><i
                                                    class="bi bi-play-fill"></i></span></a>
                                    </div>
                                    <div class="text ">
                                        <h6>Xem video &amp;<br> Biết cách chúng tôi làm việc</h6>
                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="col-lg-6 col-md-12 col-sm-12">

                    <div class="welcome_right">

                        <div class="heading">

                            <h6>WELCOME TO FRESHHOUSE</h6>

                            <div class="border_line"></div>

                        </div>

                        <h1 class="sub_heading">Giúp bạn tiết kiệm thời gian
                            cho bản thân & gia đình</h1>

                        <p class="description"> Cuộc sống hiện đại ngày càng bận rộn, áp lực công việc ngày một gia
                            tăng.Thấu hiểu đĩều đó, FreshHouse ra đời với mong muốn chia sẻ gánh nặng
                            cùng quý khách qua những công việc "không tên" nhằm giúp quý khách "có
                            thêm thời gian cho cuộc sống".</p>
                        <div class="div_button pb-3">
                            <a href="{{ route('client.index.about') }}">
                                Về chúng tôi <i class="bi bi-chevron-right"></i>
                            </a>
                        </div>

                        <div class="list_items clearfix">

                            <ul class="first">



                                <li><span class="flaticon-favourites-filled-star-symbol"><i
                                            class="bi bi-star-fill"></i></span>Nhân viên tuyển chọn & đào tạo
                                </li>


                                <li><span class="flaticon-favourites-filled-star-symbol"><i
                                            class="bi bi-star-fill"></i></span>Công nghệ tiên tiến hàng đầu </li>


                            </ul>

                            <ul class="second">



                                <li><span class="flaticon-favourites-filled-star-symbol"><i
                                            class="bi bi-star-fill"></i></span>Dung dịch được kiểm định
                                </li>


                                <li><span class="flaticon-favourites-filled-star-symbol"><i
                                            class="bi bi-star-fill"></i></span>Môi trường & thiết bị hiện đại
                                </li>


                            </ul>

                        </div>

                        <div class="authour">

                            <img src="http://a.ourhtmldemo.com/cooltek/wp-content/uploads/2019/09/welcome_sign.png"
                                class="img-fluid" alt="Awesome Image">

                            <div class="text">

                                <h2>FreshHouse</h2>

                                <p>CEO &amp; Founder of FreshHouse</p>

                            </div>

                        </div>

                    </div>

                </div>

            </div>
        </div>
    </section>
    <section class="section_service">
        <div class="container ">
            <div class="row justify-content-center">
                <div class="col-12 text-center">
                    <div class="heading">
                        <div class="h-title">DỊCH VỤ CỦA CHÚNG TÔI</div>
                        <div class="border_line"></div>
                        <div class="sub_heading">Dịch vụ FreshHouse cung cấp cho bạn</div>
                        <p class="sub_desc">Với đội ngũ nhân viên được tuyển chọn & huấn luyện chuyên nghiệp chúng tôi
                            <br>
                            hi vọng sẽ mang đến sự hài lòng cho quý khách!
                        </p>
                    </div>
                </div>
                @foreach($services as $service)
                <div class="col-lg-4 col-md-6 pb-3">
                    <div class="single-service-box">
                        <div class="service-thumb">
                            <a href="{{ route('client.service.detail', $service->slug) }}"><img src="/upload/{{ $service->picture }}" alt="{{ $service->title }}"></a>
                        </div>
                        <div class="service-content ">
                            <div class="service-title">
                                <a href="{{ route('client.service.detail', $service->slug) }}" tabindex="0">{{ $service->title }}</a>
                            </div>
                            <div class="service-desc">
                                <p class="mb-1">
                                    {{ $service->description }}
                                </p>
                            </div>
                            <div class=" py-2">

                                <div class="div_button div_button_contact d-flex justify-content-start">
                                    <a href="{{ route('client.service.detail', $service->slug) }}">
                                        Chi tiết dịch vụ <i class="bi bi-chevron-right"></i>
                                    </a>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
    <section class="work_gallery">
        <div class="container">
            <div class="top_headings pb-3">
                <div class="row">
                    <div class="col-lg-4 col-md-12 col-sm-12">
                        <div class="heading">
                            <h6>HÌNH ẢNH</h6>
                            <div class="border_line"></div>
                        </div>
                        <h1 class="sub_heading">Hình ảnh các dự án </h1>
                    </div>
                    <div class="col-lg-5 col-md-12 col-sm-12 d-none d-lg-block">
                        <div class="right_content sme">
                            <p>Chúng tôi đã thực hiện hơn 800 dự án trong 3 năm qua, với 100% sự hài lòng.</p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-12 col-sm-12">
                        <div class="right_btn sme text-right">
                            <div class="div_button pb-3">
                                <a href="{{ route('client.index.about') }}">
                                    Về chúng tôi <i class="bi bi-chevron-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                @foreach($pictures as $picture)
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 col-6">

                    <div class="our_services_inner">
                        <div class="image">
                            <img width="370" height="270" src="/upload/{{ $picture->url }}" class="img-fluid wp-post-image"
                                alt="{{ $picture->description }}">
                        </div>
                        <div class="over_content">
                            <div class="title_img">
                                FreshHouse
                            </div>
                            <p>Hình ảnh dự án thực tế</p>
                        </div>

                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
    <section class="news_and_updates">
        <div class="container">
            <div class="row pb-3">
                <div class="col-lg-12">
                    <div class="heading">
                        <h6>TIN TỨC & CẬP NHẬT</h6>
                        <div class="border_line"></div>
                    </div>
                    <h1 class="sub_heading">Các tin tức mới nhất</h1>

                </div>
            </div>
            <div class="row pt-4">
                @foreach($blogs as $blog)
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        <div class="news_updates_inner">
                            <div class="image_inner">
                                <div class="image_before">
                                    <a href="{{ route('client.blog.detail', $blog->slug) }}"><img width="770" height="420" src="/upload/{{ $blog->picture }}"
                                        class="img-fluid wp-post-image" alt=""></a><span
                                        class="flaticon-zoom flaticon-zoom-increasing-symbol"></span>
                                </div>
                            </div>
                           

                            <div class="content">
                                <ul>
                                    <li><span class="flaticon-folder"><i class="bi bi-folder-fill"></i></span>{{ $blog->category->name }}</li>
                                    <li><span class="flaticon-comment"><i class="bi bi-eye"></i></span>{{ $blog->view }} Lượt xem
                                    </li>
                                </ul>
                                <h2><a href="{{ route('client.blog.detail', $blog->slug) }}">{{ $blog->title }}</a></h2>

                            </div>

                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>


    <section class="testimonial_section">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 py-5 px-3 px-lg-5">
                    <div class="heading">
                        <div class="h-title">LIÊN HỆ</div>
                        <div class="border_line"></div>
                        <div class="sub_heading">Chúng tôi rất muốn lắng nghe ý kiến của bạn</div>
                        <p class="sub_desc">Chúng tôi sẽ phản hồi kịp thời các nhu cầu dịch vụ của bạn.<br> Đừng ngần
                            ngại khi gọi hoặc gửi tin nhắn.
                        </p>
                        <div class="accord-content " style="display: block;">
                            <ul class="contact-info-list">
                                <li>
                                    <div class="icon-holder">
                                        <i class="bi bi-geo-alt-fill"></i>
                                    </div>
                                    <div class="text-holder">
                                        <div><span>183 Quách Thị Trang,</span><br> Cảm Lệ, Tp. Đà Nẵng

                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="icon-holder">
                                        <i class="bi bi-telephone-fill"></i>
                                    </div>
                                    <div class="text-holder">
                                        <div><span>Liên hệ</span><br>0983 19 15 87</div>
                                    </div>
                                </li>
                                <li>
                                    <div class="icon-holder">
                                        <i class="bi bi-envelope-fill"></i>
                                    </div>
                                    <div class="text-holder">
                                        <div><span>Gửi mail</span><br>vscnfreshhouse@gmail.com</div>
                                    </div>
                                </li>
                                <li>
                                    <div class="icon-holder">
                                        <i class="bi bi-clock-fill"></i>
                                    </div>
                                    <div class="text-holder">
                                        <div><span>Thời gian</span><br>Thứ 2 - Chủ nhật: 09.00am to 18.00pm</div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 ">
                    <div class="schedule_appointment">
                        <div class="appointment_inner">
                            <div class="topic">
                                <div>Gửi yêu cầu</div>
                            </div>
                           @include('errors.msg')
<form action="{{ route('client.index.contact') }}" method="post">
    @csrf
    <div class="form_inner">
        <div class="row">
            <div class="col-12">
                <input type="text" class="form-control" placeholder="Họ và tên *" name="name" value="{{ old('name') }}">
                <p class="text-danger">@if($errors->has('name')){{ $errors->first('name') }}@endif</p>
            </div>
            <div class="col-6 pt-3">
                <input type="text" class="form-control" placeholder="Số điện thoại *" name="phone"
                    value="{{ old('phone') }}">
                <p class="text-danger">@if($errors->has('phone')){{ $errors->first('phone') }}@endif</p>
            </div>
            <div class="col-6 pt-3">
                <input type="email" class="form-control" placeholder="Email của bạn*" name="email"
                    value="{{ old('email') }}">
                <p class="text-danger">@if($errors->has('email')){{ $errors->first('email') }}@endif</p>
            </div>
            <div class="col-12 pt-3">
                <input type="text" class="form-control" placeholder="Tiêu đề*" name="title" value="{{ old('title') }}">
                <p class="text-danger">@if($errors->has('title')){{ $errors->first('title') }}@endif</p>
            </div>
            <div class="col-12 pt-3">
                <textarea name="description" class="form-control" rows="3"
                    placeholder="Nội dung đề cập*">{{ old('description') }}</textarea>
                <p class="text-danger">@if($errors->has('description')){{ $errors->first('description') }}@endif</p>
            </div>
            <div class="col-12">
                <div class="col-12 pt-3">
                    <div class="row">
                        <div class="col-lg-4 col-md-5 col-sm-12">
                            <div class="form-group">
                                <a type="submit" class="wpcf7-form-control wpcf7-submit theme-btn"><span
                                        class="ajax-loader"> Gửi tin nhắn</span></a>
                            </div>
                        </div>
                        <div class="col-lg-8 col-md-7 col-sm-12 d-flex align-items-center">
                            <p class="mb-0">* Nhóm hỗ trợ khách hàng của chúng tôi sẽ liên hệ
                                với bạn trong vòng 24 giờ.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="testimonial text-center">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="heading">
                        <div class="h-title">ĐÁNH GIÁ CỦA KHÁCH HÀNG</div>
                        <div class="border_line"></div>
                        <div class="sub_heading">Chất lượng dịch vụ?</div>

                    </div>
                </div>
            </div>
        </div>

        <div id="carouselExampleDark" class="carousel carousel-dark slide" data-bs-ride="carousel">
            <div class="carousel-indicators">
                <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="0" class="active"
                    aria-current="true" aria-label="Slide 1"></button>
                <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="1"
                    aria-label="Slide 2"></button>
                <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="2"
                    aria-label="Slide 3"></button>
            </div>
            <div class="carousel-inner" id="testimonial4">
                <div class="carousel-item active" data-bs-interval="10000">
                    <div class="testimonial4_slide">
                        <img src="/templates/client/assets/img/profile-img.jpg" class="img-circle img-responsive" />
                        <p>Đánh giá của khách hàng, chính là các phản hồi từ khách hàng sau một thời gian sử dụng sản
                            phẩm, dịch vụ mà doanh
                            nghiệp của bạn cung cấp. Các đánh giá này sẽ được hiển thị trên các giao diện của doanh
                            nghiệp. Đánh giá của khách hàng,
                            chính là các phản hồi từ khách hàng sau một thời gian sử dụng sản phẩm.</p>
                        <div class="name_customer">Anh Rosalina William</div>
                        <div class="address_customer">Quận 8, TP. HCM</div>
                    </div>
                </div>
                <div class="carousel-item" data-bs-interval="2000">
                    <div class="testimonial4_slide">
                        <img src="/templates/client/assets/img/user-avatar-1.webp" class="img-circle img-responsive" />
                        <p>Đánh giá của khách hàng, chính là các phản hồi từ khách hàng sau một thời gian sử dụng sản
                            phẩm, dịch vụ mà doanh
                            nghiệp của bạn cung cấp. Các đánh giá này sẽ được hiển thị trên các giao diện của doanh
                            nghiệp. Đánh giá của khách hàng,
                            chính là các phản hồi từ khách hàng sau một thời gian sử dụng sản phẩm.</p>
                        <div class="name_customer">Anh Rosalina William</div>
                        <div class="address_customer">Quận 8, TP. HCM</div>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="testimonial4_slide">
                        <img src="/templates/client/assets/img/user-avatar-6.png" class="img-circle img-responsive" />
                        <p>Đánh giá của khách hàng, chính là các phản hồi từ khách hàng sau một thời gian sử dụng sản
                            phẩm, dịch vụ mà doanh
                            nghiệp của bạn cung cấp. Các đánh giá này sẽ được hiển thị trên các giao diện của doanh
                            nghiệp. Đánh giá của khách hàng,
                            chính là các phản hồi từ khách hàng sau một thời gian sử dụng sản phẩm.</p>
                        <div class="name_customer">Anh Rosalina William</div>
                        <div class="address_customer">Quận 8, TP. HCM</div>
                    </div>
                </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleDark"
                data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleDark"
                data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>
    </section>
@endsection

@section('css')
<link rel="stylesheet" href="/templates/client/assets/css/index.css">
@endsection

@section('seo')

@endsection