<footer class="footer footer_custom footer-area ">
        <div class="container py-5">
            <div class="row">
                <div class="col-lg-12 header_bottom header_bottom_fooer">
                    <ul class="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0 menu_main">
                        <li><a href="{{ route('client.index.index') }}" class="nav-link px-2 ">Trang chủ</a></li>
                        <li><a href="{{ route('client.index.about') }}" class="nav-link px-2 ">Giới thiệu</a></li>
                        <li class="active"><a href="{{ route('client.service.category') }}" class="nav-link px-2 ">Dịch vụ</a></li>
                        <li><a href="{{ route('client.index.technology') }}" class="nav-link px-2 ">Công nghệ</a></li>
                        <li><a href="{{ route('client.index.mission') }}" class="nav-link px-2 ">Sứ Mệnh</a></li>
                        <li><a href="{{ route('client.blog.index') }}" class="nav-link px-2 ">Blog</a></li>
                        <li><a href="{{ route('client.index.contact') }}" class="nav-link px-2 ">Liên hệ</a></li>
                    </ul>
                </div>


            </div>
            <div class="row  py-5">
                <div class="col-lg-1 col-md-2 col-2 text-center">
                    <img class=" w-100" src="/templates/client/assets/img/Woman-2.svg" alt="">
                </div>
                <div class="col-lg-3 col-md-10 col-10">
                    <div class="freshhouse-heading ">
                        <span class="freshhouse_top"> WELCOME TO FRESHHOUSE</span>
                        <div class="freshhouse_title ">Get Out & Have Fun.</div>
                        <div class="underline_blue_center"></div>
                        <div class="freshhouse_bottom">Chúng tôi sẽ dọn dẹp cho bạn</div>
                        <div class="freshhouse_desc">Hãy để FreshHouse xử lý công việc
                            trong khi bạn tận hưởng thời gian rảnh rỗi</div>
                        <a class="contact_number" href="#">
                            <div class="contact_site"> <span>Book hẹn</span></div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-5 col-md-7 co-12">
                    <b>Liên hệ</b>

                    <div class="row pt-3">
                        <div class="col-lg-6 col-md-6 col-6">

                            <a href="#" class="map" target="_self">
                                <span class="vamtam-button-text">Google map</span>
                            </a>
                        </div>
                        <div class="col-lg-6 col-md-6 col-6">
                            <div class="fl-rich-text">
                                <p>Thứ 2 - Thứ 6: 7am - 4pm<br>
                                    Cuối tuần: 8am - 4pm</p>
                            </div>
                        </div>
                        <div class="col-lg-6 pt-4 col-md-6 col-6">
                            <div class="fl-rich-text">
                                <p><strong>T:</strong> 0983 19 15 87<br>
                                    <strong>E:</strong> vscnfreshhouse@gmail.com
                                </p>
                            </div>
                        </div>
                        <div class="col-lg-6 pt-4 col-md-6 col-6">
                            <div class="social">
                                <a href="#">
                                    <i class="bi bi-facebook"></i>
                                </a>
                                <a href="#">
                                    <i class="bi bi-instagram"></i>
                                </a>
                                <a href="#">
                                    <i class="bi bi-twitter"></i>
                                </a>
                                <a href="#">
                                    <i class="bi bi-youtube"></i>
                                </a>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3  col-md-5">
                    <b>Gọi điện tư vấn </b>
                    <div class="phone_footer pt-3">
                        <a href="tel: 0983191587" title=" 0983191587" target="_self">
                            <span class="heading-text"> 0983 19 15 87</span>
                        </a>
                    </div>
                    <p>Hệ thống lập lịch và thanh toán trực tuyến của chúng tôi an toàn.</p>
                    <a href="" target="_self" class="request" role="button">
                        <span class="button-text">Liên hệ</span>
                    </a>
                </div>
            </div>
        </div>
        <section class="footer_end">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <p> Copyrights © 2022 All Rights Reserved by FreshHouse</p>
                    </div>
                    <div class="col-lg-6 col-md-6 ">
                        <div class="tusyou">
                            <a href="#">Terms & Conditions</a>
                            <a class="tusyou_center" href="#">Privacy Policy</a>
                            <a href="#">Sitemap</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </footer>
    <div class="wrapper">
        <div class="ring">
            <div class="coccoc-alo-phone coccoc-alo-green coccoc-alo-show">
                <div class="coccoc-alo-ph-circle"></div>
                <div class="coccoc-alo-ph-circle-fill"></div>
                <div class="coccoc-alo-ph-img-circle"></div>
            </div>

        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"></script>
    <script src="/templates/client/assets/js/redmore.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

    <script>
        $(document).ready(function () {
            // menu click event
            $('.menuBtn').click(function () {
                $(this).toggleClass('act');
                if ($(this).hasClass('act')) {
                    $('.mainMenu').addClass('act');
                }
                else {
                    $('.mainMenu').removeClass('act');
                }
            });
        });
    </script>
    <script>
        window.addEventListener('scroll', function () {
            let header = document.querySelector('header');
            let windowPosition = window.scrollY > 0;
            header.classList.toggle('scrolling-active', windowPosition);
        })
    </script>

</body>

</html>