<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Trang chủ</title>
    @yield('seo')
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <!-- Bootstrap Icon -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">


    <!-- css Required  -->
    <link rel="stylesheet" href="/templates/client/assets/css/header.css">
    <link rel="stylesheet" href="/templates/client/assets/css/base.css">
    <!--  -->
    <!-- css page -->
    @yield('css')
      <link rel="icon" href="/templates/client/assets/img/favicon.png">

</head>

<body>


    <header class="{{ Route::currentRouteName() == 'client.index.index'?'homepage':'' }}">
        <div class="container ">
            <div class="d-flex flex-wrap align-items-center justify-content-between header_top">
                <div href="/" class="d-flex align-items-center mb-2 mb-lg-0 header_top_title">
                    <i class="bi bi-clock-history"></i> T2 - CN, Giờ hoạt động 8:00 am - 17:30 pm
                </div>


                <div class="social">
                    <a href="#">
                        <i class="bi bi-facebook"></i>
                    </a>
                    <a href="#">
                        <i class="bi bi-instagram"></i>
                    </a>
                    <a href="#">
                        <i class="bi bi-twitter"></i>
                    </a>
                    <a href="#">
                        <i class="bi bi-youtube"></i>
                    </a>

                </div>
            </div>
            <div class="mobile">
                <div class="mainContainer">
                    <div>
                        <a href="/" class=" logo">
                            <img src="/templates/client/assets/img/example-logo-png-7.png" alt="">
                        </a>
                        <a href="#" class="menuBtn">
                            <span class="lines"></span>
                        </a>
                        <div class="mainMenu">
                            <ul class="list-unstyled">
                                <li>
                                    <a href="/" class="{{ Request::is('/')?'active':'' }}">Trang chủ</a>
                                </li>
                                <li>
                                    <a class="{{ Request::is('gioi-thieu')?'active':'' }}" href="{{ route('client.index.about') }}">Giới thiệu</a>
                                </li>
                                <li>
                                    <a class="{{ Request::is('dich-vu*')?'active':'' }}" href="{{ route('client.service.category') }}">Dịch vụ</a>
                                </li>
                                <li>
                                    <a class="{{ Request::is('cong-nghe')?'active':'' }}" href="{{ route('client.index.technology') }}">Công nghệ</a>
                                </li>
                                <li>
                                    <a class="{{ Request::is('su-menh')?'active':'' }}" href="{{ route('client.index.mission') }}">Sứ mệnh</a>
                                </li>
                                <li>
                                    <a class="{{ Request::is('bai-viet*') || Request::is('danh-muc*')?'active':'' }}" href="{{ route('client.blog.index') }}">Blog</a>
                                </li>
                                <li>
                                    <a class="{{ Request::is('lien-he')?'active':'' }}" href="{{ route('client.index.contact') }}">Liên hệ</a>
                                </li>
                                <div class="dropdown account">
                                    <a href="{{ route('user.index.index') }}">
                                        <i class="bi bi-person-fill"></i> Tài khoản
                                    </a>

                                </div>
                                <div class="contact_mobile">
                                    <div class="fl-rich-text-mobile">
                                        <p><span>Hãy để FreshHouse xử lý công việc trong khi bạn tận hưởng thời gian
                                                rảnh rỗi <strong><br><a href="tel: 0983191587" target="_blank"
                                                        rel="noopener"> 0983 19 15 87</a></strong></span></p>
                                    </div>
                                    <div class="social social-mobile">
                                        <a href="#">
                                            <i class="bi bi-facebook"></i>
                                        </a>
                                        <a href="#">
                                            <i class="bi bi-instagram"></i>
                                        </a>
                                        <a href="#">
                                            <i class="bi bi-twitter"></i>
                                        </a>
                                        <a href="#">
                                            <i class="bi bi-youtube"></i>
                                        </a>

                                    </div>
                                </div>
                                <a class="contact_number" href="#">
                                    <div class="contact_site"> <span>Book hẹn</span></div>
                                </a>
                            </ul>

                        </div>
                    </div>


                </div>
            </div>
            <div class="d-flex flex-wrap align-items-center justify-content-center header_bottom">
                <a href="/" class="d-flex align-items-center mb-2 mb-lg-0 logo">
                    <img src="/templates/client/assets/img/example-logo-png-7.png" alt="">
                </a>

                <ul class="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0 menu_main">
                    <li class="{{ Request::is('/')?'active':'' }}"><a href="{{ route('client.index.index') }}" class="nav-link px-2 ">Trang chủ</a></li>
                    <li class="{{ Request::is('gioi-thieu')?'active':'' }}"><a href="{{ route('client.index.about') }}" class="nav-link px-2 ">Giới thiệu</a></li>
                    <li class="{{ Request::is('dich-vu*')?'active':'' }}"><a href="{{ route('client.service.category') }}" class="nav-link px-2 ">Dịch vụ</a></li>
                    <li class="{{ Request::is('cong-nghe')?'active':'' }}"><a href="{{ route('client.index.technology') }}" class="nav-link px-2 ">Công nghệ</a></li>
                    <li class="{{ Request::is('su-menh')?'active':'' }}"><a href="{{ route('client.index.mission') }}" class="nav-link px-2 ">Sứ Mệnh</a></li>
                    <li class="{{ Request::is('bai-viet*') || Request::is('danh-muc*')?'active':'' }}"><a href="{{ route('client.blog.index') }}" class="nav-link px-2 ">Blog</a></li>
                    <li class="{{ Request::is('lien-he')?'active':'' }}"><a href="{{ route('client.index.contact') }}" class="nav-link px-2 ">Liên hệ</a></li>
                </ul>


                <div class="dropdown account">
                    <a href="{{ route('user.index.index') }}">
                        <i class="bi bi-person-fill"></i> Tài khoản
                    </a>

                </div>
            </div>
        </div>
    </header>
  <div class="footermenu-fix sm-view">
    <ul class="footermenu-wrap">
        <li id="item-1" class="footermenu-wrap-item ">
            <div class="hover footermenu-item color_primary"> <a href="#" data-open="#main-menu" data-pos="left"
                    data-bg="main-menu-overlay" data-color="" class="icon footermenu-itemlink" aria-label="Menu"
                    aria-controls="main-menu" aria-expanded="false"> <img
                        src="/templates/client/assets/img/menu-primary.png" width="23" height="23">
                    <span>Menu</span> </a></div>
        </li>
        <li class="footermenu-wrap-item">
            <div class="footermenu-item"> <a href="tel: 0983191587" class="footermenu-itemlink register_click_dangky">
                    <img src="/templates/client/assets/img/goi-dien-thoai-primary.png" width="23"
                        height="23"> <span>Phone</span> </a></div>
        </li>
        <li class="footermenu-wrap-item center">
            <div class="footermenu-svg"> <img class=""
                    src="/templates/client/assets/img/circle-shape.png" alt="Liên hệ ngay"></div>
            <div class="footermenu-shadow"></div>
            <div class="footermenu-item color_white" style="padding-top:0px"> <a
                    class="footermenu-itemlink register_click_tuvan" href="#">
                    <img src="/templates/client/assets/img/broom.png" width="30" height="30">
                    <span style="color:#fff">Book hẹn</span> </a></div>
        </li>
        <li class="footermenu-wrap-item">
            <div class="footermenu-item"> <a href="#" target="_blank"
                    class="footermenu-itemlink"> <img
                        src="/templates/client/assets/img/chat-primary.png" width="23" height="23">
                    <span>Chat </span> </a></div>
        </li>
        <li class="footermenu-wrap-item">
            <div class="footermenu-item"> <a class=" register_click_app  icon footermenu-itemlink" href="/lien-he">
                    <img src="/templates/client/assets/img/email.png" width="23" height="23">
                    <span>Liên hệ</span> </a></div>
        </li>
    </ul>
</div>