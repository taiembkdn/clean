
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>FreshHouse</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link rel="icon" href="/templates/client/assets/img/favicon.png">

  <!-- Google Fonts -->
  <link href="https://fonts.gstatic.com" rel="preconnect">
  <link
    href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
    rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="/templates/admin/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="/templates/admin/assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="/templates/admin/assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="/templates/admin/assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="/templates/admin/assets/vendor/quill/quill.snow.css" rel="stylesheet">
  <link href="/templates/admin/assets/vendor/quill/quill.bubble.css" rel="stylesheet">
  <link href="/templates/admin/assets/vendor/simple-datatables/style.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>


  <!-- Template Main CSS File -->
  <link href="/templates/admin/assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: NiceAdmin - v2.1.0
  * Template URL: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="header fixed-top d-flex align-items-center">

    <div class="d-flex align-items-center justify-content-between">
      <a href="index.html" class="logo d-flex align-items-center">
        <img src="/templates/client/assets/img/example-logo-png-7.png" alt="">
       
      </a>
      <i class="bi bi-list toggle-sidebar-btn"></i>
    </div><!-- End Logo -->

    <nav class="header-nav ms-auto">
      <ul class="d-flex align-items-center">



        <li class="nav-item dropdown pe-3">

          <a class="nav-link nav-profile d-flex align-items-center pe-0" href="#" data-bs-toggle="dropdown">
            <img src="/templates/admin/assets/img/profile-img.jpg" alt="Profile" class="rounded-circle">
            <span class="d-none d-md-block dropdown-toggle ps-2">{{ auth()->user()->name }}</span>
          </a><!-- End Profile Iamge Icon -->

          <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow profile">

            <li>
              <a class="dropdown-item d-flex align-items-center" href="{{ route('auth.admin.logout') }}">
                <i class="bi bi-box-arrow-right"></i>
                <span>Đăng Xuất</span>
              </a>
            </li>

          </ul><!-- End Profile Dropdown Items -->
        </li><!-- End Profile Nav -->

      </ul>
    </nav><!-- End Icons Navigation -->

  </header><!-- End Header -->

  <!-- ======= Sidebar ======= -->
  <aside id="sidebar" class="sidebar">

    <ul class="sidebar-nav" id="sidebar-nav">


      <li class="nav-item">
        <a class="nav-link {{ request()->is('admin/booking*') ? '' : 'collapsed' }}" href="{{ route('admin.booking.index') }}">
          <i class="bi bi-calendar4-event"></i>
          <span>Quản lý đặt lịch</span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link {{ request()->is('admin/service*') ? '' : 'collapsed' }}" href="{{ route('admin.service.index') }}">
          <i class="bi bi-list-check"></i>
          <span>Quản lý dịch vụ</span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link {{ request()->is('admin/room*') ? '' : 'collapsed' }}" href="{{ route('admin.room.index') }}">
          <i class="bi bi-grid"></i>
          <span>Quản lý tên phòng</span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link {{ request()->is('admin/special*') ? '' : 'collapsed' }}" href="{{ route('admin.special.index') }}">
          <i class="bi bi-star"></i>
          <span>Quản lý các yêu cầu</span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link {{ request()->is('admin/gift*') ? '' : 'collapsed' }}" href="{{ route('admin.gift.index') }}">
          <i class="bi bi-gift"></i>
          <span>Quản lý mã giảm giá</span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link {{ request()->is('admin/category*') ? '' : 'collapsed' }}" href="{{ route('admin.category.index') }}">
          <i class="bi bi-clipboard-check"></i>
          <span>Quản lý danh mục blog</span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link {{ request()->is('admin/blog*') ? '' : 'collapsed' }}" href="{{ route('admin.blog.index') }}">
          <i class="bi bi-pen"></i>
          <span>Quản lý blog</span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link {{ request()->is('admin/user*') ? '' : 'collapsed' }}" href="{{ route('admin.user.index') }}">
          <i class="bi bi-people"></i>
          <span>Quản lý người dùng</span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link {{ request()->is('admin/header*') ? '' : 'collapsed' }}" href="{{ route('admin.header.index') }}">
          <i class="bi bi-clock"></i>
          <span>Quản lý SEO</span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link {{ request()->is('admin/picture*') ? '' : 'collapsed' }}" href="{{ route('admin.picture.index') }}">
          <i class="bi bi-clock"></i>
          <span>Quản lý hình ảnh</span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link {{ request()->is('admin/recruit*') ? '' : 'collapsed' }}" href="{{ route('admin.recruit.index') }}">
          <i class="bi bi-clock"></i>
          <span>Quản lý tuyển dụng</span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link {{ request()->is('admin/contact*') ? '' : 'collapsed' }}" href="{{ route('admin.contact.index') }}">
          <i class="bi bi-clock"></i>
          <span>Quản lý liên hệ</span>
        </a>
      </li>

      <li class="nav-item active">
        <a class="nav-link {{ request()->is('admin/admin*') ? '' : 'collapsed' }} " href="{{ route('admin.admin.index') }}">
          <i class="bi bi-shield"></i>
          <span>Quản lý admin</span>
        </a>
      </li>



    </ul>

  </aside><!-- End Sidebar-->