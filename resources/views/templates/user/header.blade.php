<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Dashboard</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <!-- Bootstrap Icon -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">
    <!-- css Required  -->
    <link rel="stylesheet" href="/templates/client/assets/css/header.css">
    <link rel="stylesheet" href="/templates/client/assets/css/base.css">
    <!-- CSS Files -->
    @yield('css')
</head>





<body class="">
    <div class="container ">
        <div class="row">
            <div class="col-12">
                <!-- Navbar -->
                <nav class="navbar header-nav  navbar-expand-lg blur blur-rounded top-0 z-index-3 shadow my-3 py-2 ">
                    <div class="container">
                        <a class="navbar-brand font-weight-bolder ms-lg-0 ms-3 dashBoard d-none d-md-block"
                            href="{{ route('user.index.index') }}">
                            FreshHouse Dashboard
                        </a>
                        <div class="d-flex align-items-center justify-content-between mobile-menu">
                            <button class="navbar-toggler shadow-none ms-2" type="button" data-bs-toggle="collapse"
                                data-bs-target="#navigation" aria-controls="navigation" aria-expanded="false"
                                aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon mt-2">
                                    <i class="bi bi-list"></i>
                                </span>
                            </button>
                            <div class="btn-group d-block d-md-block d-lg-none">
                                <button class="dropdown-toggle nav-profile" type="button"
                                    id="dropdownMenuClickableInside" data-bs-toggle="dropdown"
                                    data-bs-auto-close="outside" aria-expanded="false">
                                     <img src="{{ Auth::guard('web')->user()->avatar?'/upload/'.Auth::guard('web')->user()->avatar:'/templates/client/assets/img/profile-img.jpg' }}" alt="Profile"
                                                class="rounded-circle">
                                    <span class="d-none d-md-none d-lg-block  ps-2">{{ Auth::guard('web')->user()->name }}</span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-arrow profile"
                                    aria-labelledby="dropdownMenuClickableInside">



                                    <li>
                                        <a class="dropdown-item d-flex align-items-center" href="{{ route('user.index.profile') }}">
                                            <i class="bi bi-person"></i>
                                            <span>Chỉnh sửa thông tin</span>
                                        </a>
                                    </li>
                                    <li>
                                        <hr class="dropdown-divider">
                                    </li>

                                    <li>
                                        <a class="dropdown-item d-flex align-items-center" href="{{ route('user.index.profile') }}">
                                            <i class="bi bi-gear"></i>
                                            <span>Đổi mật khẩu</span>
                                        </a>
                                    </li>
                                    <li>
                                        <hr class="dropdown-divider">
                                    </li>
                                    <li>
                                        <hr class="dropdown-divider">
                                    </li>

                                    <li>
                                        <a class="dropdown-item d-flex align-items-center" href="{{ route('auth.client.logout') }}">
                                            <i class="bi bi-box-arrow-right"></i>
                                            <span>Đăng xuất</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="collapse navbar-collapse ps" id="navigation">
                            <ul class="navbar-nav mx-auto">
                                <li class="nav-item">
                                    <a class="nav-link d-flex align-items-center me-2 active" aria-current="page"
                                        href="{{ route('user.booking.booking') }}">
                                        <i class="bi bi-bookmark-star"></i>
                                        Book Hẹn
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link me-2" href="{{ route('user.booking.index') }}">
                                        <i class="bi bi-clock-history"></i>
                                        Lịch Sử Book
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link me-2" type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                                        <i class="bi bi-headset"></i>
                                        Hỗ Trợ
                                    </a>
                                    
                                </li>
                            </ul>
                            <ul class="navbar-nav d-none d-lg-block">
                                <li class="nav-item">

                                    <div class="btn-group">
                                        <button class="dropdown-toggle nav-profile" type="button"
                                            id="dropdownMenuClickableInside" data-bs-toggle="dropdown"
                                            data-bs-auto-close="outside" aria-expanded="false">
@php
    if(Auth::guard('web')->user()->provider == null){
        if(Auth::guard('web')->user()->avatar){
            $avatar = '/upload/'.Auth::guard('web')->user()->avatar;
        }else{
            $avatar = '/templates/client/assets/img/profile-img.jpg';
        }
    }else{
        $avatar = Auth::guard('web')->user()->avatar;
    }
@endphp
                                            <img src="{{ $avatar }}" alt="Profile"
                                                class="rounded-circle">
                                            <span class="d-none d-md-block  ps-2">{{ Auth::guard('web')->user()->name }}</span>
                                        </button>
                                        <ul class="dropdown-menu dropdown-menu-arrow profile"
                                            aria-labelledby="dropdownMenuClickableInside">



                                            <li>
                                                <a class="dropdown-item d-flex align-items-center"
                                                    href="{{ route('user.index.profile') }}">
                                                    <i class="bi bi-person"></i>
                                                    <span>Chỉnh sửa thông tin</span>
                                                </a>
                                            </li>
                                            <li>
                                                <hr class="dropdown-divider">
                                            </li>

                                            <li>
                                                <a class="dropdown-item d-flex align-items-center"
                                                    href="{{ route('user.index.profile') }}">
                                                    <i class="bi bi-gear"></i>
                                                    <span>Đổi mật khẩu</span>
                                                </a>
                                            </li>
                                            <li>
                                                <hr class="dropdown-divider">
                                            </li>

                                            <li>
                                                <a class="dropdown-item d-flex align-items-center"
                                                    href="">
                                                    <i class="bi bi-question-circle"></i>
                                                    <span>Hỗ trợ</span>
                                                </a>
                                            </li>
                                            <li>
                                                <hr class="dropdown-divider">
                                            </li>

                                            <li>
                                                <a class="dropdown-item d-flex align-items-center" href="{{ route('auth.client.logout') }}">
                                                    <i class="bi bi-box-arrow-right"></i>
                                                    <span>Đăng xuất</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>

                        </div>
                    </div>
                </nav>
                <!-- End Navbar -->
            </div>
        </div>
    </div>
    
    <!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
       <form>
    <div class="mb-3">
        <label for="exampleInputEmail1" class="form-label">Địa chỉ email của bạn </label>
        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
        <div id="emailHelp" class="form-text">Chúng tôi sẽ không bao giờ chia sẻ email của bạn với bất kỳ ai khác.</div>
    </div>
    <div class="mb-3">
        <label for="exampleInputPassword1" class="form-label">Nội dung</label>
        <div class="form-floating">
            <textarea class="form-control" placeholder="Leave a comment here" id="floatingTextarea2"
                style="height: 100px"></textarea>
          
        </div>
    </div>

    <button type="submit" class="btn btn-primary">Gửi đi</button>
</form>
      </div>
     
    </div>
  </div>
</div>