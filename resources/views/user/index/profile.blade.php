@extends('templates.user.master')
@section('content')
    <section class="section profile">
        <div class="container">
            <div class="row">

                <div class="col-xl-4">

                    <div class="card_info">
                        <div class="card-body profile-card pt-4 d-flex flex-column align-items-center">
@php
    if(Auth::guard('web')->user()->provider == null){
        if(Auth::guard('web')->user()->avatar){
            $avatar = '/upload/'.Auth::guard('web')->user()->avatar;
        }else{
            $avatar = '/templates/client/assets/img/profile-img.jpg';
        }
    }else{
        $avatar = Auth::guard('web')->user()->avatar;
    }
@endphp
                            <img width="200px" src="{{ $avatar }}" alt="Profile" class="rounded-circle">
                            <h2>{{ Auth::guard('web')->user()->name }}</h2>

                        </div>
                    </div>

                </div>

                <div class="col-xl-8">

                    <div class="card_info">
                        <div class="card-body pt-3">
                            <!-- Bordered Tabs -->
                            @include('errors.msg')
                            <ul class="nav nav-tabs nav-tabs-bordered">

                                <li class="nav-item">
                                    <button class="nav-link active" data-bs-toggle="tab"
                                        data-bs-target="#profile-overview">Tổng quan</button>
                                </li>

                                <li class="nav-item">
                                    <button class="nav-link" data-bs-toggle="tab" data-bs-target="#profile-edit">Chỉnh
                                        sửa thông tin</button>
                                </li>



                                <li class="nav-item">
                                    <button class="nav-link" data-bs-toggle="tab"
                                        data-bs-target="#profile-change-password">Thay đổi mật khẩu</button>
                                </li>

                            </ul>
                            <div class="tab-content pt-2">

                                <div class="tab-pane fade profile-overview active show" id="profile-overview">
                                    <h5 class="card-title">Giới thiệu</h5>
                                    
                                    <h5 class="card-title">Thông tin bản thân</h5>

                                    <div class="row">
                                        <div class="col-lg-3 col-md-4 label ">Họ và tên</div>
                                        <div class="col-lg-9 col-md-8">{{ Auth::guard('web')->user()->name }}</div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-3 col-md-4 label">Địa chỉ</div>
                                        <div class="col-lg-9 col-md-8">{{ Auth::guard('web')->user()->address }}</div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-3 col-md-4 label">Thành phố</div>
                                        <div class="col-lg-9 col-md-8">{{ Auth::guard('web')->user()->city }}</div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-3 col-md-4 label">Số điện thoại</div>
                                        <div class="col-lg-9 col-md-8">{{ Auth::guard('web')->user()->phone_number }}</div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-3 col-md-4 label">Email</div>
                                        <div class="col-lg-9 col-md-8">{{ Auth::guard('web')->user()->email }}</div>
                                    </div>

                                </div>

                                <div class="tab-pane fade profile-edit pt-3" id="profile-edit">

                                    <!-- Profile Edit Form -->
                                    <form action="{{ route('user.index.updateProfile') }}" method="post" enctype="multipart/form-data">
                                        @csrf
                                        <div class="row mb-3">
                                            <div class="col-md-4 col-lg-3">
                                                <label for="profileImage" class="col-form-label">Ảnh đại
                                                    diện</label>
                                            </div>

                                            <div class="col-md-8 col-lg-9">
                                                <img width="200px" src="{{ $avatar }}" alt="Profile">
                                                <div class="pt-2">
                                                    <label for="avatar"><a class="btn btn-primary btn-sm"
                                                        title="Upload new profile image"><i
                                                            class="bi bi-upload"></i></a></label>
                                                    <input type="file" class="d-none" name="avatar" id="avatar" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row mb-3">
                                            <div class="col-md-4 col-lg-3">
                                                <label for="fullName" class="col-form-label">Họ và
                                                    tên</label>
                                            </div>

                                            <div class="col-md-8 col-lg-9">
                                                <input name="name" type="text" class="form-control" id="fullName"
                                                    value="{{ Auth::guard('web')->user()->name }}">
                                                <p class="text-danger">@if($errors->has('name')){{ $errors->first('name') }}@endif</p>
                                            </div>
                                        </div>

                                        <div class="row mb-3">
                                            <div class="col-md-4 col-lg-3">
                                                <label for="address" class="col-form-label">Địa
                                                    chỉ</label>
                                            </div>
                                            <div class="col-md-8 col-lg-9">
                                                <input name="address" type="text" class="form-control" id="address"
                                                    value="{{ Auth::guard('web')->user()->address }}">
                                            </div>
                                        </div>

                                        <div class="row mb-3">
                                            <div class="col-md-4 col-lg-3">
                                                <label for="district" class="col-form-label">Quận/Huyện</label>
                                            </div>
                                            <div class="col-md-8 col-lg-9">
                                                <input name="district" type="text" class="form-control" id="district"
                                                    value="{{ Auth::guard('web')->user()->district }}">
                                            </div>
                                        </div>

                                        <div class="row mb-3">
                                            <div class="col-md-4 col-lg-3">
                                                <label for="city" class="col-form-label">Thành phố</label>
                                            </div>
                                            <div class="col-md-8 col-lg-9">
                                                <input name="city" type="text" class="form-control" id="city"
                                                    value="{{ Auth::guard('web')->user()->city }}">
                                            </div>
                                        </div>

                                        <div class="row mb-3">
                                            <div class="col-md-4 col-lg-3">
                                                <label for="phone_number" class="col-form-label">Số điện
                                                    thoại</label>
                                            </div>
                                            <div class="col-md-8 col-lg-9">
                                                <input name="phone_number" type="text" class="form-control" id="phone_number"
                                                    value="{{ Auth::guard('web')->user()->phone_number }}">
                                            </div>
                                        </div>

                                        <div class="row mb-3">
                                            <div class="col-md-4 col-lg-3">
                                                <label for="email" class=" col-form-label">Email</label>
                                            </div>
                                            <div class="col-md-8 col-lg-9">
                                                <input name="email" type="email" class="form-control" id="email"
                                                    value="{{ Auth::guard('web')->user()->email }}" readonly>
                                            </div>
                                        </div>



                                        <div class="text-center">
                                            <button type="submit" class="btn btn-primary">Lưu lại</button>
                                        </div>
                                    </form><!-- End Profile Edit Form -->

                                </div>


                                <div class="tab-pane fade pt-3" id="profile-change-password">
                                    <!-- Change Password Form -->
                                    <form action="{{ route('user.index.updatePassword') }}" method="post">
                                        @csrf

                                        <div class="row mb-3">
                                            <div class="col-md-4 col-lg-3">
                                                <label for="currentPassword" class="col-form-label">Mật
                                                    khẩu hiện tại</label>
                                            </div>
                                            <div class="col-md-8 col-lg-9">
                                                <input name="currentPassword" type="password" class="form-control"
                                                    id="currentPassword">
                                                <p class="text-danger">@if($errors->has('currentPassword')){{ $errors->first('currentPassword') }}@endif</p>
                                            </div>
                                        </div>

                                        <div class="row mb-3">
                                            <div class="col-md-4 col-lg-3">
                                                <label for="password" class=" col-form-label">Mật khẩu
                                                    mới</label>
                                            </div>
                                            <div class="col-md-8 col-lg-9">
                                                <input name="password" type="password" class="form-control"
                                                    id="password">
                                                <p class="text-danger">@if($errors->has('password')){{ $errors->first('password') }}@endif</p>
                                            </div>
                                        </div>

                                        <div class="row mb-3">
                                            <div class="col-md-4 col-lg-3">
                                                <label for="password_confirmation" class="col-form-label">Nhập lại
                                                    mật khẩu mới</label>
                                            </div>
                                            <div class="col-md-8 col-lg-9">
                                                <input name="password_confirmation" type="password" class="form-control"
                                                    id="password_confirmation">
                                                <p class="text-danger">@if($errors->has('password_confirmation')){{ $errors->first('password_confirmation') }}@endif</p>
                                            </div>
                                        </div>

                                        <div class="text-center">
                                            <button type="submit" class="btn btn-primary">Lưu lại</button>
                                        </div>
                                    </form><!-- End Change Password Form -->

                                </div>

                            </div><!-- End Bordered Tabs -->

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection

@section('css')
<link rel="stylesheet" href="/templates/client/assets/css/customer_page.css">

@endsection