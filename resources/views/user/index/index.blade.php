@extends('templates.user.master')
@section('content')
    <div class="container pt-5">
        <div class="row  justify-content-center ">
            <div class="col-lg-10 ">
                <div class="dashboard_form">
                    <h1 class="text-center dashboard_form_title">Xin chào {{ Auth()->guard('web')->user()->name }}!</h1>
                    <h3 class="text-center">Welcome to FreshHouse</h3>
                    <div class="row py-5 justify-content-center">
                        <div class="col-lg-5 col-md-6">
                            <div class="card">
                                <div class="img-avatar">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                        class="bi bi-calendar" viewBox="0 0 16 16">
                                        <path
                                            d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z" />
                                    </svg>
                                </div>
                                <div class="card-text">
                                    <div class="portada portada_1">

                                    </div>
                                    <div class="title-total">
                                        <div class="title">FreshHouse</div>
                                        <h2>Book Hẹn</h2>

                                        <div class="desc">Lên lịch xử lý công việc vệ sinh
                                            trong khi bạn tận hưởng thời gian rảnh rỗi.</div>
                                        <div class="actions">
                                            <a href="{{ route('user.booking.booking') }}">
                                                <div class="contact_site"> <span>Book ngay</span></div>
                                            </a>
                                        </div>
                                    </div>

                                </div>



                            </div>
                        </div>
                        <div class="col-lg-5 col-md-6 pt-3 pt-md-0 pt-lg-0">
                            <div class="card">
                                <div class="img-avatar">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                        class="bi bi-gift-fill" viewBox="0 0 16 16">
                                        <path
                                            d="M3 2.5a2.5 2.5 0 0 1 5 0 2.5 2.5 0 0 1 5 0v.006c0 .07 0 .27-.038.494H15a1 1 0 0 1 1 1v1a1 1 0 0 1-1 1H1a1 1 0 0 1-1-1V4a1 1 0 0 1 1-1h2.038A2.968 2.968 0 0 1 3 2.506V2.5zm1.068.5H7v-.5a1.5 1.5 0 1 0-3 0c0 .085.002.274.045.43a.522.522 0 0 0 .023.07zM9 3h2.932a.56.56 0 0 0 .023-.07c.043-.156.045-.345.045-.43a1.5 1.5 0 0 0-3 0V3zm6 4v7.5a1.5 1.5 0 0 1-1.5 1.5H9V7h6zM2.5 16A1.5 1.5 0 0 1 1 14.5V7h6v9H2.5z" />
                                    </svg>
                                </div>
                                <div class="card-text">
                                    <div class="portada portada_2">

                                    </div>
                                    <div class="title-total">
                                        <div class="title">FreshHouse</div>
                                        <h2>Lịch sử booking</h2>

                                        <div class="desc">Xem lịch sử booking của bạn. Để biết thêm các thông tin chi tiết</div>
                                        <div class="actions">
                                            <a href="{{ route('user.booking.index') }}">
                                                <div class="contact_site"> <span>Lịch sử booking</span></div>
                                            </a>
                                        </div>
                                    </div>

                                </div>



                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
@endsection

@section('css')
<link rel="stylesheet" href="/templates/client/assets/css/customer_page.css">

@endsection