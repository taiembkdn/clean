@extends('templates.user.master')
@section('content')
<div class="container pt-5">

        <div class="row">
            <div class="col-12 text-center">
                <div class="page_history_title">Chi tiết book 25/06/2016 </div>
            </div>
            <div class="col-lg-6 ">
                <div class="user_booking">
                    <div class="info_customer py-3 under_boder_bottom">
                        <div class="title_form_booking">THÔNG TIN CỦA BẠN</div>
                        <p>Thông tin này sẽ được sử dụng để liên hệ với bạn về dịch vụ của bạn.</p>
                        <div class="row">
                            <div class="col-12 col-md-6 ">
                                <label class="form-control-label ">Họ và tên *</label>
                                <input type="text" class="form-control" placeholder="Họ và tên *" value="{{ Auth::guard('web')->user()->name }}"
                                    disabled="">
                            </div>
                            <div class="col-12 col-md-6 ">
                                <label class="form-control-label ">Email*</label>
                                <input type="text" class="form-control" placeholder="Email*"
                                    value="{{ Auth::guard('web')->user()->email }}" disabled="">
                            </div>

                        </div>
                    </div>
                    <div class="address_customer py-3 under_boder_bottom">
                        <div class="title_form_booking">ĐỊA CHỈ</div>
                        <p>Bạn muốn chúng tôi làm sạch ở đâu?</p>
                        <div class="row">

                            <div class="col-12">
                                <input type="text" class="form-control" placeholder="Số nhà địa chỉ đường*" disabled="" value="{{ Auth::guard('web')->user()->address }}">
                            </div>
                            <div class="col-12 col-md-6 pt-3">
                                <input type="text" class="form-control" placeholder="Quận *" disabled="" value="{{ Auth::guard('web')->user()->district }}">
                            </div>
                            <div class="col-12 col-md-6 pt-3">
                                <input type="text" class="form-control" placeholder="Thành phố *" disabled="" value="{{ Auth::guard('web')->user()->city }}">
                            </div>
                            <div class="col-12 col-md-6 pt-3">
                                <input type="text" class="form-control" placeholder="Số điện thoại*" disabled="" value="{{ Auth::guard('web')->user()->phone_number }}">
                            </div>
                        </div>
                    </div>
                    <div class="add_extras py-3 under_boder_bottom">
                        <div class="row">
                            <div class="col-12 col-md-4">
                                <div class="title_form_booking">YÊU CẦU ĐẶC BIỆT</div>
                            </div>
                            <div class="col-md-8">
                                <div class="row ">
                                    @foreach($specials as $special)
                                    @foreach($booking->special as $spe)
                                    @if($special->id == $spe->id)
                                        @php $checked = 'checked' @endphp
                                    @else
                                        @php $checked = '' @endphp
                                    @endif
                                    @endforeach
                                    <div class="col-12">
                                        <div class="form-check">
                                            <input class="form-check-input" {{ $checked }} type="checkbox" value=""
                                                id="defaultCheck1" disabled >
                                            <label class="form-check-label-new" for="defaultCheck1">
                                                {{ $special->name }}
                                            </label>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="row ">
                                <div class="col-12">
                                    <div class="form-check">
                                        <label class="form-check-label-new" for="defaultCheck1">
                                            Yêu cầu đặc biệt khác
                                        </label>
                                        <input type="text" class="form-control" placeholder="yêu cầu khác*" disabled="" value="{{ $booking->note }}">
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>

                </div>
            </div>
            <div class="col-lg-6">
                <div class="user_booking">
                    <div class="detail_room  py-3 under_boder_bottom">
                        <div class="title_form_booking"> CHI TIẾT CĂN NHÀ CỦA BẠN</div>
                        <div class="row pt-3 ">
                            <div class="col-12 col-md-3 d-flex align-items-center">
                                <div class="form-control-title">Chọn dịch vụ</div>
                            </div>
                            <div class="col-12 col-md-9 ">
                                <select class="form-select" aria-label="Default select example" disabled="">
                                    @foreach($services as $service)
                                    @if($service->id == $booking->service_id)
                                    <option selected="">{{ $service->title }}</option>
                                    @else
                                    <option>{{ $service->title }}</option>
                                    @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row pt-3 ">
                            <div class="col-12 col-md-3 d-flex align-items-center">
                                <div class="form-control-title">Sử dụng dịch vụ</div>
                            </div>
                            <div class="col-12 col-md-9">
                                <select class="form-select" aria-label="Default select example" disabled="">
                                    <option {{ $booking->calendar == '1 Lần'?'selected':'' }}>1 Lần</option>
                                    <option {{ $booking->calendar == 'Hàng tuần'?'selected':'' }}>Hàng tuần</option>
                                    <option {{ $booking->calendar == '2 tuần 1 lần'?'selected':'' }}>2 tuần 1 lần</option>
                                    <option {{ $booking->calendar == 'Hàng tháng'?'selected':'' }}>Hàng tháng</option>
                                </select>
                            </div>
                        </div>
                        <div class="row pt-3 ">
                            <div class="col-12 col-md-3 d-flex align-items-center">
                                <div class="form-control-title">Ngày / Thời gian</div>
                            </div>
                            <div class="col-12 col-md-9">
                                <div class="row">
                                    <div class="col-6 col-md-7">
                                        <input id="startDate" class="form-control" type="text" disabled="" value="{{ date_format(date_create($booking->date_view), 'd-m-Y') }}">
                                        <span id="startDateSelected"></span>
                                    </div>
                                    <div class="col-6 col-md-5">
                                        <input id="startDate" class="form-control" type="text" disabled="" value="{{ $booking->hour }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        @foreach($rooms as $room)
                        <div class="row pt-3 ">
                            <div class="col-12 col-md-3 d-flex align-items-center">
                                <div class="form-control-title">{{ $room->name }}</div>
                            </div>
                            <div class="col-12 col-md-5">
                                <select class="form-select" aria-label="Default select example" disabled="">
                                    @foreach($room->room as $rom)
                                    @foreach($booking->room as $broom)
                                    @if($broom->id == $rom->id)
                                    <option selected="">{{ $rom->name }}</option>
                                    @endif
                                    @endforeach
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <div class="other_service py-3 ">
                        <div class="row">
                            <div class="col-12 col-md-3">
                                <div class="title_form_booking">BẠN CÓ YÊU CẦU GÌ KHÁC</div>

                            </div>
                            <div class="col-12 col-md-9">
                                <textarea disabled="" rows="5" class="form-control"
                                    placeholder="Nếu bạn có những yêu cầu nào khác bạn vui lòng điền ở đây...">{{ $booking->other_request }}</textarea>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@stop
@section('css')
    <link id="pagestyle" href="/templates/client/assets/css/customer_page.css" rel="stylesheet" />
@stop