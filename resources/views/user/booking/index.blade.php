@extends('templates.user.master')
@section('content')

    <div class="container pt-2">

        <div class="row">
            <div class="col-12">

                <div class="container_booking">
                    <div class="page_history_title">Lịch sử book</div>


                </div>
            </div>
        </div>
        <div class="row history_gift_card">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-body px-0 pt-0 pb-2">
                        <div class="table-responsive p-0">
                            <table class="table align-items-center mb-0">
                                <thead>
                                    <tr>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                            Dich vụ & Địa chỉ</th>
                                        <th
                                            class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                            Trạng thái</th>
                                        <th
                                            class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                            Ngày</th>
                                        <th
                                            class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                            Hành động</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($bookings as $booking)
                                    <tr>
                                        <td>
                                            <div class="d-flex px-2 py-1">

                                                <div class="d-flex flex-column justify-content-center">
                                                    <h6 class="mb-0 text-sm">{{ $booking->service->title }}</h6>
                                                    <p class="text-xs text-secondary mb-0">{{ $booking->address }} - {{ $booking->district }} - {{ $booking->city }}</p>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="align-middle text-center text-sm">
                                            @if($booking->active == 1)
                                            <span class="badge badge-sm bg-gradient-success">Đã hoàn thành</span>
                                            @else
                                            <span class="badge badge-sm bg-gradient-info">Chưa hoàn thành</span>
                                            @endif
                                        </td>
                                        <td class="align-middle text-center">
                                            <span class="text-secondary text-xs font-weight-bold">giờ: {{ $booking->hour }} - ngày: {{ date_format(date_create($booking->date_view), 'd-m-Y') }}</span>
                                        </td>
                                        <td class="align-middle text-center">
                                            <a href="{{ route('user.booking.detail', $booking->id) }}" class="text-secondary btn btn-info font-weight-bold text-xs"
                                                data-toggle="tooltip" data-original-title="Edit user">
                                                Chi tiết
                                            </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('css')
    <link id="pagestyle" href="/templates/client/assets/css/customer_page.css" rel="stylesheet" />
@stop