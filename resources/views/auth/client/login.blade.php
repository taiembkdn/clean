<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Đăng nhập</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <!-- Bootstrap Icon -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">
    <!-- css Required  -->
    <link rel="stylesheet" href="/templates/client/assets/css/header.css">
    <link rel="stylesheet" href="/templates/client/assets/css/base.css">
    <!-- CSS Files -->
    <link id="pagestyle" href="/templates/client/assets/css/customer_page.css" rel="stylesheet" />
</head>

<body class="">
    <div class="container position-sticky z-index-sticky top-0">
        <div class="row">
            <div class="col-12">
                <!-- Navbar -->
                <nav
                    class="navbar navbar-expand-lg blur blur-rounded top-0 z-index-3 shadow position-absolute my-3 py-2 start-0 end-0 mx-4">
                    <div class="container-fluid d-flex justify-content-center">
                        <a class="font-weight-bolder  logo d-flex justify-content-center" href="{{ route('client.index.index') }}">
                            <img width="150px" src="/templates/client/assets/img/example-logo-png-7.png" alt="">
                        </a>

                    </div>
            </div>
            </nav>
            <!-- End Navbar -->
        </div>
    </div>

    <main class="main-content  mt-0">
        <section>
            <div class="page-header min-vh-75">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-4 col-lg-5 col-md-6 d-flex flex-column mx-auto">
                            <div class="card card-plain mt-8">
                                <div class="card-header pb-0 text-left bg-transparent">
                                    <h4 class="font-weight-bolder text-info text-gradient">Welcome back FreshHouse</h4>

                                    <small class="mb-0">Đăng nhập để theo dõi đặt dịch vụ, lên lịch lại, quản lý tiện
                                        ích bổ sung và hơn thế nữa.</small>
                                </div>
                                @include('errors.msg')
                                <div class="card-body">
@foreach ($errors->all() as $error)
<p style="color: red">
    {{ $error }}
</p>
@endforeach
                                    <form role="form" action="{{ route('auth.client.login') }}" method="post">
                                        @csrf
                                        <label>Email</label>
                                        <div class="mb-3">
                                            <input type="email" name="email" class="form-control" placeholder="Email"
                                                aria-label="Email" aria-describedby="email-addon">
                                        </div>
                                        <label>Password</label>
                                        <div class="mb-3">
                                            <input type="password" name="password" class="form-control" placeholder="Password"
                                                aria-label="Password" aria-describedby="password-addon">
                                        </div>

                                        <div class="text-center">
                                            <button type="submit" class="btn bg-gradient-info w-100 mt-2 mb-0">Đăng
                                                nhập</button>
                                        </div>
                                    </form>
                                </div>
                                <div class="row">
                                    <div class="col-3 ms-auto px-1">
                                        <a class="btn btn-outline-light w-100" href="javascript:;">
                                            <svg width="24px" height="32px" viewBox="0 0 64 64" version="1.1"
                                                xmlns="http://www.w3.org/2000/svg"
                                                xmlns:xlink="http://www.w3.org/1999/xlink32">
                                                <g id="Artboard" stroke="none" stroke-width="1" fill="none"
                                                    fill-rule="evenodd">
                                                    <g id="facebook-3" transform="translate(3.000000, 3.000000)"
                                                        fill-rule="nonzero">
                                                        <circle id="Oval" fill="#3C5A9A" cx="29.5091719" cy="29.4927506"
                                                            r="29.4882047"></circle>
                                                        <path
                                                            d="M39.0974944,9.05587273 L32.5651312,9.05587273 C28.6886088,9.05587273 24.3768224,10.6862851 24.3768224,16.3054653 C24.395747,18.2634019 24.3768224,20.1385313 24.3768224,22.2488655 L19.8922122,22.2488655 L19.8922122,29.3852113 L24.5156022,29.3852113 L24.5156022,49.9295284 L33.0113092,49.9295284 L33.0113092,29.2496356 L38.6187742,29.2496356 L39.1261316,22.2288395 L32.8649196,22.2288395 C32.8649196,22.2288395 32.8789377,19.1056932 32.8649196,18.1987181 C32.8649196,15.9781412 35.1755132,16.1053059 35.3144932,16.1053059 C36.4140178,16.1053059 38.5518876,16.1085101 39.1006986,16.1053059 L39.1006986,9.05587273 L39.0974944,9.05587273 L39.0974944,9.05587273 Z"
                                                            id="Path" fill="#FFFFFF"></path>
                                                    </g>
                                                </g>
                                            </svg>
                                        </a>
                                    </div>


                                    <div class="col-3 me-auto px-1">
                                        <a class="btn btn-outline-light w-100" href="/auth/google/redirect">
                                            <svg width="24px" height="32px" viewBox="0 0 64 64" version="1.1"
                                                xmlns="http://www.w3.org/2000/svg"
                                                xmlns:xlink="http://www.w3.org/1999/xlink">
                                                <g id="Artboard" stroke="none" stroke-width="1" fill="none"
                                                    fill-rule="evenodd">
                                                    <g id="google-icon" transform="translate(3.000000, 2.000000)"
                                                        fill-rule="nonzero">
                                                        <path
                                                            d="M57.8123233,30.1515267 C57.8123233,27.7263183 57.6155321,25.9565533 57.1896408,24.1212666 L29.4960833,24.1212666 L29.4960833,35.0674653 L45.7515771,35.0674653 C45.4239683,37.7877475 43.6542033,41.8844383 39.7213169,44.6372555 L39.6661883,45.0037254 L48.4223791,51.7870338 L49.0290201,51.8475849 C54.6004021,46.7020943 57.8123233,39.1313952 57.8123233,30.1515267"
                                                            id="Path" fill="#4285F4"></path>
                                                        <path
                                                            d="M29.4960833,58.9921667 C37.4599129,58.9921667 44.1456164,56.3701671 49.0290201,51.8475849 L39.7213169,44.6372555 C37.2305867,46.3742596 33.887622,47.5868638 29.4960833,47.5868638 C21.6960582,47.5868638 15.0758763,42.4415991 12.7159637,35.3297782 L12.3700541,35.3591501 L3.26524241,42.4054492 L3.14617358,42.736447 C7.9965904,52.3717589 17.959737,58.9921667 29.4960833,58.9921667"
                                                            id="Path" fill="#34A853"></path>
                                                        <path
                                                            d="M12.7159637,35.3297782 C12.0932812,33.4944915 11.7329116,31.5279353 11.7329116,29.4960833 C11.7329116,27.4640054 12.0932812,25.4976752 12.6832029,23.6623884 L12.6667095,23.2715173 L3.44779955,16.1120237 L3.14617358,16.2554937 C1.14708246,20.2539019 0,24.7439491 0,29.4960833 C0,34.2482175 1.14708246,38.7380388 3.14617358,42.736447 L12.7159637,35.3297782"
                                                            id="Path" fill="#FBBC05"></path>
                                                        <path
                                                            d="M29.4960833,11.4050769 C35.0347044,11.4050769 38.7707997,13.7975244 40.9011602,15.7968415 L49.2255853,7.66898166 C44.1130815,2.91684746 37.4599129,0 29.4960833,0 C17.959737,0 7.9965904,6.62018183 3.14617358,16.2554937 L12.6832029,23.6623884 C15.0758763,16.5505675 21.6960582,11.4050769 29.4960833,11.4050769"
                                                            id="Path" fill="#EB4335"></path>
                                                    </g>
                                                </g>
                                            </svg>
                                        </a>
                                    </div>

                                </div>
                                <div class="card-footer text-center pt-0 px-lg-2 px-1 mt-3">
                                    <p class="mb-4 text-sm mx-auto">
                                        Nếu bạn chưa có tài khoản?
                                        <a href="{{ route('auth.client.register') }}" class="text-info text-gradient font-weight-bold">Đăng
                                            ký</a>
                                        ngay.
                                    </p>
                                    <p class="mb-4 text-sm mx-auto">
                                        Quên mật khẩu, hãy
                                        <a href="{{ route('auth.client.forgetpassword') }}" class="text-info text-gradient font-weight-bold"> bấm vào đây</a>.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <!-- <div class="oblique oblique_new  position-absolute top-0 h-100 d-md-block d-none me-n8">
                                <div class="oblique-image bg-cover position-absolute fixed-top ms-auto h-100 z-index-0 ms-n6"
                                    style="background-image:url('/templates/client/assets/img/Illustratioon-Office-1.png')">
                                </div>
                            </div> -->
                            <div class="mt-8  d-md-block d-none">
                                <img class="w-100" src="/templates/client/assets/img/Illustratioon-Office-1.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <footer class="footer pt-5 py-2">
        <div class="container">
            <div class="row">
                <div class="col-8 mx-auto text-center mt-1">
                    <p class="mb-0 text-secondary">
                        Copyright ©
                        <script>
                            document.write(new Date().getFullYear())
                        </script> Soft by FreshHouse.
                    </p>
                </div>
            </div>
        </div>
    </footer>

</body>

</html>