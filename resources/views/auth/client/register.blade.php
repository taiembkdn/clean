
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Đăng ký</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <!-- Bootstrap Icon -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">
    <!-- css Required  -->
    <link rel="stylesheet" href="/templates/client/assets/css/header.css">
    <link rel="stylesheet" href="/templates/client/assets/css/base.css">
    <!-- CSS Files -->
    <link id="pagestyle" href="/templates/client/assets/css/customer_page.css" rel="stylesheet" />
</head>

<body class="">
    <div class="container position-sticky z-index-sticky top-0">
        <div class="page-header align-items-start min-vh-50 pt-3 pb-11 m-3 border-radius-lg"
            style="background:#2983fd;border-radius: 10px;">
            <span class="mask bg-gradient-dark opacity-6"></span>
            <div class="container pb-5">
                <div class="row justify-content-center">
                    <div class="col-lg-5 text-center mx-auto pb-2">
                        <a class="font-weight-bolder  logo d-flex justify-content-center" href="#">
                            <img class="w-50" src="/templates/client/assets/img/example-logo-png-7-trang.png" alt="">
                        </a>
                        <p class="text-lead text-white">Đăng ký để theo dõi đặt dịch vụ, lên lịch lại, quản lý tiện ích
                            bổ sung và hơn thế nữa.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <main class="main-content  mt-lg-n10 ">
        <section>
            <div class="page-header min-vh-75">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-4 col-lg-5 col-md-6 d-flex flex-column mx-auto">
                            <div class="card card-plain card-plain-custom mt-8">
                                <div class="card-header pb-0 text-left bg-transparent text-center pt-3">
                                    <h2 class="font-weight-bolder text-info text-gradient text-center">Đăng ký</h2>

                                    <small class="mb-0 text-center">Vui lòng điền vào form để tạo tài
                                        khoản mới.</small>
                                </div>

                                <div class="card-body">
                                    
@foreach ($errors->all() as $error)
<p style="color: red">
    {{ $error }}
</p>
@endforeach
                                    <form role="form" action="{{ route('auth.client.register') }}" method="post">
                                        @csrf
                                        <div class="mb-3">
                                            <input type="name" class="form-control" placeholder="Họ và tên"
                                                aria-label="Name" name="name" aria-describedby="name-addon">
                                        </div>
                                        <div class="mb-3">
                                            <input type="email" class="form-control" placeholder="Email"
                                                aria-label="Email" name="email" aria-describedby="email-addon">
                                        </div>
                                        <div class="mb-3">
                                            <input type="password" class="form-control" placeholder="Mật khẩu"
                                                aria-label="Password" name="password" aria-describedby="password-addon">
                                        </div>
                                        <div class="mb-3">
                                            <input type="password" class="form-control" placeholder="Nhập lại mật khẩu"
                                                aria-label="Password" name="password_confirmation" aria-describedby="password-addon">
                                        </div>

                                        <div class="text-center">
                                            <button type="submit" class="btn bg-gradient-info w-100 mt-2 mb-0">Đăng
                                                ký</button>
                                        </div>
                                    </form>
                                </div>

                                <div class="card-footer text-left pt-0 px-lg-2 px-1 mt-3">
                                    <p class="mb-2 text-sm ">
                                        Nếu bạn đã có tài khoản?
                                        <a href="{{ route('auth.client.login') }}" class="text-info text-gradient font-weight-bold">Đăng
                                            nhập</a>

                                    </p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </main>
    <footer class="footer pt-5 py-2">
        <div class="container">
            <div class="row">
                <div class="col-8 mx-auto text-center mt-1">
                    <p class="mb-0 text-secondary">
                        Copyright ©
                        <script>
                            document.write(new Date().getFullYear())
                        </script> Soft by FreshHouse.
                    </p>
                </div>
            </div>
        </div>
    </footer>

</body>

</html>