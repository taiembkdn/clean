<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web']], function () {
  \UniSharp\LaravelFilemanager\Lfm::routes();
});

Route::group(['namespace' => 'Client'], function () {
  Route::get('', 'IndexController@index')->name('client.index.index');
  Route::get('gioi-thieu', 'IndexController@about')->name('client.index.about');
  Route::get('lien-he', 'IndexController@getContact')->name('client.index.contact');
  Route::post('lien-he', 'IndexController@postContact')->name('client.index.contact');
  Route::get('su-menh', 'IndexController@mission')->name('client.index.mission');
  Route::get('cong-nghe', 'IndexController@technology')->name('client.index.technology');
  
  Route::get('bai-viet', 'BlogController@index')->name('client.blog.index');
  Route::get('danh-muc/{slug}', 'BlogController@category')->name('client.blog.category');
  Route::get('bai-viet/{slug}', 'BlogController@detail')->name('client.blog.detail');

  Route::get('dich-vu', 'ServiceController@category')->name('client.service.category');
  Route::get('dich-vu/{slug}', 'ServiceController@detail')->name('client.service.detail');
  Route::get('dat-lich', 'ServiceController@bookings')->name('client.service.bookings');
  Route::get('dat-lich/{slug}', 'ServiceController@booking')->name('client.service.booking');
  Route::post('dat-lich', 'ServiceController@postBooking')->name('client.service.booking');
  Route::get('dat-lich-thanh-cong', 'ServiceController@success')->name('client.service.success');
});

Route::namespace('Auth')->group(function () {
  Route::get('dang-nhap', 'ClientController@showLoginForm')->name('auth.client.login');
  Route::post('dang-nhap', 'ClientController@login')->name('auth.client.login');
  Route::get('dang-ky', 'ClientController@showRegisterForm')->name('auth.client.register');
  Route::post('dang-ky', 'ClientController@register')->name('auth.client.register');
  Route::get('quen-mat-khau', 'ClientController@forgetpassword')->name('auth.client.forgetpassword');
  Route::post('quen-mat-khau', 'ClientController@postForget')->name('auth.client.forgetpassword');
  Route::get('dang-xuat', 'ClientController@logout')->name('auth.client.logout');
});


 Route::get('/auth/{provider}/redirect', 'Auth\ClientController@redirect');
 Route::get('/auth/{provider}/callback', 'Auth\ClientController@callback');

Route::group(['namespace' => 'User', 'middleware' => ['auth:web']], function () {
  Route::get('tai-khoan', 'IndexController@index')->name('user.index.index');
  Route::get('thong-tin', 'IndexController@profile')->name('user.index.profile');
  Route::post('updateProfile', 'IndexController@updateProfile')->name('user.index.updateProfile');
  Route::post('updatePassword', 'IndexController@updatePassword')->name('user.index.updatePassword');
  
  Route::get('lich-su-dat-lich', 'BookingController@index')->name('user.booking.index');
  Route::get('user/dat-lich', 'BookingController@booking')->name('user.booking.booking');
  Route::post('user/dat-lich', 'BookingController@postBooking')->name('user.booking.booking');
  Route::get('lich-su-dat-lich/chi-tiet/{id}', 'BookingController@detail')->name('user.booking.detail');
});



