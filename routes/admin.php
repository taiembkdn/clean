<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::namespace('Auth')->group(function () {
    Route::get('login', 'AdminController@showLoginForm')->name('auth.admin.login');
    Route::post('login', 'AdminController@login')->name('auth.admin.login');
    Route::get('logout', 'AdminController@logout')->name('auth.admin.logout');
 });
 
Route::group(['namespace' => 'Admin', 'middleware' => ['auth:admin']], function () {

    Route::get('', 'IndexController@index')->name('admin.index.index');

    Route::get('category/delete/{id}', 'CategoryController@delete')->name('admin.category.delete');
    Route::resource('category', 'CategoryController', ['as' => 'admin']);

    Route::get('room/delete/{id}', 'RoomController@delete')->name('admin.room.delete');
    Route::resource('room', 'RoomController', ['as' => 'admin']);

    Route::get('special/delete/{id}', 'SpecialController@delete')->name('admin.special.delete');
    Route::resource('special', 'SpecialController', ['as' => 'admin']);

    Route::get('admin/delete/{id}', 'AdminController@delete')->name('admin.admin.delete');
    Route::resource('admin', 'AdminController', ['as' => 'admin']);

    Route::get('gift/delete/{id}', 'GiftController@delete')->name('admin.gift.delete');
    Route::get('gift/active', 'GiftController@active')->name('admin.gift.active');
    Route::resource('gift', 'GiftController', ['as' => 'admin']);

    Route::get('service/addProcess/{id}', 'ServiceController@addProcess')->name('admin.service.addProcess');
    Route::post('service/addProcess/{id}', 'ServiceController@postAddProcess')->name('admin.service.postAddProcess');
    Route::get('service/editProcess/{id}', 'ServiceController@editProcess')->name('admin.service.editProcess');
    Route::post('service/editProcess/{id}', 'ServiceController@postEditProcess')->name('admin.service.postEditProcess');
    Route::get('service/deleteProcess/{id}', 'ServiceController@deleteProcess')->name('admin.service.deleteProcess');

    Route::get('service/delete/{id}', 'ServiceController@delete')->name('admin.service.delete');
    Route::get('service/active', 'ServiceController@active')->name('admin.service.active');
    Route::resource('service', 'ServiceController', ['as' => 'admin']);

    Route::get('blog/delete/{id}', 'BlogController@delete')->name('admin.blog.delete');
    Route::get('blog/active', 'BlogController@active')->name('admin.blog.active');
    Route::resource('blog', 'BlogController', ['as' => 'admin']);

    Route::get('recruit/delete/{id}', 'RecruitController@delete')->name('admin.recruit.delete');
    Route::get('recruit/active', 'RecruitController@active')->name('admin.recruit.active');
    Route::resource('recruit', 'RecruitController', ['as' => 'admin']);

    Route::get('header/delete/{id}', 'HeaderController@delete')->name('admin.header.delete');
    Route::resource('header', 'HeaderController', ['as' => 'admin']);

    Route::get('picture/delete/{id}', 'PictureController@delete')->name('admin.picture.delete');
    Route::get('picture/active', 'PictureController@active')->name('admin.picture.active');
    Route::resource('picture', 'PictureController', ['as' => 'admin']);

    Route::get('booking/delete/{id}', 'BookingController@delete')->name('admin.booking.delete');
    Route::get('booking/active', 'BookingController@active')->name('admin.booking.active');
    Route::resource('booking', 'BookingController', ['as' => 'admin']);

    Route::get('contact/delete/{id}', 'ContactController@delete')->name('admin.contact.delete');
    Route::resource('contact', 'ContactController', ['as' => 'admin']);

    Route::get('user/delete/{id}', 'UserController@delete')->name('admin.user.delete');
    Route::get('user/active', 'UserController@active')->name('admin.user.active');
    Route::resource('user', 'UserController', ['as' => 'admin']);
    // Route::group(['middleware' => 'auth'], function () {
    // });

});