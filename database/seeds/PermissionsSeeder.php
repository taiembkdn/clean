<?php

use App\Permission;
use Illuminate\Database\Seeder;

class PermissionsSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    Permission::create([
      'name' => 'role-list',
      'display_name' => 'Quản lý vai trò <span class="text-danger">(Admin)</span>',
    ]);
    Permission::create([
      'name' => 'role-create',
      'display_name' => 'Thêm vai trò <span class="text-danger">(Admin)</span>',
    ]);
    Permission::create([
      'name' => 'role-edit',
      'display_name' => 'Sửa vai trò <span class="text-danger">(Admin)</span>',
    ]);
    Permission::create([
      'name' => 'role-delete',
      'display_name' => 'Xoá vai trò <span class="text-danger">(Admin)</span>',
    ]);
    Permission::create([
      'name' => 'user-list',
      'display_name' => 'Quản lý tài khoản <span class="text-danger">(Admin)</span>',
    ]);
    Permission::create([
      'name' => 'user-create',
      'display_name' => 'Tạo tài khoản <span class="text-danger">(Admin)</span>',
    ]);
    Permission::create([
      'name' => 'user-edit',
      'display_name' => 'Sửa tài khoản <span class="text-danger">(Admin)</span>',
    ]);
    Permission::create([
      'name' => 'user-delete',
      'display_name' => 'Xoá tài khoản <span class="text-danger">(Admin)</span>',
    ]);
    Permission::create([
      'name' => 'student-list',
      'display_name' => 'Xem danh sách tất cả học viên <span class="text-danger">(Admin)</span>',
    ]);
    Permission::create([
      'name' => 'student-show',
      'display_name' => 'Xem học viên của mình <span class="text-danger">(Admin</span> - <span class="text-success">User)</span>',
    ]);
    Permission::create([
      'name' => 'statistic-list',
      'display_name' => 'Quản lý thống kê <span class="text-danger">(Admin</span> - <span class="text-success">User)</span>',
    ]);
    Permission::create([
      'name' => 'statistic-student',
      'display_name' => 'Thống kê danh sách học viên <span class="text-danger">(Admin</span> - <span class="text-success">User)</span>',
    ]);
  }
}
