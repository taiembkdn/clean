<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required|email',
            'title' => 'required',
            'description' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'required' => 'Vui lòng nhập :attribute',
            'email' => 'Vui lòng chọn đúng định dạng :attribute',
        ];
    }
    public function attributes()
    {
        return [
            'name' => 'tên',
            'email' => 'email',
            'phone' => 'số điện thoại',
            'title' => 'tiêu đề',
            'description' => 'nội dung',
        ];
    }
}
