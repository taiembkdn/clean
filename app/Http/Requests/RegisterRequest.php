<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed',
        ];
    }
    public function messages()
    {
        return [
            'required' => 'Vui lòng nhập :attribute',
            'email' => 'Vui lòng chọn đúng định dạng :attribute',
            'unique' => ':attribute trên đã được đăng ký',
            'confirmed' => 'Lỗi :attribute không trùng nhau',
        ];
    }
    public function attributes()
    {
        return [
            'email' => 'email',
            'password' => 'password',
            'name' => 'tên',
        ];
    }
}
