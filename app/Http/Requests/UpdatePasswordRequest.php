<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;
use Hash;

class UpdatePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'currentPassword' => [
                'required',
                
                function ($attribute, $value, $fail) {
                    if (!Hash::check($value, Auth()->guard('web')->user()->password)) {
                        $fail('Mật khẩu cũ không đúng.');
                    }
                }
            ],
            'password' => [
                'required', 'min:6', 'confirmed', 'different:currentPassword'
            ]
        ];
    }
    public function messages()
    {
        return [
            'required' => 'Vui lòng nhập :attribute',
            'confirmed' => 'Lỗi :attribute không trùng nhau',
        ];
    }
    public function attributes()
    {
        return [
            'currentPassword' => 'mật khẩu cũ',
            'password' => 'mật khẩu mới',
        ];
    }
}
