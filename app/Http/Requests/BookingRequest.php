<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => 'required',
            'hour' => 'required',
            'name' => 'required',
            'email' => 'required|email',
            'phone_number' => 'required',
            'address' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'required' => 'Vui lòng nhập :attribute',
            'email' => 'Vui lòng chọn đúng định dạng :attribute',
        ];
    }
    public function attributes()
    {
        return [
            'email' => 'email',
            'name' => 'tên',
            'date' => 'ngày',
            'hour' => 'giờ',
            'phone_number' => 'số điện thoại',
            'address' => 'địa chỉ',
        ];
    }
}
