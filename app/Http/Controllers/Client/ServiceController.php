<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Service;
use App\User;
use App\Models\Room;
use App\Models\Booking;
use App\Models\RoomBooking;
use App\Models\SpecialRequestBooking;
use App\Models\SpecialRequest;
use App\Http\Requests\BookingRequest;

class ServiceController extends Controller
{
    public function category(){
    	$services = Service::where('active', 1)->get();
        return view('client.service.category', compact('services'));
    }

    public function detail($slug){
    	$service = Service::where('slug', 'like', '%'.$slug.'%')->where('active', 1)->first();
    	if(!$service){
    		return redirect()->back();
    	}
        return view('client.service.detail', compact('service'));
    }
    public function bookings(){
        $rooms = Room::where('room_id', 0)->get();
        $specials = SpecialRequest::all();
    	$services = Service::all();
        $slug = null;
        return view('client.service.booking', compact('services', 'slug', 'rooms', 'specials'));
    }
    public function booking($slug = null){
        $rooms = Room::where('room_id', 0)->get();
        $specials = SpecialRequest::all();
        $services = Service::all();
        if($slug != null){
            $service = Service::where('slug', 'like', $slug)->first();
            if(!$service){
                return redirect('/dat-lich');
            }
        }
        return view('client.service.booking', compact('services', 'slug', 'rooms', 'specials'));
    }

    public function postBooking(BookingRequest $request){
        $user = User::where('email', $request->email)->first();
        if(!$user){
            $user = new User();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = bcrypt(substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZ"), -5));
            $user->address = $request->address;
            $user->district = $request->district;
            $user->city = $request->city;
            $user->save();
        }else{
            $user->address = $request->address;
            $user->district = $request->district;
            $user->city = $request->city;
            $user->update();
        }

        $booking = new Booking();
        $booking->user_id = $user->id;
        $booking->service_id = $request->service_id;
        $booking->note = $request->other;
        $booking->hour = $request->hour;
        $booking->date_view = $request->date;
        $booking->calendar = $request->calendar;
        $booking->know = $request->know;
        $booking->other_request = $request->other_request;
        $booking->save();

        foreach($request->room as $room){
            $rom = new RoomBooking();
            $rom->room_id = $room;
            $rom->booking_id = $booking->id;
            $rom->save();
        }

        foreach($request->special as $special){
            $speci = new SpecialRequestBooking();
            $speci->special_request_id = $special;
            $speci->booking_id = $booking->id;
            $speci->save();
        }

        return redirect()->route('client.service.success');
    }

    public function success(){
        return view('client.service.success');
    }
}
