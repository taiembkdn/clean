<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\ContactRequest;
use App\Models\Contact;
use App\Models\Service;
use App\Models\Blog;
use App\Models\Picture;

class IndexController extends Controller
{
    public function index(){
        $services = Service::where('active', 1)->get();
        $pictures = Picture::where('active', 1)->get();
        $blogs = Blog::where('active', 1)->orderBy('id', 'desc')->limit(3)->get();
        return view('client.index.index', compact('services', 'blogs', 'pictures'));
    }

    public function about(){
        return view('client.index.about');
    }

    public function getContact(){
        return view('client.index.contact');
    }

    public function postContact(ContactRequest $request){
        $contact = new Contact();
        $contact->name = $request->name;
        $contact->email = $request->email;
        $contact->phone = $request->phone;
        $contact->title = $request->title;
        $contact->description = $request->description;
        $contact->save();

        return redirect()->back()->with('success', 'Gửi liên hệ thành công');
    }

    public function mission(){
        return view('client.index.mission');
    }

    public function technology(){
        return view('client.index.technology');
    }
}
