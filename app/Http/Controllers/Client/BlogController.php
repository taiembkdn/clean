<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Blog;
use App\Models\Category;

class BlogController extends Controller
{
    public function index(){
    	$categories = Category::all();
    	$rand_blogs = Blog::where('active', 1)->orderBy('view', 'desc')->limit(5)->get();
    	$blogs = Blog::where('active', 1)->orderBy('id', 'desc')->limit(10)->get();
        return view('client.blog.category', compact('categories', 'rand_blogs', 'blogs'));
    }
    public function category($slug){
    	$cat = Category::where('slug', 'like', '%'. $slug.'%')->first();
    	if(!$cat){
    		return redirect()->back();
    	}
    	$categories = Category::all();
    	$rand_blogs = Blog::where('active', 1)->orderBy('view', 'desc')->limit(5)->get();
    	$blogs = Blog::where('active', 1)->where('category_id', $cat->id)->orderBy('id', 'desc')->limit(10)->get();
        return view('client.blog.category', compact('categories', 'rand_blogs', 'blogs'));
    }

    public function detail($slug){
    	$blog = Blog::where('active', 1)->where('slug', 'like', '%'.$slug.'%')->first();
        if(!$blog){
            return redirect()->back();
        }
        $blog->increment('view',1);
        $categories = Category::all();
        $rand_blogs = Blog::where('active', 1)->orderBy('view', 'desc')->limit(5)->get();
        return view('client.blog.detail', compact('blog', 'categories', 'rand_blogs'));
    }
}
