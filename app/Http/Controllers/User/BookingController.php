<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Booking;
use App\Models\SpecialRequest;
use App\Models\Service;
use App\Models\Room;
use Auth;

class BookingController extends Controller
{
    public function index(){
    	$bookings = Booking::where('user_id', Auth::guard('web')->user()->id)->get();
    	return view('user.booking.index', compact('bookings'));
    }
    public function detail($id){
    	$rooms = Room::where('room_id', 0)->get();
    	$specials = SpecialRequest::all();
    	$services = Service::all();
    	$booking = Booking::findOrFail($id);
    	return view('user.booking.detail', compact('booking', 'specials', 'services', 'rooms'));
    }
    public function booking(){
        $services = Service::all();
        $rooms =Room::where('room_id', 0)->get();
        $specials = SpecialRequest::all();
        return view('user.booking.booking', compact('services', 'rooms', 'specials'));
    }
    public function postBooking(Request $request){
        
    }
}
