<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\UpdatePasswordRequest;
use App\Http\Requests\UpdateProfileRequest;
use Auth;

class IndexController extends Controller
{
    public function index(){
        return view('user.index.index');
    }
    public function profile(){
        return view('user.index.profile');
    }
    public function updateProfile(UpdateProfileRequest $request){
    	$user = Auth::guard('web')->user();
    	$user->name = $request->name;
    	$user->phone_number = $request->phone_number;
    	$user->address = $request->address;
    	$user->district = $request->district;
    	$user->city = $request->city;

    	if($request->file('avatar')){
            if($user->avatar){
                $app_path = str_replace("\\", '/', public_path());
                $image = $app_path.'/upload/'.$user->avatar;
                if (file_exists($image)){
                    unlink($image);
                }
            }
            $icon = $request->file('avatar');
            $file =  $icon->storeAs('service','service-'.time().'.jpg', ['disk' => 'upload']);
        	$user->avatar = $file;
            $user->provider = null;
        }

        $user->update();
        return redirect()->route('user.index.profile')->with('success', 'Cập nhật thành công');
    }
    public function updatePassword(UpdatePasswordRequest $request){
    	$user = Auth::guard('web')->user();
    	$user->password = bcrypt($request->password);
    	$user->update();
        return redirect()->route('user.index.profile')->with('success', 'Cập nhật thành công');
    }
}
