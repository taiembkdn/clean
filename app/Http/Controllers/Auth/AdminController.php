<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth;

class AdminController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    public function __construct()
    {
        $this->middleware('guest:admin')->except('logout'); 
    }

    protected $redirectTo = '/admin';
    
    protected function guard()
    {
        return Auth::guard('admin');
    } 

    public function showLoginForm()
    {
        return view('auth.admin.login');
    }
    
    public function login(Request $request){
        
        $email = trim($request->email);
        $password = trim($request->password);

        $credentials = [
            'email' => $email,
            'password' => $password,
        ];

        if(Auth::guard('admin')->attempt(['email' => $email, 'password' => $password])){
            return redirect()->route('admin.index.index');
        } else{
            return redirect()->route('auth.admin.login')->with('error','Sai username hoặc password');
        }

    }

    public function logout(){
        $this->guard('admin')->logout();
        return redirect()->route('auth.admin.login');
    }

}
