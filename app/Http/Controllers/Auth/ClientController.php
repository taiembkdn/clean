<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\LoginRequest;
use App\User;
use Laravel\Socialite\Facades\Socialite;

class ClientController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = RouteServiceProvider::HOME;


    public function __construct()
    {
        $this->middleware('guest:web')->except('logout'); 
    }

    protected $redirectTo = '/';
    
    protected function guard()
    {
        return Auth::guard('web');
    }

    public function forgetpassword()
    {
        return view('auth.client.forgetpassword');
    }

    public function postForget()
    {
        return view('auth.client.forgetpassword');
    }

    public function showRegisterForm()
    {
        return view('auth.client.register');
    }

    public function showLoginForm()
    {
        return view('auth.client.login');
    }

    public function register(RegisterRequest $request)
    {
        $user = new User();
        $user->name = $request->name;
        $user->password = trim(bcrypt($request->password));
        $user->email = $request->email;
        $user->save();
        return redirect(route('auth.client.login'))->with('success', 'Đăng ký thành công, xin mời đăng nhập');
    }

    public function login(LoginRequest $request){
        $email = trim($request->email);
        $password = trim($request->password);
        if(Auth::guard('web')->attempt(['email' => $email, 'password' => $password])){
            return redirect()->route('user.index.index');
        } else{
            return redirect()->route('auth.client.login')->with('error','Sai username hoặc password');
        }

    }

    public function logout(){
        $this->guard('web')->logout();
        return redirect()->route('auth.client.login');
    }

    public function redirect($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function callback($provider)
    {
        switch ($provider) {
            case 'facebook':
              if (!$request->has('code') || $request->has('denied')) {
                return redirect()->route('auth.client.login');
              }
              $user = Socialite::driver($provider)->stateless()->user();
              $account = User::where('provider_id', $user->id)->first();
              if ($account) {
                Auth::login($account);
                return redirect()->route('user.index.index');
              } else {
                $account = User::firstOrCreate([
                  'name' => $user->name,
                  'email' => $user->id."@balofun.com",
                  'provider' => "facebook",
                  'provider_id' => $user->id,
                  'password' => bcrypt('freshhouse'),
                  'avatar' => $user->avatar,
                  'email_verified_at' => date("Y-m-d H:i:s"),
                  'remember_token' => Str::random(80),
                ]);
                Auth::login($account);
                return redirect()->route('user.index.index');
              }
              break;
      
            case 'google':
              $user = Socialite::driver($provider)->user();
              $account = User::where('email', $user->email)->first();
              if ($account) {
                Auth::login($account);
                return redirect()->route('user.index.index');
              } else {
                $account = User::firstOrCreate([
                  'name' => $user->name,
                  'email' => $user->email,
                  'provider' => "google",
                  'provider_id' => $user->user['id'],
                  'password' => bcrypt('freshhouse'),
                  'avatar' => $user->user['picture'],
                  'email_verified_at' => date("Y-m-d H:i:s"),
                  'remember_token' => str_random(80),
                ]);
                Auth::login($account);
                return redirect()->route('user.index.index');
              }
              break;
      
            default:
              break;
          }
    }
}
