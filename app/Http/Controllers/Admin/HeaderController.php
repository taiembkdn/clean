<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Header;

class HeaderController extends Controller
{
    public function index(){
        $headers = Header::orderBy('id', 'desc')->get();
        return view('admin.header.index', compact('headers'));
    }

    public function create(){
        return view('admin.header.add');
    }

    public function store(Request $request){
        $header = new Header();
        $header->url = $request->url;
        $header->header = $request->header;
        $header->footer = $request->footer;
        $header->save();
        return redirect()->route('admin.header.index')->with('success', 'Thêm thành công');
    }

    public function edit($id){
        $header = Header::findOrFail($id);
        return view('admin.header.edit', compact('header'));
    }

    public function update(Request $request, $id){
        $header = Header::findOrFail($id);
        $header->url = $request->url;
        $header->header = $request->header;
        $header->footer = $request->footer;
        $header->update();
        return redirect()->route('admin.header.index')->with('success', 'Sửa thành công');
    }

    public function delete($id){
        
        $header = Header::findOrFail($id);

        $header->delete();
        return redirect()->route('admin.header.index')->with('success', 'Xóa thành công');
    }
}
