<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Gift;

class GiftController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gifts = Gift::orderBy('id', 'desc')->get();
        return view('admin.gift.index', compact('gifts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.gift.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category = new Gift();
        $category->code = $request->code;
        $category->type = $request->type;
        $category->value = $request->value;
        $category->save();
        return redirect()->route('admin.gift.index')->with('success', 'Thêm thành công');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $gift = Gift::findOrFail($id);
        return view('admin.gift.edit', compact('gift'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Gift::findOrFail($id);
        $category->code = $request->code;
        $category->type = $request->type;
        $category->value = $request->value;
        $category->update();
        return redirect()->route('admin.gift.index')->with('success', 'Sửa thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }

    public function delete($id)
    {
        $cate = Gift::find($id);
        
        if(!$cate){
            return redirect()->route('admin.gift.index')->with('error', 'Không tìm thấy mã giảm giá trên');
        }
        $cate->delete();
        return redirect()->route('admin.gift.index')->with('success', 'Xóa thành công');
    }
    
    public function active(Request $request)
    {
        $id = $request->id;
        $gift = Gift::find($id);


        if(!$gift){
            return redirect()->route('admin.gift.index')->with('error', 'Không tìm thấy mã giảm giá trên');
        }
        if($gift->active == 0){
            $gift->active = 1;
            $gift->update();
            return '<a onclick="return getActive('.$gift->id.')" class="text-success" href="javascript:void(0)">Đang hoạt động</a>';
        }else{
            $gift->active = 0;
            $gift->update();
            return '<a onclick="return getActive('.$gift->id.')" class="text-danger" href="javascript:void(0)">Ngưng hoạt động</a>';
        }
    }
}
