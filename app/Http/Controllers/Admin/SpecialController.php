<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SpecialRequest;

class SpecialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $specials = SpecialRequest::orderBy('id', 'desc')->get();
        return view('admin.special.index', compact('specials'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.special.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $special = new SpecialRequest();
        $special->name = $request->name;
        $special->slug = str_slug($request->name);

        if($request->file('icon')){
            $icon = $request->file('icon');
            $file =  $icon->storeAs('icon','icon-'.time().'.jpg', ['disk' => 'upload']);
        }
        $special->icon = $file;

        $special->save();
        return redirect()->route('admin.special.index')->with('success', 'Thêm thành công');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $special = SpecialRequest::findOrFail($id);
        return view('admin.special.edit', compact('special'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $special = SpecialRequest::findOrFail($id);
        $special->name = $request->name;
        $special->slug = str_slug($request->name);

        $file = $special->icon;
        if($request->file('icon')){
            if($request->icon){
                $app_path = str_replace("\\", '/', public_path());
                $image = $app_path.'/upload/'.$special->icon;
                if (file_exists($image)){
                    unlink($image);
                }
            }
            $icon = $request->file('icon');
            $file =  $icon->storeAs('icon','icon-'.time().'.jpg', ['disk' => 'upload']);
        }
        $special->icon = $file;

        $special->update();
        return redirect()->route('admin.special.index')->with('success', 'Sửa thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }

    public function delete($id)
    {
        $cate = SpecialRequest::find($id);
        
        if(!$cate){
            return redirect()->route('admin.special.index')->with('error', 'Không tìm thấy yêu cầu trên');
        }
        $cate->delete();
        return redirect()->route('admin.special.index')->with('success', 'Xóa thành công');
    }
}
