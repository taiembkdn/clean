<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Service;
use App\Models\Process;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Service::orderBy('id', 'desc')->get();
        return view('admin.service.index', compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.service.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $service = new Service();

        if($request->file('picture')){
            $icon = $request->file('picture');
            $file =  $icon->storeAs('service','service-'.time().'.jpg', ['disk' => 'upload']);
        }


        $service->title = $request->title;
        $service->slug = str_slug($request->title);
        $service->description = $request->description;
        $service->detail = $request->detail;
        $service->picture = $file;
        $service->save();
        return redirect()->route('admin.service.index')->with('success', 'Thêm thành công');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service = Service::findOrFail($id);
        return view('admin.service.edit', compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $service = Service::findOrFail($id);
        $file = $service->picture;

        if($request->file('picture')){
            if($request->picture){
                $app_path = str_replace("\\", '/', public_path());
                $image = $app_path.'/upload/'.$service->picture;
                if (file_exists($image)){
                    unlink($image);
                }
            }
            $icon = $request->file('picture');
            $file =  $icon->storeAs('service','service-'.time().'.jpg', ['disk' => 'upload']);
        }


        $service->title = $request->title;
        $service->slug = str_slug($request->title);
        $service->description = $request->description;
        $service->detail = $request->detail;
        $service->picture = $file;
        $service->update();
        return redirect()->route('admin.service.index')->with('success', 'Sửa thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }

    public function delete($id)
    {
        $service = Service::find($id);
        
        if(!$service){
            return redirect()->route('admin.service.index')->with('error', 'Không tìm thấy bài viết trên');
        }

        if($service->picture){
            $app_path = str_replace("\\", '/', public_path());
            $image = $app_path.'/upload/'.$service->picture;
            if (file_exists($image)){
                unlink($image);
            }
        }
        foreach($service->process as $process){
            $process->delete();
        }

        $service->delete();
        return redirect()->route('admin.service.index')->with('success', 'Xóa thành công');
    }
    
    public function active(Request $request)
    {
        $id = $request->id;
        $service = Service::find($id);


        if(!$service){
            return redirect()->route('admin.service.index')->with('error', 'Không tìm thấy bài viết trên');
        }
        if($service->active == 0){
            $service->active = 1;
            $service->update();
            return '<a onclick="return getActive('.$service->id.')" class="text-success" href="javascript:void(0)">Có hiển thị</a>';
        }else{
            $service->active = 0;
            $service->update();
            return '<a onclick="return getActive('.$service->id.')" class="text-danger" href="javascript:void(0)">Không hiển thị</a>';
        }
    }

    public function addProcess($id){
        return view('admin.service.addProcess', compact('id'));
    }

    public function postAddProcess(Request $request, $id){
        $process = new Process();
        $process->service_id = $id;
        $process->name = $request->name;
        $process->slug = str_slug($request->name);
        $process->save();
        return redirect()->route('admin.service.index')->with('success', 'Thêm thành công');
    }

    public function editProcess($id){
        $process = Process::findOrFail($id);
        return view('admin.service.editProcess', compact('process'));
    }

    public function postEditProcess(Request $request, $id){
        $process = Process::find($id);
        $process->name = $request->name;
        $process->slug = str_slug($request->name);
        $process->update();
        return redirect()->route('admin.service.index')->with('success', 'Sửa thành công');
    }

    public function deleteProcess($id)
    {
        $process = Process::find($id);
        
        if(!$process){
            return redirect()->route('admin.service.index')->with('error', 'Không tìm thấy tiến trình trên');
        }

        $process->delete();
        return redirect()->route('admin.service.index')->with('success', 'Xóa thành công');
    }
}
