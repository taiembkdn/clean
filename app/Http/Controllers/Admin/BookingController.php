<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Booking;

class BookingController extends Controller
{
	public function index()
    {
        $bookings = Booking::orderBy('id', 'desc')->get();
        return view('admin.booking.index', compact('bookings'));
    }

    public function show($id){
    	$booking = Booking::findOrFail($id);
    	return view('admin.booking.show', compact('booking'));
    }
}
