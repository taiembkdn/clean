<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Picture;

class PictureController extends Controller
{
    public function index(){
        $pictures = Picture::orderBy('id', 'desc')->get();
        return view('admin.picture.index', compact('pictures'));
    }

    public function create(){
        return view('admin.picture.add');
    }

    public function store(Request $request){
        $picture = new Picture();
        $picture->description = $request->description;

        if($request->file('picture')){
            $icon = $request->file('picture');
            $file =  $icon->storeAs('picture','picture-'.time().'.jpg', ['disk' => 'upload']);
            $picture->url = $file;
        }else{
            return redirect()->back()->with('error', 'Bạn chưa chọn hình ảnh');
        }

        $picture->save();
        return redirect()->route('admin.picture.index')->with('success', 'Thêm thành công');
    }

    public function edit($id){
        $picture = Picture::findOrFail($id);
        return view('admin.picture.edit', compact('picture'));
    }

    public function update(Request $request, $id){
        $picture = Picture::findOrFail($id);

        $picture->description = $request->description;

        if($request->file('picture')){
            if($request->picture){
                $app_path = str_replace("\\", '/', public_path());
                $image = $app_path.'/upload/'.$picture->url;
                if (file_exists($image)){
                    unlink($image);
                }
            }
            $icon = $request->file('picture');
            $file =  $icon->storeAs('blog','blog-'.time().'.jpg', ['disk' => 'upload']);
            $picture->url = $file;
        }

        $picture->update();
        return redirect()->route('admin.picture.index')->with('success', 'Sửa thành công');
    }

    public function delete($id){
        
        $picture = Picture::findOrFail($id);

        $picture->delete();
        return redirect()->route('admin.picture.index')->with('success', 'Xóa thành công');
    }
    
    public function active(Request $request)
    {
        $id = $request->id;
        $picture = Picture::find($id);


        if(!$picture){
            return redirect()->route('admin.picture.index')->with('error', 'Không tìm thấy hình ảnh trên');
        }
        if($picture->active == 0){
            $picture->active = 1;
            $picture->update();
            return '<a onclick="return getActive('.$picture->id.')" class="text-success" href="javascript:void(0)">Có hiển thị</a>';
        }else{
            $picture->active = 0;
            $picture->update();
            return '<a onclick="return getActive('.$picture->id.')" class="text-danger" href="javascript:void(0)">Không hiển thị</a>';
        }
    }
}
