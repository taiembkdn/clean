<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Recruit;

class RecruitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $recruits = Recruit::orderBy('id', 'desc')->get();
        return view('admin.recruit.index', compact('recruits'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.recruit.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $recruit = new recruit();

        if($request->file('picture')){
            $icon = $request->file('picture');
            $file =  $icon->storeAs('recruit','recruit-'.time().'.jpg', ['disk' => 'upload']);
        }


        $recruit->title = $request->title;
        $recruit->slug = str_slug($request->title);
        $recruit->description = $request->description;
        $recruit->detail = $request->detail;
        $recruit->picture = $file;
        $recruit->save();
        return redirect()->route('admin.recruit.index')->with('success', 'Thêm thành công');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $recruit = Recruit::findOrFail($id);
        return view('admin.recruit.edit', compact('recruit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $recruit = Recruit::findOrFail($id);
        $file = $recruit->picture;

        if($request->file('picture')){
            if($request->picture){
                $app_path = str_replace("\\", '/', public_path());
                $image = $app_path.'/upload/'.$recruit->picture;
                if (file_exists($image)){
                    unlink($image);
                }
            }
            $icon = $request->file('picture');
            $file =  $icon->storeAs('recruit','recruit-'.time().'.jpg', ['disk' => 'upload']);
        }


        $recruit->title = $request->title;
        $recruit->slug = str_slug($request->title);
        $recruit->description = $request->description;
        $recruit->detail = $request->detail;
        $recruit->picture = $file;
        $recruit->update();
        return redirect()->route('admin.recruit.index')->with('success', 'Sửa thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }

    public function delete($id)
    {
        $recruit = Recruit::find($id);
        
        if(!$recruit){
            return redirect()->route('admin.recruit.index')->with('error', 'Không tìm thấy tuyển dụng trên');
        }

        if($recruit->picture){
            $app_path = str_replace("\\", '/', public_path());
            $image = $app_path.'/upload/'.$recruit->picture;
            if (file_exists($image)){
                unlink($image);
            }
        }

        $recruit->delete();
        return redirect()->route('admin.recruit.index')->with('success', 'Xóa thành công');
    }
    
    public function active(Request $request)
    {
        $id = $request->id;
        $recruit = Recruit::find($id);


        if(!$recruit){
            return redirect()->route('admin.recruit.index')->with('error', 'Không tìm thấy tuyển dụng trên');
        }
        if($recruit->active == 0){
            $recruit->active = 1;
            $recruit->update();
            return '<a onclick="return getActive('.$recruit->id.')" class="text-success" href="javascript:void(0)">Có hiển thị</a>';
        }else{
            $recruit->active = 0;
            $recruit->update();
            return '<a onclick="return getActive('.$recruit->id.')" class="text-danger" href="javascript:void(0)">Không hiển thị</a>';
        }
    }
}
