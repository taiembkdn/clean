<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Blog;
use App\Models\Category;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogs = Blog::orderBy('id', 'desc')->get();
        return view('admin.blog.index', compact('blogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('admin.blog.add', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $blog = new Blog();

        if($request->file('picture')){
            $icon = $request->file('picture');
            $file =  $icon->storeAs('blog','blog-'.time().'.jpg', ['disk' => 'upload']);
        }


        $blog->title = $request->title;
        $blog->slug = str_slug($request->title);
        $blog->description = $request->description;
        $blog->detail = $request->detail;
        $blog->category_id = $request->category_id;
        $blog->picture = $file;
        $blog->save();
        return redirect()->route('admin.blog.index')->with('success', 'Thêm thành công');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blog = Blog::findOrFail($id);
        $categories = Category::all();
        return view('admin.blog.edit', compact('blog', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $blog = Blog::findOrFail($id);
        $file = $blog->picture;

        if($request->file('picture')){
            if($request->picture){
                $app_path = str_replace("\\", '/', public_path());
                $image = $app_path.'/upload/'.$blog->picture;
                if (file_exists($image)){
                    unlink($image);
                }
            }
            $icon = $request->file('picture');
            $file =  $icon->storeAs('blog','blog-'.time().'.jpg', ['disk' => 'upload']);
        }


        $blog->title = $request->title;
        $blog->slug = str_slug($request->title);
        $blog->description = $request->description;
        $blog->detail = $request->detail;
        $blog->category_id = $request->category_id;
        $blog->picture = $file;
        $blog->update();
        return redirect()->route('admin.blog.index')->with('success', 'Sửa thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }

    public function delete($id)
    {
        $blog = Blog::find($id);
        
        if(!$blog){
            return redirect()->route('admin.blog.index')->with('error', 'Không tìm thấy bài viết trên');
        }

        if($blog->picture){
            $app_path = str_replace("\\", '/', public_path());
            $image = $app_path.'/upload/'.$blog->picture;
            if (file_exists($image)){
                unlink($image);
            }
        }

        $blog->delete();
        return redirect()->route('admin.blog.index')->with('success', 'Xóa thành công');
    }
    
    public function active(Request $request)
    {
        $id = $request->id;
        $blog = Blog::find($id);


        if(!$blog){
            return redirect()->route('admin.blog.index')->with('error', 'Không tìm thấy bài viết trên');
        }
        if($blog->active == 0){
            $blog->active = 1;
            $blog->update();
            return '<a onclick="return getActive('.$blog->id.')" class="text-success" href="javascript:void(0)">Có hiển thị</a>';
        }else{
            $blog->active = 0;
            $blog->update();
            return '<a onclick="return getActive('.$blog->id.')" class="text-danger" href="javascript:void(0)">Không hiển thị</a>';
        }
    }

}
