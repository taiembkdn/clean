<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class UserController extends Controller
{
  public function index()
  {
    $users = User::orderBy('id', 'desc')->get();
    
    return view('admin.user.index', compact('users'));
  }

  public function show($id)
  {
    $user = User::findOrFail($id);
    return view('admin.user.show', compact('user'));
  }

  public function delete($id)
  {
    $user = User::findOrFail($id);
    $user->delete();

    return redirect()->route('admin.user.index')->with('success', 'Xóa thành công');
  }
}
