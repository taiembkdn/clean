<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Contact;

class ContactController extends Controller
{
	public function index()
    {
        $contacts = Contact::orderBy('id', 'desc')->get();
        return view('admin.contact.index', compact('contacts'));
    }

    public function show($id){
    	$contact = Contact::findOrFail($id);
    	return view('admin.contact.show', compact('contact'));
    }

    public function delete($id){
    	$contact = Contact::findOrFail($id);
    	return redirect()->back()->with('Xóa thành công');
    }
}
