<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin;

class AdminController extends Controller
{
    public function index(){
        $users = Admin::orderBy('id', 'desc')->get();
    	return view('admin.admin.index', compact('users'));
    }
    public function create(){
    	return view('admin.admin.add');
    }
    public function store(Request $request){
    	$request->validate([
    		'name' => 'required',
    		'email' => 'required|email|unique:admins',
    		'password' => 'required'
    	]);
    	$user = new Admin();
    	$user->name = $request->name;
    	$user->email = $request->email;
    	$user->password = trim(bcrypt($request->password));

    	$user->save();
    	return redirect(route('admin.admin.index'))->with('success', 'Thêm thành công');
    }
    public function edit($id)
    {
        $user = Admin::find($id);
        return view('admin.admin.edit', compact('user'));
    }

    public function update($id, Request $request){
            $request->validate([
                'name' => 'required',
                'email' => 'required|email|unique:admins,email,'.$id,
            ]);

            $user = Admin::find($id);
            $user->name = $request->name;
            $user->email = $request->email;

            if(isset($request->password)){
                $user->password = trim(bcrypt($request->password));
            }

            $user->update();
            return redirect(route('admin.admin.index'))->with('success', 'Sửa thành công');

    }
    public function delete($id){
        $user = Admin::findOrFail($id);
        $user->delete();
        return redirect(route('admin.admin.index'))->with('success', 'Xoá thành công');
    }

}
