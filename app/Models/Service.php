<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    public function booking(){
    	return $this->hasMany('App\Models\Booking');
    }
    public function process(){
    	return $this->hasMany('App\Models\Process');
    }
}
