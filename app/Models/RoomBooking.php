<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoomBooking extends Model
{
    public function booking(){
    	return $this->belongsTo('App\Models\Booking');
    }
    public function room(){
    	return $this->belongsTo('App\Models\Room');
    }
}
