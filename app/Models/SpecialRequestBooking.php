<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SpecialRequestBooking extends Model
{
    public function booking(){
    	return $this->belongsTo('App\Models\Booking');
    }
    
    public function special(){
    	return $this->belongsTo('App\Models\SpecialRequest');
    }
}
