<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $fillable = ['name', 'slug', 'room_id'];

    public function room(){
        return $this->hasMany('App\Models\Room');
    }

    public function isRoom(){
        return $this->belongsTo('App\Models\Room');
    }

    public function booking(){
    	return $this->belongsToMany('App\Models\Booking', 'room_bookings');
    }
}
