<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SpecialRequest extends Model
{
    protected $table = 'special_requests';
    protected $fillable = ['name', 'slug', 'icon'];


    public function booking(){
    	return $this->belongsToMany('App\Models\Booking', 'special_request_bookings');
    }
}
