<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    public function specialrequest(){
    	return $this->belongsTo('App\Models\SpecialRequest');
    }

    public function room(){
    	return $this->belongsToMany('App\Models\Room', 'room_bookings');
    }

    public function special(){
    	return $this->belongsToMany('App\Models\SpecialRequest', 'special_request_bookings');
    }

    public function user(){
    	return $this->belongsTo('App\User');
    }

    public function service(){
    	return $this->belongsTo('App\Models\Service');
    }
}
